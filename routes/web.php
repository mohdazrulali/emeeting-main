<?php

use App\Http\Controllers\ACLController;
use App\Http\Controllers\AgihanController;
use App\Http\Controllers\AuditLogController;
use App\Http\Controllers\CabutanController;
use App\Http\Controllers\DepartmentController;
use App\Http\Controllers\DocumentController;
use App\Http\Controllers\LampiranController;
use App\Http\Controllers\MeetingController;
use App\Http\Controllers\ReplyController;
use App\Http\Controllers\UserController;
use App\Models\Role;
use App\Services\DocumentUploadService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('dashboard');
    // return view('welcome');
});

Auth::routes([
    'register' => false, // Registration Routes...
    // 'reset' => false, // Password Reset Routes...
    // 'verify' => false, // Email Verification Routes...
]);




Route::middleware(['auth', 'role:' . implode('|', Role::ALL_SYSTEM_ROLES)])->group(function () {
    // Route::prefix('/dokumen/{jenis}')->controller(DocumentController::class)->middleware('role:' . Role::PERANAN_PENTADBIR . '|' . Role::PERANAN_KETUA_JABATAN)->name('documents.')->group(function () {
    Route::prefix('/dokumen/{jenis}')->controller(DocumentController::class)->middleware('role:' . Role::PERANAN_PENTADBIR)->name('documents.')->group(function () {
        Route::get('/', 'index')->name('index');
        Route::post('/', 'store')->name('store');
        Route::get('/create', 'create')->name('create');
        Route::prefix('/{document}')->group(function () {
            Route::get('/', 'show')->name('show');
            Route::put('/', 'update')->name('update');
            Route::get('/edit', 'edit')->name('edit');
            Route::delete('/', 'destroy')->name('destroy');
            Route::resource('/lampirans', LampiranController::class, [
                'only'  =>  ['store', 'destroy']
            ]);
            Route::group(['prefix' => '/agihans', 'as' => 'agihans.', 'middleware'  => ['cannot-set-agihan']], function () {
                Route::post('/store', [AgihanController::class, 'store'])->name('store');
                Route::post('/resend', [AgihanController::class, 'resend'])->name('resend');
                Route::post('/reset', [AgihanController::class, 'reset'])->name('reset');
            });
        });
    });

    Route::get('/dashboard', [App\Http\Controllers\HomeController::class, 'index'])->name('dashboard');


    Route::prefix('/mesyuarat/{jenis}')->controller(MeetingController::class)->middleware('role:' . Role::PERANAN_PENTADBIR . '|' . Role::PERANAN_KETUA_JABATAN)->name('meetings.')->group(function () {
        Route::get('/', 'index')->name('index');
        Route::post('/', 'store')->name('store');
        Route::get('/create', 'create')->name('create');
        Route::prefix('/{meeting}')->group(function () {
            Route::get('/', 'show')->name('show');
            Route::put('/', 'update')->name('update');
            Route::get('/edit', 'edit')->name('edit');
            Route::delete('/', 'destroy')->name('destroy');
            Route::get('/agihan', 'agihan')->name('agihan');
            Route::resource('/lampirans', LampiranController::class, [
                'only'  =>  ['store', 'destroy']
            ]);
            Route::resource('/cabutans', CabutanController::class, [
                'only'  =>  ['store', 'update', 'destroy']
            ]);
            Route::group(['prefix' => '/agihans', 'as' => 'agihans.'], function () {
                Route::post('/store', [AgihanController::class, 'store'])->name('store');
                Route::post('/resend', [AgihanController::class, 'resend'])->name('resend');
                Route::post('/reset', [AgihanController::class, 'reset'])->name('reset');
            });
        });
    });

    Route::controller(LampiranController::class)->prefix('lampirans')->name('lampirans.')->group(function () {
        Route::get('{lampiran}/download', 'download')->name('download');
    });

    Route::delete("/lampirans/{lampiran}", [LampiranController::class, 'destroy'])->name('lampirans.destroy');

    Route::middleware(['role:' . Role::PERANAN_PENTADBIR . '|' . Role::PERANAN_PENTADBIR_SISTEM])->group(function () {
        Route::resources([
            'departments' => DepartmentController::class,
            'users' => UserController::class,
        ]);
        Route::put('/users/{user}/changepassword', [UserController::class, 'changePassword'])->name('users.change-password');
        Route::prefix('/users/{user}/acls')->middleware('cannot-update-self-role-access')->group(function () {
            Route::put('/', [ACLController::class, 'update'])->name('acls.update');
            Route::get('/edit', [ACLController::class, 'edit'])->name('acls.edit');
            Route::post('/reset', [ACLController::class, 'reset'])->name('acls.reset');
        });
        Route::get('/acls', [ACLController::class, 'index'])->name('acls.index');
    });




    //Azrul Tambah
    Route::middleware(['role:' . Role::PERANAN_KETUA_JABATAN])->group(function () {
        Route::get('/users/{id}', [App\Http\Controllers\UserController::class, 'index'])->name('user.index');
        Route::get('/maklumbalas/index', [App\Http\Controllers\ReplyController::class, 'index'])->name('maklumbalas.index');
        Route::get('/mesyuarat/mesyuarat-jemaah-menteri', [App\Http\Controllers\MeetingController::class, 'index'])->name('mesyuarat.mesyuarat-jemaah-menteri');
        // Route::get('/mesyuarat/mesyuarat-jemaah-menteri/{meeting}', [App\Http\Controllers\MeetingController::class, 'show'])->name('mesyuarat.mesyuarat-jemaah-menteri.show');
        Route::get('/mesyuarat/mesyuarat-utama', [App\Http\Controllers\MeetingController::class, 'index'])->name('mesyuarat.mesyuarat-utama');

        Route::get('/view/docs/{lampiran}', [LampiranController::class, 'download'])->name('view.docs');
        Route::group(['prefix' => 'maklumbalas', 'as' => 'reply.'], function () {
            Route::get('/', [ReplyController::class, 'index'])->name('index')->middleware('can:view-maklum-balas');

            Route::prefix('/{model}/{model_id}/ulasan/{agihan}')->group(function () {
                Route::get('/', [ReplyController::class, 'show'])->name('show');
                Route::post('/', [ReplyController::class, 'store'])->name('store')->middleware('can-update-reply');
            })->middleware(['valid-feedback-url']);
        });

    });





    // Route::get('/dashboard', 'HomeController@index')->name('dashboard');

    Route::group(['prefix'  => '/logaudit', 'as' => 'logaudit.', 'middleware' => 'role:' . Role::PERANAN_PENTADBIR. '|' . Role::PERANAN_PENTADBIR_SISTEM], function () {
        Route::prefix('/pengguna')->name('pengguna.')->group(function () {
            Route::get('/', [AuditLogController::class, 'indexUserLogs'])->name('index');
            Route::get('/{user}/{type?}', [AuditLogController::class, 'showUserLogs'])->name('show');
        });

        Route::prefix('/dokumen')->name('dokumen.')->group(function () {
            Route::get('/{type?}', [AuditLogController::class, 'indexDocumentLogs'])->name('index');
            Route::get('/dokumen/{document}', [AuditLogController::class, 'showDocumentLogs'])->name('document.show');
            Route::get('/mesyuarat/{meeting}', [AuditLogController::class, 'showMeetingLogs'])->name('meeting.show');
        });
    });
});

Route::group(['prefix' => '/test'], function () {
    Route::get('/downloadwithwatermark/{watermarkString}', function () {

        $path = base_path("/tests/TestAssets/lorem-ipsum.pdf");

        $doc = (new DocumentUploadService())->setWatermarkToDoc($path, request()->watermarkString);

        return response()->download($doc, null);
    });
});
