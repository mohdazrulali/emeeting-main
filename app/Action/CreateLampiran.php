<?php

namespace App\Action;

use App\Models\Lampiran;

class CreateLampiran
{
    public function create($type, $file, $file_name, $model)
    {
        $path = $file->store('lampiran_' . $type);

        $lampiran = new Lampiran();
        $lampiran->name = $file_name;
        $lampiran->pautan = $path;
        $lampiran->jenis = $type;
        $lampiran->lampiranable()->associate($model);
        $lampiran->save();

        return $lampiran;
    }
}
