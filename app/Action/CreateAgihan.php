<?php

namespace App\Action;

use App\Models\Agihan;

class CreateAgihan
{
    public function create($penerima, $model)
    {
        $agihan = new Agihan();
        $agihan->penerima = $penerima;
        $agihan->agihanable()->associate($model);
        $agihan->save();

        return $agihan;
    }
}
