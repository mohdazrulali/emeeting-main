<?php

namespace App\Models;

use App\Jobs\TriggerLogs;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class Meeting extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'name',
        'tarikh_mesyuarat',
        'is_approved',
        'jenis',
        'tujuan',
        'approved_date',
        'ringkasan',
        'no_rujukan_fail',
        'tarikh_tutup',
    ];

    // ---------------------------------
    // Relationship
    // ---------------------------------
    public function lampirans()
    {
        return $this->morphMany(Lampiran::class, 'lampiranable');
    }

    public function agihans()
    {
        return $this->morphMany(Agihan::class, 'agihanable');
    }

    public function cabutans()
    {
        return $this->hasMany(Cabutan::class);
    }

    // ---------------------------------
    // Accessors & Mutators
    // ---------------------------------

    protected function tarikhMesyuarat(): Attribute
    {
        return Attribute::make(
            get: fn ($value) => Carbon::parse($value)->format('d-m-Y'),
        );
    }

    protected function approvedDate(): Attribute
    {
        return Attribute::make(
            get: fn ($value) => Carbon::parse($value)->format('d-m-Y'),
        );
    }

    protected function getJabatanAgihansAttribute()
    {
        $agihans = $this->agihans()->get();

        $jabatans = [];

        foreach($agihans as $agihan) {
            if (!in_array($agihan->jabatan_penerima, $jabatans)) {
                array_push($jabatans, $agihan->jabatan_penerima);
            }
        }

        return $jabatans;
    }

    public function auditLogs(): MorphMany
    {
        return $this->morphMany(AuditLog::class, 'loggable');
    }

    protected static function booted(): void
    {
        static::created(function (Meeting $meeting) {
            dispatch(new TriggerLogs(auth()->user(), Meeting::class, $meeting->id, 'created'));
        });

        static::updated(function (Meeting $meeting) {
            dispatch(new TriggerLogs(auth()->user(), Meeting::class, $meeting->id, 'updated', $meeting->getChanges()));
        });

        static::deleted(function (Meeting $meeting) {
            dispatch(new TriggerLogs(auth()->user(), Meeting::class, $meeting->id, 'deleted'));
        });
    }
}
