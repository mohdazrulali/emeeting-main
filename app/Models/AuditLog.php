<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AuditLog extends Model
{
    use HasFactory;

    protected $guarded = [];

    const ALLOWED_EVENTS = ['created', 'updated', 'deleted'];

    public function user()
    {
        return $this->belongsTo(User::class, 'performed_by');
    }

    public function loggable()
    {
        return $this->morphTo()->withTrashed();
    }
}
