<?php

namespace App\Models;

use App\Jobs\TriggerLogs;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Agihan extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'penerima',
    ];

    protected $appends = ['jabatan_penerima'];

    public function agihanable()
    {
        return $this->morphTo();
    }

    // ---------------------------------
    // Accessors & Mutators
    // ---------------------------------

    protected function createdAt(): Attribute
    {
        return Attribute::make(
            get: fn ($value) => Carbon::parse($value)->format('d-m-Y H:i:s'),
        );
    }

    protected function getJabatanPenerimaAttribute()
    {
        return User::where('email', $this->penerima)->first()->department->shortcode;
    }

    public function getLabelJabatanPenerimaAttribute()
    {
        return User::where('email', $this->penerima)->first()->department->name;
    }

    protected static function booted(): void
    {
        static::created(function (Agihan $agihan) {
            $changes = [
                'agihan' => $agihan,
                'event'  => 'created'
            ];

            $agihanableType = $agihan->agihanable_type;
            $agihanableId = $agihan->agihanable_id;


            if ($agihan->agihanable_type === Cabutan::class) {
                $changes = [
                    'cabutan'   => $changes,
                    'event'     => 'updated'
                ];
                $parent = Cabutan::find($agihan->agihanable_id)->meeting;
                $agihanableType = get_class($parent);
                $agihanableId = $parent->id;
            }

            dispatch(new TriggerLogs(auth()->user(), $agihanableType, $agihanableId, 'updated', [
                'agihan' => $agihan,
                'event'  => 'created'
            ]));
        });

        static::deleted(function (Agihan $agihan) {
            $changes = [
                'agihan' => $agihan,
                'event'  => 'deleted'
            ];

            $agihanableType = $agihan->agihanable_type;
            $agihanableId = $agihan->agihanable_id;


            if ($agihan->agihanable_type === Cabutan::class) {
                $changes = [
                    'cabutan'   => $changes,
                    'event'     => 'updated'
                ];
                $parent = Cabutan::find($agihan->agihanable_id)->meeting;
                $agihanableType = get_class($parent);
                $agihanableId = $parent->id;
            }

            dispatch(new TriggerLogs(auth()->user(), $agihanableType, $agihanableId, 'updated', [
                'agihan' => $agihan,
                'event'  => 'deleted'
            ]));
        });
    }

    public function replies()
    {
        return $this->morphMany(Reply::class, 'repliable');
    }

    public function getUserReplyAttribute()
    {
        return $this->replies()->where('user_id', auth()->user()->id)->first();
    }
}
