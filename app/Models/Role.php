<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    use HasFactory;

    const PERANAN_PENTADBIR_SISTEM = 'Pentadbir Sistem';
    const PERANAN_PENTADBIR = 'Pentadbir';
    const PERANAN_KETUA_JABATAN = 'Ketua Jabatan';
    const PERANAN_SUPERUSER = 'Superuser';

    const ALL_SYSTEM_ROLES = [
        self::PERANAN_KETUA_JABATAN,
        self::PERANAN_PENTADBIR_SISTEM,
        self::PERANAN_PENTADBIR,
        self::PERANAN_SUPERUSER
    ];
}
