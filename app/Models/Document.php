<?php

namespace App\Models;

use App\Jobs\TriggerLogs;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class Document extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'name',
        'sub_name',
        'no_fail_kementerian',
        'tarikh_setuju_ksu',
        'tarikh_lulus_ybm',
        'tarikh_tutup_ulasan',
        'tarikh_keputusan',
        'tarikh_dokumen',
        'salinan_bilangan',
        'jenis',
        'created_by',
        'department_id'
    ];

    protected $appends = ['jabatan_agihans'];

    // ---------------------------------
    // Relationship
    // ---------------------------------

    public function createdBy()
    {
        return $this->belongsTo(User::class, 'created_by', 'id');
    }

    public function updatedBy()
    {
        return $this->belongsTo(User::class, 'updated_by', 'id');
    }

    public function department()
    {
        return $this->belongsTo(Department::class);
    }

    public function lampirans()
    {
        return $this->morphMany(Lampiran::class, 'lampiranable');
    }

    // ---------------------------------
    // Accessors & Mutators
    // ---------------------------------

    protected function tarikhSetujuKsu(): Attribute
    {
        return Attribute::make(
            get: fn ($value) => Carbon::parse($value)->format('d-m-Y'),
        );
    }

    protected function tarikhLulusYbm(): Attribute
    {
        return Attribute::make(
            get: fn ($value) => Carbon::parse($value)->format('d-m-Y'),
        );
    }

    protected function tarikhTutupUlasan(): Attribute
    {
        return Attribute::make(
            get: fn ($value) => Carbon::parse($value)->format('d-m-Y'),
        );
    }

    protected function tarikhKeputusan(): Attribute
    {
        return Attribute::make(
            get: fn ($value) => Carbon::parse($value)->format('d-m-Y'),
        );
    }

    protected function tarikhDocumen(): Attribute
    {
        return Attribute::make(
            get: fn ($value) => Carbon::parse($value)->format('d-m-Y'),
        );
    }

    protected function createdAt(): Attribute
    {
        return Attribute::make(
            get: fn ($value) => Carbon::parse($value)->format('d-m-Y'),
        );
    }

    protected function updatedAt(): Attribute
    {
        return Attribute::make(
            get: fn ($value) => Carbon::parse($value)->format('d-m-Y'),
        );
    }

    public function agihans()
    {
        return $this->morphMany(Agihan::class, 'agihanable');
    }

    protected function getJabatanAgihansAttribute()
    {
        $agihans = $this->agihans()->get();

        $jabatans = [];

        foreach($agihans as $agihan) {
            if (!in_array($agihan->jabatan_penerima, $jabatans)) {
                array_push($jabatans, $agihan->jabatan_penerima);
            }
        }

        return $jabatans;
    }

    public function auditLogs(): MorphMany
    {
        return $this->morphMany(AuditLog::class, 'loggable');
    }

    protected static function booted(): void
    {
        static::created(function (Document $document) {
            dispatch(new TriggerLogs(auth()->user(), Document::class, $document->id, 'created'));
        });

        static::updated(function (Document $document) {
            dispatch(new TriggerLogs(auth()->user(), Document::class, $document->id, 'updated', $document->getChanges()));
        });

        static::deleted(function (Document $document) {
            dispatch(new TriggerLogs(auth()->user(), Document::class, $document->id, 'deleted'));
        });
    }
}
