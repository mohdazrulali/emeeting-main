<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ministry extends Model
{
    use HasFactory;

    protected $fillable = [
        'name', 'shortcode',
    ];

    // ---------------------------------
    // Relationships
    // ---------------------------------

    public function documents()
    {
        return $this->hasMany(Document::class);
    }
}
