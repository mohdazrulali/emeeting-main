<?php

namespace App\Models;

use App\Jobs\TriggerLogs;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Lampiran extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'name',
        'pautan',
        'jenis',
    ];

    public function lampiranable()
    {
        return $this->morphTo();
    }

    public function getLabelJenisLampiranAttribute()
    {
        switch ($this->jenis) {
            case 'mesyuarat':
                return 'Mesyuarat';
            case 'lain':
                return 'Lampiran Lain';
            case 'ringkasan': 
                return 'Ringkasan Dokumen';
            case 'tambahan':
                return 'Dokumen Tambahan';
            default:
                return '-';
        }
    }

    protected static function booted(): void
    {
        static::created(function (Lampiran $lampiran) {
            dispatch(new TriggerLogs(auth()->user(), $lampiran->lampiranable_type, $lampiran->lampiranable_id, 'updated', [
                'lampiran'  => $lampiran,
                'event'     => 'created'
            ]));
        });

        static::deleted(function (Lampiran $lampiran) {
            dispatch(new TriggerLogs(auth()->user(), $lampiran->lampiranable_type, $lampiran->lampiranable_id, 'updated', [
                'lampiran'  => $lampiran,
                'event'     => 'deleted'
            ]));
        });
    }
}
