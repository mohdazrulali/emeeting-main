<?php

namespace App\Models;

use App\Jobs\TriggerLogs;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Reply extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = ['content', 'user_id', 'approved_by', 'approved_at'];

    const TYPE_DOKUMEN_ULASAN_JABATAN = 'lampiran_dokumen_ulasan_jabatan';
    const TYPE_DOKUMEN_SOKONGAN = 'lampiran_dokumen_sokongan';

    public function repliable()
    {
        return $this->morphTo();
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function lampiranUlasanJabatan()
    {
        return $this->morphOne(Lampiran::class, 'lampiranable')->where('jenis', self::TYPE_DOKUMEN_ULASAN_JABATAN);
    }

    public function lampiranSokongan()
    {
        return $this->morphOne(Lampiran::class, 'lampiranable')->where('jenis', self::TYPE_DOKUMEN_SOKONGAN);
    }

    protected static function booted(): void
    {
        static::created(function (Reply $reply) {
            if ($reply->repliable->agihanable_type === Cabutan::class) {
                return dispatch(new TriggerLogs(auth()->user(), $reply->repliable->agihanable_type, $reply->repliable->agihanable_id, 'updated', [
                    'cabutan' => array_merge(collect($reply->repliable->agihanable)->toArray(), [
                        'agihan' => array_merge(collect($reply->repliable)->toArray(), [
                            'reply' => $reply,
                            'event' => 'created'
                        ]),
                    ]),
                ]));
            }

            dispatch(new TriggerLogs(auth()->user(), $reply->repliable->agihanable_type, $reply->repliable->agihanable_id, 'updated', [
                'agihan' => array_merge(collect($reply->repliable)->toArray(), [
                    'reply' => $reply,
                    'event' => 'created'
                ]),
            ]));
        });

        static::updated(function (Reply $reply) {
            if ($reply->repliable->agihanable_type === Cabutan::class) {
                return dispatch(new TriggerLogs(auth()->user(), $reply->repliable->agihanable_type, $reply->repliable->agihanable_id, 'updated', [
                    'cabutan' => array_merge(collect($reply->repliable->agihanable)->toArray(), [
                        'agihan' => array_merge(collect($reply->repliable)->toArray(), [
                            'reply' => $reply,
                            'event' => 'updated'
                        ]),
                    ]),
                ]));
            }

            dispatch(new TriggerLogs(auth()->user(), $reply->repliable->agihanable_type, $reply->repliable->agihanable_id, 'updated', [
                'agihan' => array_merge(collect($reply->repliable)->toArray(), [
                    'reply' => $reply,
                    'event' => 'updated'
                ]),
            ]));
        });
    }
}
