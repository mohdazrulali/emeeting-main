<?php

namespace App\Models;

use App\Jobs\TriggerLogs;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cabutan extends Model
{
    use HasFactory;

    protected $fillable = [
        'content',
        'maklum_balas',
        'meeting_id'
    ];

    protected $appends = ['jabatans'];


    // ---------------------------------
    // Relationship
    // ---------------------------------

    public function meeting()
    {
        return $this->belongsTo(Meeting::class);
    }

    public function agihans()
    {
        return $this->morphMany(Agihan::class, 'agihanable');
    }

    protected function getJabatansAttribute()
    {
        $agihans = $this->agihans()->get();

        $jabatans = [];

        foreach($agihans as $agihan) {
            if (!in_array($agihan->jabatan_penerima, $jabatans)) {
                array_push($jabatans, $agihan->jabatan_penerima);
            }
        }

        return $jabatans;
    }

    // public function replies()
    // {
    //     return $this->morphMany(Reply::class, 'repliable');
    // }

    // public function getUserReplyAttribute()
    // {
    //     return $this->replies()->where('user_id', auth()->user()->id)->first();
    // }

    protected static function booted(): void
    {
        static::created(function (Cabutan $cabutan) {
            dispatch(new TriggerLogs(auth()->user(), Meeting::class, $cabutan->meeting->id, 'updated', [
                'cabutan'   => $cabutan,
                'event'     => 'created'
            ]));
        });

        static::updated(function (Cabutan $cabutan) {
            dispatch(new TriggerLogs(auth()->user(), Meeting::class, $cabutan->meeting->id, 'updated', [
                'cabutan'   => $cabutan->fresh(),
                'event'     => 'updated'
            ]));
        });

        static::deleted(function (Cabutan $cabutan) {
            dispatch(new TriggerLogs(auth()->user(), Meeting::class, $cabutan->meeting->id, 'updated', [
                'cabutan'   => $cabutan,
                'event'     => 'deleted'
            ]));
        });
    }
}
