<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;

use App\Jobs\TriggerLogs;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;
use Laravel\Sanctum\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable, HasRoles, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'designation',
        'phone',
        'mobile',
        'change_password_at',
        'ic_no',
        'department_id'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected $append = [
        'current_roles',
    ];

    protected $dates = [
        'change_password_at'
    ];

    // ---------------------------------
    // Relationship
    // ---------------------------------

    public function documentCreatedBy()
    {
        return $this->hasMany(Document::class, 'created_by', 'id');
    }

    public function documentUpdatedBy()
    {
        return $this->hasMany(Document::class, 'updated_by', 'id');
    }

    public function department()
    {
        return $this->belongsTo(Department::class);
    }

    // ---------------------------------
    // Accessors & Mutators
    // ---------------------------------

    protected function currentRoles(): Attribute
    {
        return Attribute::make(
            get: fn ($value) => $this->getRoleNames(),
        );
    }

    protected static function booted(): void
    {
        static::creating(function (User $user) {
            if (Hash::needsRehash($user->password)) {
                $user->password = Hash::make($user->password);
                $user->change_password_at = now();
            }

            $user->email_verified_at = now();
        });

        static::updating(function (User $user) {
            if ($user->password && Hash::needsRehash($user->password)) {
                $user->password = Hash::make($user->password);
                $user->change_password_at = now();
            }
        });

        static::deleted(function (User $user) {
            dispatch(new TriggerLogs(auth()->user(), User::class, $user->id, 'deleted'));
        });
    }

    public function auditLogs(): MorphMany
    {
        return $this->morphMany(AuditLog::class, 'loggable');
    }
}
