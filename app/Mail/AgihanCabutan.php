<?php

namespace App\Mail;

use App\Models\Cabutan;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Mail\Mailables\Content;
use Illuminate\Mail\Mailables\Envelope;
use Illuminate\Queue\SerializesModels;

class AgihanCabutan extends Mailable
{
    use Queueable, SerializesModels;

    private $cabutan;
    private $jenis;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Cabutan $cabutan)
    {
        $this->cabutan = $cabutan;
        $this->jenis = $this->cabutan->meeting->jenis === 'mesyuarat-utama' ? 'Utama' : 'Jemaah Menteri';
    }

    /**
     * Get the message envelope.
     *
     * @return \Illuminate\Mail\Mailables\Envelope
     */
    public function envelope()
    {
        return new Envelope(
            subject: 'eCORES :Arahan Maklumbalas Cabutan Minit Mesyuarat ' . $this->jenis,
        );
    }

    /**
     * Get the message content definition.
     *
     * @return \Illuminate\Mail\Mailables\Content
     */
    public function content()
    {
        return new Content(
            view: 'emails.agihan_cabutan',
            with: [
                'meeting' => $this->cabutan->meeting,
            ]
        );
    }

    /**
     * Get the attachments for the message.
     *
     * @return array
     */
    public function attachments()
    {
        return [];
    }
}
