<?php

namespace App\Http\Requests;

use App\Models\Department;
use Illuminate\Foundation\Http\FormRequest;

class StoreDocumentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function prepareForValidation()
    {
        if ($this->jenis === 'memo-kpkt') {
            $this->merge([
                'department_id' => Department::where('shortcode', 'KPKT')->first()->id,
            ]);
        }
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'name'                  => 'required|string',
            'sub_name'              => 'required|string',
            'no_fail_kementerian'   => 'required|string',
            'tarikh_dokumen'        => 'required|date',
            'salinan_bilangan'      => 'required|string',
            'jenis'                 => 'required|in:memo-kpkt,kementerian-lain,nota',
            'department_id'         => 'required|exists:departments,id'
        ];
    }
}
