<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreMeetingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'name' => 'required|string',
            'tarikh_mesyuarat' => 'required|date',
            'is_approved' => 'required|boolean',
            'jenis' => 'required|string|in:mesyuarat-jemaah-menteri,mesyuarat-utama',
            'tujuan' => 'required|in:Makluman,Tindakan Segera,Tindakan Serta Merta,Tindakan Susulan',
            'approved_date' => 'required|date',
            'ringkasan' => 'required|string',
            'no_rujukan_fail' => 'required|string',
        ];
    }
}
