<?php

namespace App\Http\Requests;

use App\Rules\TestDocLampiran;
use App\Services\DocumentUploadService;
use Exception;
use Illuminate\Foundation\Http\FormRequest;

class StoreLampiranRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'jenis_lampiran'            => 'required|string|in:mesyuarat,lain,ringkasan,tambahan,keputusan-mesyuarat-jemaah-menteri,keputusan-mesyuarat-utama',
            'nama_lampiran'             => 'required|string',
            'fail_lampiran'             => ['required', 'mimes:pdf', new TestDocLampiran]
        ];
    }
}
