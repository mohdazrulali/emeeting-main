<?php

namespace App\Http\Controllers;

use App\Action\CreateLampiran;
use App\Http\Requests\StoreLampiranRequest;
use App\Http\Requests\UpdateLampiranRequest;
use App\Models\Document;
use App\Models\Lampiran;
use App\Models\Meeting;
use App\Services\DocumentUploadService;
use Illuminate\Support\Facades\Storage;

class LampiranController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreLampiranRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreLampiranRequest $request, $jenis, Document $document, Meeting $meeting, CreateLampiran $createLampiran)
    {
        $validated = $request->validated();

        $model = null;

        if ($meeting->id) {
            $model = $meeting;
        }

        if ($document->id) {
            $model = $document;
        }

        $createLampiran->create($validated['jenis_lampiran'], $validated['fail_lampiran'], $validated['nama_lampiran'], $model);

        session()->flash('status', [
            'type' => 'success',
            'message' => 'Lampiran berjaya ditambah.',
        ]);

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Lampiran  $lampiran
     * @return \Illuminate\Http\Response
     */
    public function show(Lampiran $lampiran)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Lampiran  $lampiran
     * @return \Illuminate\Http\Response
     */
    public function edit(Lampiran $lampiran)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateLampiranRequest  $request
     * @param  \App\Models\Lampiran  $lampiran
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateLampiranRequest $request, Lampiran $lampiran)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Lampiran  $lampiran
     * @return \Illuminate\Http\Response
     */
    public function destroy(Lampiran $lampiran)
    {
        Storage::delete($lampiran->pautan);
        $lampiran->delete();

        session()->flash('status', [
            'type' => 'success',
            'message' => 'Lampiran berjaya dipadam.',
        ]);

        return response(null, 200);
    }

    public function download(Lampiran $lampiran)
    {
        $pathToDoc = Storage::path($lampiran->pautan);
        $doc = (new DocumentUploadService())->setWatermarkToDoc($pathToDoc, auth()->user()->name);
        return response()->download($doc, null);
    }
}
