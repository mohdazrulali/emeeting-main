<?php

namespace App\Http\Controllers;

use App\Action\CreateAgihan;
use App\Jobs\SendAgihan;
use App\Jobs\TriggerLogs;
use App\Mail\AgihanMesyuarat;
use App\Models\Agihan;
use App\Models\Department;
use App\Models\Document;
use App\Models\Meeting;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class AgihanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $jenis, Document $document, Meeting $meeting, CreateAgihan $createAgihan)
    {
        $validated = $request->validate([
            'departments' => 'array|required',
            'departments.*' => 'exists:departments,shortcode',
            'model_id' => 'required',
            'jenis' => 'required|in:meeting,document'
        ]);

        $remove_emails = [];

        if ($validated['jenis'] === 'meeting') {
            foreach ($meeting->jabatan_agihans as $jabatan) {
                if (!in_array($jabatan, $validated['departments'])) {
                    $user_emails = User::whereHas('department', function ($q) use ($jabatan) {
                        $q->where('shortcode', $jabatan);
                    })->get()->pluck('email');
                    $remove_emails = array_merge($remove_emails, $user_emails->toArray());
                }
            }

            $meeting->agihans()->whereIn('penerima', $remove_emails)->delete();

            $penerima_emails = User::whereHas('department', function ($q) use ($validated) {
                $q->whereIn('shortcode', $validated['departments']);
            })->get();

            foreach ($penerima_emails as $penerima) {
                SendAgihan::dispatch($penerima, $meeting);
                $agihan = $meeting->agihans()->where('penerima', $penerima->email)->first();

                if ($agihan) {
                    $agihan->touch();
                } else {
                    $meeting->agihans()->create([
                        'penerima'  => $penerima->email
                    ]);
                }
            }
        }

        if ($validated['jenis'] === 'document') {
            foreach ($document->jabatan_agihans as $jabatan) {
                if (!in_array($jabatan, $validated['departments'])) {
                    $user_emails = User::whereHas('department', function ($q) use ($jabatan) {
                        $q->where('shortcode', $jabatan);
                    })->get()->pluck('email');
                    $remove_emails = array_merge($remove_emails, $user_emails->toArray());
                }
            }

            $document->agihans()->whereIn('penerima', $remove_emails)->delete();

            $penerima_emails = User::whereHas('department', function ($q) use ($validated) {
                $q->whereIn('shortcode', $validated['departments']);
            })->get();

            foreach ($penerima_emails as $penerima) {
                SendAgihan::dispatch($penerima, $document);
                $agihan = $document->agihans()->where('penerima', $penerima->email)->first();

                if ($agihan) {
                    $agihan->touch();
                } else {
                    $document->agihans()->create([
                        'penerima'  => $penerima->email
                    ]);
                }
            }
        }

        session()->flash('status', [
            'type' => 'success',
            'message' => 'Agihan berjaya dilaksanakan.',
        ]);

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Agihan  $agihan
     * @return \Illuminate\Http\Response
     */
    public function show(Agihan $agihan)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Agihan  $agihan
     * @return \Illuminate\Http\Response
     */
    public function edit(Agihan $agihan)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Agihan  $agihan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Agihan $agihan)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Agihan  $agihan
     * @return \Illuminate\Http\Response
     */
    public function destroy(Agihan $agihan)
    {
        //
    }

    public function resend(Request $request, $jenis, Document $document, Meeting $meeting)
    {
        $validated = $request->validate([
            'model_id'          => 'required',
            'jenis'             => 'required|in:meeting,document',
            'email_penerima'    => 'required'
        ]);

        $penerima = User::where('email', $validated['email_penerima'])->first();

        if ($document->id) {
            SendAgihan::dispatch($penerima, $document);
            dispatch(new TriggerLogs(auth()->user(), Document::class, $document->id, 'updated', [
                'agihan' => [
                    'penerima'    => $penerima->email
                ],
                'event' => 'updated'
            ]));
            $document->agihans()->where('penerima', $validated['email_penerima'])->touch();
        }

        if ($meeting->id) {
            SendAgihan::dispatch($penerima, $meeting);
            dispatch(new TriggerLogs(auth()->user(), Meeting::class, $meeting->id, 'updated', [
                'agihan' => [
                    'penerima'    => $penerima->email
                ],
                'event' => 'updated'
            ]));
            $meeting->agihans()->where('penerima', $validated['email_penerima'])->touch();
        }

        session()->flash('status', [
            'type' => 'success',
            'message' => 'Agihan berjaya dihantar semula.',
        ]);

        return redirect()->back();
    }

    public function reset($jenis, Document $document, Meeting $meeting)
    {
        if ($document->id) {
            $document->agihans()->get()->each->delete();
        }

        if ($meeting->id) {
            $meeting->agihans()->get()->each->delete();
        }

        session()->flash('status', [
            'type'  => 'success',
            'message'   => 'Kesemua agihan berjaya diset semula.'
        ]);

        return redirect()->back();
    }
}
