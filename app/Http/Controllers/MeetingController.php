<?php

namespace App\Http\Controllers;

use App\Action\CreateAgihan;
use App\Http\Requests\StoreMeetingRequest;
use App\Http\Requests\UpdateMeetingRequest;
use App\Models\Agihan;
use App\Models\Cabutan;
use App\Models\Department;
use App\Models\Meeting;
use App\Models\User;
use Illuminate\Http\Request;

class MeetingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $meetings = Meeting::where('jenis', $request->jenis)
            ->when(request()->carian, function ($q) {
                $q->where(function ($sub) {
                    $sub->where('name', 'LIKE', '%' . request()->carian . '%');
                });
            })
            ->orderBy('name', 'ASC')
            ->paginate(10);

        return view('Meeting.index', [
            'jenis' => $request->jenis,
            'meetings' => $meetings,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        return view('Meeting.create-multistep', [
            'jenis' => $request->jenis,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreMeetingRequest  $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(StoreMeetingRequest $request)
    {

        $meeting = new Meeting();
        $meeting->name = $request['name'];
        $meeting->tarikh_mesyuarat = $request['tarikh_mesyuarat'];
        $meeting->is_approved = $request['is_approved'];
        $meeting->jenis = $request['jenis'];
        $meeting->tujuan = $request['tujuan'];
        $meeting->approved_date = $request['approved_date'];
        $meeting->ringkasan = $request['ringkasan'];
        $meeting->no_rujukan_fail = $request['no_rujukan_fail'];
        $meeting->save();

        session()->flash('status', [
            'type'      => 'success',
            'message'   => 'Maklumat Berjaya Disimpan',
        ]);

        return redirect(route('meetings.index', [
            'jenis'   => request()->jenis,
        ]));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Meeting  $meeting
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Meeting $meeting)
    {
        dd($meeting);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Meeting  $meeting
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($jenis, Meeting $meeting)
    {
        if (!isset(request()->step) || (!in_array(request()->step, [1, 2, 3, 4]))) {
            return redirect()->route('meetings.edit', ['jenis'   => request()->jenis, 'meeting' => $meeting->id, 'step' => 1]);
        }

        if (request('kemaskini_cabutan', null)) {
            $current_cabutan = Cabutan::where('id', request()->kemaskini_cabutan)->where('meeting_id', $meeting->id)->first();

            if (!$current_cabutan) {
                return redirect()->route('meetings.edit', ['jenis'   => request()->jenis, 'meeting' => $meeting->id, 'step' => 4]);
            }
        }

        $lampirans = $meeting->lampirans()->where('jenis', 'mesyuarat')->orderBy('name', 'ASC')->paginate(10, ['*'], 'lampiran_page');
        $cabutans = $meeting->cabutans()->get();

        return view('Meeting.edit-multistep', [
            'meeting'       => $meeting,
            'jenis'         => $meeting->jenis,
            'departments'   => Department::all(),
            'lampirans'     => $lampirans,
            'cabutans'      => $cabutans
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateMeetingRequest  $request
     * @param  \App\Models\Meeting  $meeting
     *
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateMeetingRequest $request, $jenis, Meeting $meeting, CreateAgihan $createAgihan)
    {
        // dd($request->all(), $meeting);

        $user = auth()->user();

        $meeting->name = $request['name'];
        $meeting->tarikh_mesyuarat = $request['tarikh_mesyuarat'];
        $meeting->is_approved = $request['is_approved'];
        $meeting->jenis = $request['jenis'];
        $meeting->tujuan = $request['tujuan'];
        $meeting->approved_date = $request['approved_date'];
        $meeting->ringkasan = $request['ringkasan'];
        $meeting->no_rujukan_fail = $request['no_rujukan_fail'];
        $meeting->tarikh_tutup =  $request['tarikh_tutup'];
        $meeting->save();

        session()->flash('status', [
            'type' => 'success',
            'message' => 'Maklumat berjaya dikemaskini.',
        ]);

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Meeting  $meeting
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($jenis, Meeting $meeting)
    {
        $meeting->delete();

        if ($meeting) {
            request()->session()->flash('status', [
                'type' => 'success',
                'message' => 'Maklumat Berjaya Dihapus',
            ]);
        } else {
            request()->session()->flash('status', [
                'type' => 'danger',
                'message' => 'Maklumat Gagal Dihapus',
            ]);
        }

        return redirect(route('meetings.index', ['jenis'   => request()->jenis]));
    }

    public function agihan($jenis, Meeting  $meeting)
    {
        return view('Meeting.agihan', [
            'meeting' => $meeting->load('agihans'),
            'departments' => Department::all(),
        ]);
    }
}
