<?php

namespace App\Http\Controllers;

use App\Models\Department;
use Illuminate\Http\Request;

class DepartmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('Department.index', [
            'departments' => Department::all(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Department.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'name' => 'required|string',
            'code' => 'required|string',
            'shortcode' => 'required|alpha_dash|unique:departments,shortcode',
            'section' => 'required|string',
        ]);

        $department = new Department();
        $department->name = $validated['name'];
        $department->code = $validated['code'];
        $department->shortcode = $validated['shortcode'];
        $department->section = $validated['section'];
        $department->save();

        if ($department) {
            $request->session()->flash('status', [
                'type' => 'success',
                'message' => 'Maklumat Berjaya Disimpan',
            ]);
        } else {
            $request->session()->flash('status', [
                'type' => 'danger',
                'message' => 'Maklumat Gagal Disimpan',
            ]);
        }

        return redirect(route('departments.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Department  $department
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Department $department)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Department  $department
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Department $department)
    {
        return view('Department.edit', [
            'department' => $department,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Department  $department
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Department $department)
    {
        $validated = $request->validate([
            'name' => 'required|string',
            'code' => 'required|string',
            'shortcode' => 'required|alpha_dash|unique:departments,shortcode,' . $department->id,
            'section' => 'required|string',
        ]);

        $department->name = $validated['name'];
        $department->code = $validated['code'];
        $department->shortcode = $validated['shortcode'];
        $department->section = $validated['section'];
        $department->save();

        if ($department) {
            $request->session()->flash('status', [
                'type' => 'success',
                'message' => 'Maklumat Berjaya Dikemaskini',
            ]);
        } else {
            $request->session()->flash('status', [
                'type' => 'danger',
                'message' => 'Maklumat Gagal Dikemaskini',
            ]);
        }

        return redirect(route('departments.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Department  $department
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Department $department)
    {
        $department->delete();

        if ($department) {
            request()->session()->flash('status', [
                'type' => 'success',
                'message' => 'Maklumat Berjaya Dihapus',
            ]);
        } else {
            request()->session()->flash('status', [
                'type' => 'danger',
                'message' => 'Maklumat Gagal Dihapus',
            ]);
        }

        return redirect(route('departments.index'));
    }
}
