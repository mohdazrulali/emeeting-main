<?php

namespace App\Http\Controllers;

use App\Models\Department;
use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    private function getUserService()
    {
        return app('UserService')->setCurrentUser(auth()->user());
    }

    public function index()
    {
        $validated = request()->validate([
            'page'      => 'bail|integer',
            'carian'    => 'bail|string|nullable'
        ]);

        $users = $this->getUserService()->getUsers($validated);

        return view('User.index', [
            'users' => $users
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('User.create', [
            'roles' => Role::ALL_SYSTEM_ROLES,
            'departments'   => Department::orderBy('name', 'ASC')->get()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'name'          => 'bail|required|string',
            'email'         => 'bail|required|email|unique:users,email',
            'password'      => 'bail|required|confirmed|min:8',
            'designation'   => 'bail|required',
            'ic_no'         => 'bail|required|unique:users,ic_no|string|min:12|max:12',
            'phone'         => 'bail|required|regex:/^([0-9\s\-\+\(\)]*)$/|min:10',
            'mobile'        => 'bail|required|regex:/^([0-9\s\-\+\(\)]*)$/|min:10',
            'department_id' => 'bail|required|exists:departments,id',
            'role'          => 'bail|required|in:' . implode(',', Role::ALL_SYSTEM_ROLES)
        ]);

        $user = $this->getUserService()->registerUser($validated);

        session()->flash('status', [
            'type' => 'success',
            'message' => 'Pengguna berjaya didaftar.',
        ]);

        return redirect()->route('users.show', $user->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        return view('User.show', [
            'user'  => $user,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        return view('User.edit', [
            'user'  => $user,
            'roles' => Role::ALL_SYSTEM_ROLES,
            'departments'   => Department::orderBy('name', 'ASC')->get()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(User $user, Request $request)
    {
        $validated = $request->validate([
            'name'          => 'bail|required|string',
            'email'         => 'bail|required|email|unique:users,email,' . $user->id,
            'designation'   => 'bail|required',
            'ic_no'         => 'bail|required|unique:users,ic_no,' . $user->id .'|string|min:12|max:12',
            'phone'         => 'bail|required|regex:/^([0-9\s\-\+\(\)]*)$/|min:10',
            'mobile'        => 'bail|required|regex:/^([0-9\s\-\+\(\)]*)$/|min:10',
            'department_id' => 'bail|required|exists:departments,id',
        ]);

        $user = $this->getUserService()->updateUserProfile($user, $validated);

        session()->flash('status', [
            'type' => 'success',
            'message' => 'Profil pengguna berjaya dikemaskini.',
        ]);

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $this->getUserService()->deleteUser($user);

        session()->flash('status', [
            'type' => 'success',
            'message' => 'Profil pengguna berjaya dipadam.',
        ]);

        return response()->json([], 200);
    }

    public function changePassword(User $user, Request $request)
    {
        $validated = $request->validate([
            'password'      => 'bail|required|confirmed|min:8',
        ]);

        $user = $this->getUserService()->updateUserProfile($user, $validated);

        session()->flash('status', [
            'type' => 'success',
            'message' => 'Kata laluan pengguna berjaya dikemaskini.',
        ]);

        return redirect()->back();
    }
}
