<?php

namespace App\Http\Controllers;

use App\Action\CreateAgihan;
use App\Jobs\SendAgihan;
use App\Models\Agihan;
use App\Models\Cabutan;
use App\Models\Meeting;
use App\Models\User;
use Illuminate\Http\Request;

class CabutanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $jenis, Meeting $meeting, CreateAgihan $createAgihan)
    {
        $validated = $request->validate([
            'cabutan_departments'   => 'required|array',
            'cabutan_departments.*' => 'required|exists:departments,shortcode',
            'content'               => 'required|string',
        ]);

        $cabutan = Cabutan::create([
            'content' => $validated['content'],
            'meeting_id'    => $meeting->id,
        ]);

        $departmentUsers = User::whereHas('department', function ($q) use ($validated) {
            $q->whereIn('shortcode', $validated['cabutan_departments']);
        })->get();

        foreach ($departmentUsers as $penerima) {
            SendAgihan::dispatch($penerima, $cabutan);
            $createAgihan->create($penerima->email, $cabutan);
        }

        session()->flash('status', [
            'type' => 'success',
            'message' => 'Cabutan berjaya dihantar.',
        ]);

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Cabutan  $cabutan
     * @return \Illuminate\Http\Response
     */
    public function show(Cabutan $cabutan)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Cabutan  $cabutan
     * @return \Illuminate\Http\Response
     */
    public function edit(Cabutan $cabutan)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Cabutan  $cabutan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $jenis, Meeting $meeting, Cabutan $cabutan)
    {
        $validated = $request->validate([
            'cabutan_departments'   => 'required|array',
            'cabutan_departments.*' => 'required|exists:departments,shortcode',
            'content'               => 'required|string',
        ]);

        $cabutan->content = $validated['content'];

        $cabutan->save();

        $remove_emails = [];

        foreach ($cabutan->jabatans as $jabatan) {
            if (!in_array($jabatan, $validated['cabutan_departments'])) {
                $user_emails = User::whereHas('department', function ($q) use ($jabatan) {
                    $q->where('shortcode', $jabatan);
                })->get()->pluck('email');
                $remove_emails = array_merge($remove_emails, $user_emails->toArray());
            }
        }

        $cabutan->agihans()->whereIn('penerima', $remove_emails)->delete();

        $penerima_emails = User::whereHas('department', function ($q) use ($validated) {
            $q->whereIn('shortcode', $validated['cabutan_departments']);
        })->get();

        foreach ($penerima_emails as $penerima) {
            SendAgihan::dispatch($penerima, $cabutan);
            $agihan = $cabutan->agihans()->where('penerima', $penerima->email)->first();

            if ($agihan) {
                $agihan->touch();
            }
            else {
                $cabutan->agihans()->create([
                    'penerima'  => $penerima->email
                ]);
            }
        }

        session()->flash('status', [
            'type'      => 'success',
            'message'   => 'Maklumat cabutan berjaya dikemaskini.'
        ]);

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Cabutan  $cabutan
     * @return \Illuminate\Http\Response
     */
    public function destroy($jenis, Meeting $meeting, Cabutan $cabutan)
    {
        Cabutan::where('meeting_id', $meeting->id)->where('id', $cabutan->id)->get()->each->delete();

        session()->flash('status', [
            'type'      => 'success',
            'message'   => 'Cabutan mesyuarat berjaya dipadam.'
        ]);

        return response()->json([], 200);
    }
}
