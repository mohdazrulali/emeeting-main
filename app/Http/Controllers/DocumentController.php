<?php

namespace App\Http\Controllers;

use App\Action\CreateLampiran;
use App\Http\Requests\StoreDocumentRequest;
use App\Http\Requests\UpdateDocumentRequest;
use App\Models\Department;
use App\Models\Document;
use App\Models\Ministry;
use Illuminate\Http\Request;

class DocumentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $documents = Document::where('jenis', $request->jenis)
            ->when(request()->carian, function ($q) {
                $q->where(function ($sub) {
                    $sub->where('name', 'LIKE', '%' . request()->carian . '%')
                        ->orWhere('sub_name', 'LIKE', '%' . request()->carian . '%')
                        ->orWhere('no_fail_kementerian', 'LIKE', '%' . request()->carian . '%');
                });
            })
            ->orderBy('name', 'ASC')
            ->paginate(10);

        return view('Document.index', [
            'jenis' => $request->jenis,
            'documents' => $documents,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        return view('Document.create-multistep', [
            'jenis' => $request->jenis,
            'departments' => Department::all(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreDocumentRequest  $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store($jenis, StoreDocumentRequest $request)
    {
        $validated = $request->validated();

        Document::create(array_merge($validated, [
            'created_by'    => auth()->user()->id
        ]));

        session()->flash('status', [
            'type' => 'success',
            'message' => 'Maklumat berjaya disimpan.',
        ]);

        return redirect(route('documents.index', [
            'jenis' => request()->jenis,
        ]));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Document  $document
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Document $document)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Document  $document
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($jenis, Document $document)
    {
        if (!isset(request()->step) || (!in_array(request()->step, [1, 2, 3]))) {
            return redirect()->route('documents.edit', ['jenis' => request()->jenis, 'document' => $document->id, 'step' => 1]);
        }

        $lampirans = $document->lampirans()->orderBy('name', 'ASC')->paginate(10, ['*'], 'lampiran_page');

        return view('Document.edit-multistep', [
            'jenis'                     => $document->jenis,
            'document'                  => $document,
            'departments'               => Department::orderBy('name', 'asc')->get(),
            'lampirans'                 => $lampirans
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateDocumentRequest  $request
     * @param  \App\Models\Document  $document
     *
     * @return \Illuminate\Http\Response
     */
    public function update($jenis, Document $document, UpdateDocumentRequest $request)
    {
        $validated = $request->validated();

        $document->update($validated);

        $request->session()->flash('status', [
            'type' => 'success',
            'message' => 'Maklumat berjaya dikemaskini.',
        ]);

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Document  $document
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($jenis, Document $document)
    {
        $document->delete();

        if ($document) {
            request()->session()->flash('status', [
                'type' => 'success',
                'message' => 'Maklumat Berjaya Dihapus',
            ]);
        } else {
            request()->session()->flash('status', [
                'type' => 'danger',
                'message' => 'Maklumat Gagal Dihapus',
            ]);
        }

        return redirect(route('documents.index', ['jenis' => request()->jenis]));
    }

    private function validateUploadFile(Request $request)
    {

        if ($request->has('fail_lampiran_mesyuarat')) {
            foreach ($request->file('fail_lampiran_mesyuarat') as $file) {
                validator(['fail_lampiran_mesyuarat' => $file], [
                    'fail_lampiran_mesyuarat' => 'mimes:pdf'
                ])->validate();
            }
        }

        if ($request->has('fail_lampiran_lain')) {
            foreach ($request->file('fail_lampiran_lain') as $file) {
                validator(['fail_lampiran_lain' => $file], [
                    'fail_lampiran_lain' => 'mimes:pdf'
                ])->validate();
            }
        }

        if ($request->has('fail_lampiran_ringkasan')) {
            foreach ($request->file('fail_lampiran_ringkasan') as $file) {
                validator(['fail_lampiran_ringkasan' => $file], [
                    'fail_lampiran_ringkasan' => 'mimes:pdf'
                ])->validate();
            }
        }

        if ($request->has('fail_lampiran_tambahan')) {
            foreach ($request->file('fail_lampiran_tambahan') as $file) {
                validator(['fail_lampiran_tambahan' => $file], [
                    'fail_lampiran_tambahan' => 'mimes:pdf'
                ])->validate();
            }
        }
    }
}
