<?php

namespace App\Http\Controllers;

use App\Models\Agihan;
use App\Models\AuditLog;
use App\Models\Cabutan;
use App\Models\Document;
use App\Models\Meeting;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        //  $users = User::role('Pentadbir')->get();
        // $users = User::with(['permissions', 'roles'])->get();

        // dd(auth()->user()->roles,$users);
        // dd(AuditLog::get());
        if (!request()->tapisan_tahun) {
            return redirect()->route('dashboard', ['tapisan_tahun' => now()->year]);
        }

        $documents = Document::when(request()->tapisan_tahun, function ($q) {
            $q->whereYear('created_at', request()->tapisan_tahun);
        })
            ->select(\DB::raw("COUNT(id) as total, DATE_FORMAT(created_at, '%m') as month"))
            ->groupBy(\DB::raw("DATE_FORMAT(created_at, '%m')"))->get();

        $meetings = Meeting::when(request()->tapisan_tahun, function ($q) {
            $q->whereYear('created_at', request()->tapisan_tahun);
        })
            ->select(\DB::raw("COUNT(id) as total, DATE_FORMAT(created_at, '%m') as month"))
            ->groupBy(\DB::raw("DATE_FORMAT(created_at, '%m')"))->get();

            $months = [
                '01'    => 'Januari',
                '02'    => 'Februari',
                '03'    => 'Mac',
                '04'    => 'April',
                '05'    => 'Mei',
                '06'    => 'Jun',
                '07'    => 'Julai',
                '08'    => 'Ogos',
                '09'    => 'September',
                '10'    => 'Oktober',
                '11'    => 'November',
                '12'    => 'Disember'
            ];

            $documentData = [];
            $meetingData = [];
            $documentCount = 0;
            $meetingCount = 0;

            foreach ($months as $key => $month) {
                $meeting = array_values(array_filter($meetings->toArray(), fn ($item) => $item['month'] === $key));
                $document = array_values(array_filter($documents->toArray(), fn ($item) => $item['month'] === $key));

                $documentCount = $documentCount + (!empty($document) ? $document[0]['total'] : 0);
                $meetingCount = $meetingCount + (!empty($meeting) ? $meeting[0]['total'] : 0);

                array_push($documentData, !empty($document) ? $document[0]['total'] : 0);
                array_push($meetingData, !empty($meeting) ? $meeting[0]['total'] : 0);
            }

        return view('dashboard', [
            'document_data'     => $documentData,
            'meeting_data'      => $meetingData,
            'document_count'    => $documentCount,
            'meeting_count'     => $meetingCount,
            'labels'            => array_values($months)
        ]);
    }
}
