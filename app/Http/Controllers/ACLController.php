<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class ACLController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('ACL.index', [
            'users' => app('UserService')->setCurrentUser(auth()->user())->getUsers(request()->all())
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        return view('ACL.edit', [
            'user'                  => $user,
            'available_roles'       => Role::pluck('name'),
            'available_permissions' => Permission::pluck('name')
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(User $user, Request $request)
    {
        $validated = $request->validate([
            'roles'     => 'bail|required',
            'roles.*'   => 'string|string|exists:roles,name'
        ]);

        app('ACLService')->setCurrentUser(auth()->user())->updateUserRole($user, $validated);

        session()->flash('status', [
            'type'      => 'success',
            'message'   => 'Maklumat peranan akses pengguna berjaya dikemaskini.',
        ]);

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function reset(User $user)
    {
        app('ACLService')->setCurrentUser(auth()->user())->resetUserRole($user);

        session()->flash('status', [
            'type'      => 'success',
            'message'   => 'Maklumat peranan akses pengguna berjaya diset semula.',
        ]);

        return redirect()->back();
    }
}
