<?php

namespace App\Http\Controllers;

use App\Models\AuditLog;
use App\Models\Document;
use App\Models\Meeting;
use App\Models\User;

class AuditLogController extends Controller
{
    private function getUserService()
    {
        return app('UserService')->setCurrentUser(auth()->user());
    }

    public function indexUserLogs()
    {
        $validated = request()->validate([
            'page'      => 'bail|integer',
            'carian'    => 'bail|string|nullable'
        ]);

        $users = $this->getUserService()->getUsers($validated);

        return view('AuditLog.users.index', [
            'users' => $users
        ]);
    }

    public function showUserLogs(User $user, $type = '')
    {
        request()->validate([
            'type'      => 'bail|string|in:aktiviti,akaun|nullable',
        ]);

        if (!$type) {
            return redirect()->route('logaudit.pengguna.show', [$user, 'aktiviti']);
        }

        $accountLogs = $user->auditLogs()
            ->where('changes->logged_in_at', null)
            ->where('changes->logged_out_at', null)
            ->orderBy('created_at', 'DESC')
            ->orderBy('id', 'DESC')
            ->paginate(20, ['*'], 'user_account_log_page', request('user_account_log_page', 1));

        $activityLogs = AuditLog::where('performed_by', $user->id)
            ->orderBy('created_at', 'DESC')
            ->orderBy('id', 'DESC')
            ->paginate(20, ['*'], 'user_activity_log_page', request('user_activity_log_page', 1));

        return view('AuditLog.users.show', [
            'account_logs'  => $accountLogs,
            'activity_logs' => $activityLogs
        ]);
    }

    public function indexDocumentLogs($type = '')
    {
        request()->validate([
            'type'      => 'bail|string|in:aktiviti,akaun|nullable',
        ]);

        if (!$type) {
            return redirect()->route('logaudit.dokumen.index', ['dokumen']);
        }

        $documents = Document::when(request()->carian, function ($q) {
            $q->where(function ($sub) {
                $sub->where('name', 'LIKE', '%' . request()->carian . '%')
                    ->orWhere('sub_name', 'LIKE', '%' . request()->carian . '%')
                    ->orWhere('no_fail_kementerian', 'LIKE', '%' . request()->carian . '%');
            });
        })->orderBy('id', 'DESC')->orderBy('created_at', 'DESC')
            ->paginate(20, ['*'], 'documents_page', request('documents_page', 1));

        $meetings = Meeting::when(request()->carian, function ($q) {
            $q->where(function ($sub) {
                $sub->where('name', 'LIKE', '%' . request()->carian . '%');
            });
        })->orderBy('id', 'DESC')->orderBy('created_at', 'DESC')
            ->paginate(20, ['*'], 'meetings_page', request('meetings_page', 1));

        return view('AuditLog.documents.index', [
            'documents' => $documents,
            'meetings'  => $meetings
        ]);
    }

    public function showDocumentLogs(Document $document)
    {
        return view('AuditLog.documents.documents', [
            'logs'  => $document->auditLogs()->orderBy('created_at', 'DESC')
                ->orderBy('id', 'DESC')
                ->paginate(20)
        ]);
    }

    public function showMeetingLogs(Meeting $meeting)
    {
        return view('AuditLog.documents.meetings', [
            'logs'  => $meeting->auditLogs()->orderBy('created_at', 'DESC')
                ->orderBy('id', 'DESC')
                ->paginate(20)
        ]);
    }
}
