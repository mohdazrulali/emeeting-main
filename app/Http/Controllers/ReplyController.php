<?php

namespace App\Http\Controllers;

use App\Models\Agihan;
use App\Models\Cabutan;
use App\Models\Department;
use App\Models\Document;
use App\Models\Meeting;
use App\Models\Reply;
use App\Rules\TestDocLampiran;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ReplyController extends Controller
{
    public function index()
    {
        $replies = Agihan::whereHasMorph('agihanable', [Cabutan::class, Document::class, Meeting::class])
            ->where('penerima', auth()->user()->email)
            ->orderBy('created_at', 'DESC')
            ->get();

        return view('Reply.index', [
            'replies'   => $replies
        ]);
    }

    public function show($model, $modelId, Agihan $agihan)
    {
        if (!isset(request()->step) || (!in_array(request()->step, [1, 2]))) {
            return redirect(generateFeedbackUrl('reply.show', $agihan, ['step' => 1]));
        }

        return view('Reply.reply-multistep', [
            'item'          => $agihan->agihanable_type === Cabutan::class ?
                $agihan->agihanable->meeting : $agihan->agihanable,
            'departments'   => Department::all(),
            'agihan'        => $agihan,
            'cabutan'       => $agihan->agihanable_type === Cabutan::class ?
                $agihan->agihanable : null,
        ]);
    }

    public function store($model, $modelId, Agihan $agihan, Request $request)
    {
        $validated = $request->validate([
            'content'                       => 'bail|required|string',
            'fail_dokumen_ulasan_jabatan'   => ['bail', 'sometimes', 'mimes:pdf', new TestDocLampiran],
            'fail_dokumen_sokongan'         => ['bail', 'sometimes', 'mimes:pdf', new TestDocLampiran]
        ]);

        $existedReply = $agihan->user_reply;

        if (!$existedReply) {
            $existedReply = $agihan->replies()->create([
                'content'   => $validated['content'],
                'user_id'   => auth()->user()->id
            ]);
        } else {
            $existedReply->update(collect($validated)->only(['content'])->toArray());
        }

        if (isset($validated['fail_dokumen_ulasan_jabatan']) && $validated['fail_dokumen_ulasan_jabatan']) {
            if ($existedReply->lampiranUlasanJabatan) {
                Storage::delete($existedReply->lampiranUlasanJabatan->pautan);
                $path = Storage::put(Reply::TYPE_DOKUMEN_ULASAN_JABATAN, $validated['fail_dokumen_ulasan_jabatan']);
                $existedReply->lampiranUlasanJabatan()->update([
                    'pautan'    => $path
                ]);
            } else {
                $path = Storage::put(Reply::TYPE_DOKUMEN_ULASAN_JABATAN, $validated['fail_dokumen_ulasan_jabatan']);
                $existedReply->lampiranUlasanJabatan()->create([
                    'pautan'    => $path,
                    'name'      => 'Fail Dokumen Ulasan Jabatan',
                    'jenis'     => Reply::TYPE_DOKUMEN_ULASAN_JABATAN,
                ]);
            }
        }

        if (isset($validated['fail_dokumen_sokongan']) && $validated['fail_dokumen_sokongan']) {
            if ($existedReply->lampiranSokongan) {
                Storage::delete($existedReply->lampiranSokongan->pautan);
                $path = Storage::put(Reply::TYPE_DOKUMEN_SOKONGAN, $validated['fail_dokumen_sokongan']);
                $existedReply->lampiranSokongan()->update([
                    'pautan'    => $path
                ]);
            } else {
                $path = Storage::put(Reply::TYPE_DOKUMEN_SOKONGAN, $validated['fail_dokumen_sokongan']);
                $existedReply->lampiranSokongan()->create([
                    'pautan'    => $path,
                    'name'      => 'Fail Dokumen Sokongan',
                    'jenis'     => Reply::TYPE_DOKUMEN_SOKONGAN,
                ]);
            }
        }

        session()->flash('status', [
            'type'      => 'success',
            'message'   => 'Maklum balas berjaya dihantar.'
        ]);

        return redirect()->back();
    }
}
