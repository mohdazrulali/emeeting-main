<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class CannotSetAgihan
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        if ($request->route('document')->jenis === 'nota') {
            return abort(403, 'Agihan tidak dibenarkan untuk dokumen nota.');
        }

        return $next($request);
    }
}
