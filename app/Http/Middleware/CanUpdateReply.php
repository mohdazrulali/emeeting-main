<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class CanUpdateReply
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $reply = $request->route('agihan')->user_reply;

        if ($reply && $reply->approved_by) return abort(403);

        return $next($request);
    }
}
