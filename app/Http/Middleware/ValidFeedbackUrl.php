<?php

namespace App\Http\Middleware;

use App\Models\Cabutan;
use App\Models\Document;
use App\Models\Meeting;
use Closure;
use Illuminate\Http\Request;

class ValidFeedbackUrl
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $allowedModels = ['mesyuarat', 'dokumen', 'cabutan'];

        if (!in_array($request->route('model'), $allowedModels)) abort(404);

        switch ($request->route('model')) {
            case 'mesyuarat':
                Meeting::findOrFail($request->route('model_id'));
                break;
            case 'dokumen':
                Document::findOrFail($request->route('model_id'));
                break;
            case 'cabutan':
                Cabutan::findOrFail($request->route('model_id'));
                break;
            default:
                abort(404);
                break;
        }

        return $next($request);
    }
}
