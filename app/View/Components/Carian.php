<?php

namespace App\View\Components;

use Illuminate\View\Component;

class Carian extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(
        public string $resetRoute = '#',
        public string $searchKeyword = 'carian'
    ) {}

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.carian');
    }
}
