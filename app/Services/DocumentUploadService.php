<?php

namespace App\Services;

use Exception;
use setasign\Fpdi\Fpdi;
use setasign\Fpdi\PdfParser\StreamReader;

class DocumentUploadService
{
    public function __construct()
    {
    }

    public function setWatermarkToDoc($pathToDoc, $watermarkText)
    {
        $document = new Fpdi();

        $pages_count = 0;

        try {
            $pages_count = $document->setSourceFile($pathToDoc);
        } catch (Exception $e) {
            abort(500, 'Ralat semasa memuat turun dokument.');
        }

        for ($i = 1; $i <= $pages_count; $i++) {
            $document->AddPage();
            $tplIdx = $document->importPage($i);
            $document->useTemplate($tplIdx, 0, 0);
            $document->SetFont('Times', 'B', 16);
            $document->SetTextColor(0, 0, 255);
            $this->addWatermark(105, 220, $watermarkText, 45, $document);
            $document->SetXY(25, 25);
        }

        return $document->Output();
    }

    private function addWatermark($x, $y, $watermarkText, $angle, $pdf)
    {
        $angle = $angle * M_PI / 180;
        $c = cos($angle);
        $s = sin($angle);
        $cx = $x * 1;
        $cy = (300 - $y) * 1;
        $pdf->_out(sprintf('q %.5F %.5F %.5F %.5F %.2F %.2F cm 1 0 0 1 %.2F %.2F cm', $c, $s, -$s, $c, $cx, $cy, -$cx, -$cy));
        $pdf->Text($x, $y, $watermarkText);
        $pdf->_out('Q');
    }

    public function testDoc($pathToDoc)
    {
        $document = new Fpdi();

        $file = file_get_contents($pathToDoc);

        try {
            $document->setSourceFile(StreamReader::createByString($file));
        } catch (Exception $e) {
            throw $e;
        }
    }
}
