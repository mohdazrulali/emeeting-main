<?php

namespace App\Services;

use App\Jobs\TriggerLogs;
use App\Models\User;

class ACLService
{
    private $currentUser = null;

    public function setCurrentUser(User $user)
    {
        $this->currentUser = $user;

        return $this;
    }

    public function updateUserRole(User $user, $params)
    {
        $roles = $params['roles'];

        $userRoles = $user->getRoleNames()->toArray();

        foreach($userRoles as $role) {
            if (!in_array($role, $roles)) {
                $user->removeRole($role);
            }
        }

        $user->assignRole($roles);

        dispatch(new TriggerLogs($this->currentUser, User::class, $user->id, 'updated', [
            'roles'  => $user->getRoleNames()
        ]));

        return $user->fresh();
    }

    public function resetUserRole(User $user)
    {
        $userRoles = $user->getRoleNames()->toArray();

        foreach($userRoles as $role) {
            $user->removeRole($role);
        }

        dispatch(new TriggerLogs($this->currentUser, User::class, $user->id, 'updated', [
            'roles'  => $user->getRoleNames()
        ]));

        return $user->fresh();
    }
}