<?php

namespace App\Services;

use App\Jobs\TriggerLogs;
use App\Models\User;

class UserService
{
    private $currentUser = null;

    public function setCurrentUser(User $user)
    {
        $this->currentUser = $user;

        return $this;
    }

    public function registerUser($params)
    {
        $user = User::create(collect($params)->except(['role'])->toArray());

        dispatch(new TriggerLogs($this->currentUser, User::class, $user->id, 'created'));

        $user->assignRole($params['role']);

        dispatch(new TriggerLogs($this->currentUser, User::class, $user->id, 'updated', [
            'roles'  => $user->getRoleNames()
        ]));

        return $user;
    }

    public function getUsers($params)
    {
        return User::when(isset($params['carian']), function ($q) use ($params) {
            $q->where(function ($sub) use ($params) {
                $sub->where('name', 'LIKE', '%' . $params['carian'] . '%')
                    ->orWhere('email', 'LIKE', '%' . $params['carian'] . '%')
                    ->orWhere('ic_no', 'LIKE', '%' . $params['carian'] . '%');
            });
        })
            ->whereNot('id', $this->currentUser->id)
            ->orderBy('name', 'ASC')
            ->paginate(10, ['*'], 'page', isset($params['page']) ? $params['page'] : 1);
    }

    public function updateUserProfile(User $user, $params)
    {
        $user->update($params);

        if (isset($params['password'])) {
            $maskedPassword = str_repeat('*', strlen($params['password']) - 3) . substr($params['password'], -3);
            dispatch(new TriggerLogs($this->currentUser, User::class, $user->id, 'updated', [
                'new_password'  => $maskedPassword
            ]));
        } else {
            dispatch(new TriggerLogs($this->currentUser, User::class, $user->id, 'updated', $user->getChanges()));
        }

        return $user->fresh();
    }

    public function deleteUser(User $user)
    {
        $user->delete();

        return $user->fresh();
    }
}
