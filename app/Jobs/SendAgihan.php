<?php

namespace App\Jobs;

use App\Mail\AgihanCabutan;
use App\Mail\AgihanMesyuarat;
use App\Models\Cabutan;
use App\Models\Document;
use App\Models\Meeting;
use App\Models\User;
use Error;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class SendAgihan implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    protected $penerima, $model;

    public function __construct(User $penerima, $model)
    {
        $this->penerima = $penerima;
        $this->model = $model;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if ($this->model instanceof Document) {
            // TODO from official template
            Mail::raw(sprintf('Email for agihan document #%s [%s]', $this->model->id, $this->model->name), function ($message) {
                $message->from(config('mail.from.address'), config('mail.from.name'));
                $message->to($this->penerima->email);
                $message->subject(sprintf('This is a test email from agihan document #%s [%s].', $this->model->id, $this->model->name));
            });
        }
        elseif ($this->model instanceof Meeting) {
            Mail::to($this->penerima)->send(new AgihanMesyuarat($this->model));
        }
        elseif ($this->model instanceof Cabutan) {
            Mail::to($this->penerima)->send(new AgihanCabutan($this->model));
        }
        else {
            throw new Error("Invalid model provided.");
        }
    }
}
