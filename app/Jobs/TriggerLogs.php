<?php

namespace App\Jobs;

use App\Models\AuditLog;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class TriggerLogs implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    public $user, $loggableType, $loggableId, $event, $changes;

    public function __construct($user, $loggableType, $loggableId, $event, $changes = [])
    {

        $this->user = $user;
        $this->loggableType = $loggableType;
        $this->loggableId = $loggableId;
        $this->event = $event;
        $this->changes = $changes;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        // dd($this->event,$this->user->id,$this->loggableType,$this->loggableId);
        AuditLog::create([
            'event'     => $this->event,
            'performed_by'   => $this->user->id,
            'loggable_type' => $this->loggableType,
            'loggable_id'   => $this->loggableId,
            'changes'           =>  json_encode(collect($this->changes)->except(['updated_at'])->toArray())
        ]);
    }
}
