<?php

namespace App\Rules;

use App\Services\DocumentUploadService;
use Exception;
use Illuminate\Contracts\Validation\InvokableRule;

class TestDocLampiran implements InvokableRule
{
    /**
     * Run the validation rule.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @param  \Closure(string): \Illuminate\Translation\PotentiallyTranslatedString  $fail
     * @return void
     */
    public function __invoke($attribute, $value, $fail)
    {
        try {
            (new DocumentUploadService())->testDoc($value);
        } catch (Exception $e) {
            $fail('Fail lampiran dokumen tidak sah. Sila pilih fail lain.');
        }
    }
}
