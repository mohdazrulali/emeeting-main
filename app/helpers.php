<?php


use App\Models\{
    Agihan,
    Cabutan,
    Document,
    Meeting,
    User,
    Reply
};

if (!function_exists('getNavbarMenu')) {
    function getNavbarMenu()
    {
        return [
            // [
            //     'label' => 'Dashboard',
            //     'route' => route('dashboard'),
            //     'access'    => ['view-dashboard']
            // ],
            [
                'label' => 'Senarai Dokumen',
                'route' => '#',
                'access'    => ['view-documents'],
                'sub_menu'  => [
                    [
                        'label' => 'Memo KPT',
                        'route' => route('documents.index', ['jenis' => 'memo-kpkt']),
                        'access'    => ['view-documents-memo-kpkt']
                    ],
                    [
                        'label' => 'Memo Kementerian Lain',
                        'route' => route('documents.index', ['jenis' => 'kementerian-lain']),
                        'access'    => ['view-documents-kementerian-lain']
                    ],
                    [
                        'label' => 'Nota',
                        'route' => route('documents.index', ['jenis' => 'nota']),
                        'access'    => ['view-documents-nota']
                    ]
                    // null,
                    // [
                    //     'label' => 'Mesyuarat Jemaah Menteri',
                    //     'route' => route('meetings.index', ['jenis' => 'mesyuarat-jemaah-menteri']),
                    //     'access'    => ['view-meetings-mesyuarat-jemaah-menteri']
                    // ],
                    // [
                    //     'label' => 'Mesyuarat Utama',
                    //     'route' => route('meetings.index', ['jenis' => 'mesyuarat-utama']),
                    //     'access'    => ['view-meetings-mesyuarat-utama']
                    // ]
                ]
            ],
            [
                'label' => 'Senarai Mesyuarat',
                'route' => '#',
                'access'    => ['view-documents'],
                'sub_menu'  => [
                    [
                        'label' => 'Mesyuarat Jemaah Menteri',
                        'route' => route('meetings.index', ['jenis' => 'mesyuarat-jemaah-menteri']),
                        'access'    => ['view-meetings-mesyuarat-jemaah-menteri']
                    ],
                    [
                        'label' => 'Mesyuarat Utama',
                        'route' => route('meetings.index', ['jenis' => 'mesyuarat-utama']),
                        'access'    => ['view-meetings-mesyuarat-utama']
                    ]
                ]
            ],
            [
                'label' => 'Maklum Balas',
                'route' => route('reply.index'),
                'access'    => ['view-maklum-balas']
            ],
            [
                'label' => 'Pengguna Sistem',
                'route' => route('users.index'),
                'access'    => ['view-users']
            ],
            [
                'label' => 'Jabatan/Kementerian',
                'route' => route('departments.index'),
                'access'    => ['view-departments']
            ],
            [
                'label' => 'Kawalan Capaian',
                'route' => route('acls.index'),
                'access'    => ['view-acls']
            ],
            [
                'label' => 'Log Audit',
                'route' => '#',
                'access'    => ['view-log-audit'],
                'sub_menu'  => [
                    [
                        'label' => 'Pengguna',
                        'route' => route('logaudit.pengguna.index'),
                        'access'    => ['view-activiti-log-pengguna']
                    ],
                    [
                        'label' => 'Dokumen',
                        'route' => route('logaudit.dokumen.index'),
                        'access'    => ['view-documents-trail']
                    ],
                ]
            ],
        ];
    }
}

if (!function_exists('routeIsActive')) {
    function routeIsActive($routePath)
    {
        if ($routePath === '#') {
            return false;
        }

        if (isset(parse_url($routePath)['query'])) {
            return url()->full() === $routePath;
        }

        return request()->is(substr(parse_url($routePath)['path'], 1) . '*');
    }
}

if (!function_exists('parseDocumentLog')) {
    function parseDocumentLog($log)
    {
        $changes = json_decode($log->changes);

        $string = json_encode($log);

        switch ($log->event) {
            case 'created':
                $string = sprintf('<code>%s</code> telah mendaftar dokumen baru: <code>%s (%s)</code>.', $log->user->name, $log->loggable->name, parseDocumentType($log->loggable->jenis));
                break;
            case 'updated':
                if (property_exists($changes, 'lampiran')) {
                    if ($changes->event === 'created') {
                        $string = sprintf('<code>%s</code> telah menambah lampiran: <code>%s</code> pada dokumen: <code>%s (%s)</code>.', $log->user->name, $changes->lampiran->name, $log->loggable->name, parseDocumentType($log->loggable->jenis));
                        break;
                    }

                    if ($changes->event === 'deleted') {
                        $string = sprintf('<code>%s</code> telah menghapus lampiran: <code>%s</code> pada dokumen: <code>%s (%s)</code>.', $log->user->name, $changes->lampiran->name, $log->loggable->name, parseDocumentType($log->loggable->jenis));
                        break;
                    }
                }

                if (property_exists($changes, 'agihan')) {
                    if (property_exists($changes, 'event')) {
                        if ($changes->event === 'created') {
                            $string = sprintf('<code>%s</code> telah menambah/menghantar agihan kepada pengguna: <code>%s</code> bagi dokumen: <code>%s (%s)</code>.',$log->user->name, $changes->agihan->penerima, $log->loggable->name, parseDocumentType($log->loggable->jenis));
                            break;
                        }

                        if ($changes->event === 'updated') {
                            $string = sprintf('<code>%s</code> telah menambah/menghantar semula agihan kepada pengguna: <code>%s</code> bagi dokumen: <code>%s (%s)</code>.', $log->user->name, $changes->agihan->penerima, $log->loggable->name, parseDocumentType($log->loggable->jenis));
                            break;
                        }
                    } elseif (property_exists($changes->agihan, 'event')) {
                        if ($changes->agihan->event === 'created') {
                            $string = sprintf('<code>%s</code> telah menambah <a href="#" data-bs-toggle="tooltip" data-bs-placement="bottom" title="%s">maklum balas</a> agihan bagi dokumen: <code>%s (%s)</code>.', $log->user->name, $changes->agihan->reply->content, $log->loggable->name, parseDocumentType($log->loggable->jenis));
                            break;
                        }
                    }
                }

                $string = sprintf('<code>%s</code> telah mengemaskini dokumen: <code>%s (%s)</code>.', $log->user->name, $log->loggable->name, parseDocumentType($log->loggable->jenis));
                break;
            case 'deleted':
                $string = sprintf('<code>%s</code> telah menghapus dokumen: <code>%s (%s)</code>.', $log->user->name, $log->loggable->name, parseDocumentType($log->loggable->jenis));
                break;
            default:
                break;
        }

        return $string;
    }
}

if (!function_exists('parseMeetingLog')) {
    function parseMeetingLog($log)
    {
        $changes = json_decode($log->changes);

        $string = json_encode($log);

        switch ($log->event) {
            case 'created':
                $string = sprintf('<code>%s</code> telah mendaftar dokumen mesyuarat baru: <code>%s (%s)</code>.', $log->user->name, $log->loggable->name, parseDocumentType($log->loggable->jenis));
                break;
            case 'updated':
                if (property_exists($changes, 'lampiran')) {
                    if ($changes->event === 'created') {
                        $string = sprintf('<code>%s</code> telah menambah lampiran: <code>%s</code> pada dokumen mesyuarat: <code>%s (%s)</code>.', $log->user->name, $changes->lampiran->name, $log->loggable->name, parseDocumentType($log->loggable->jenis));
                        break;
                    }

                    if ($changes->event === 'deleted') {
                        $string = sprintf('<code>%s</code> telah menghapus lampiran: <code>%s</code> pada dokumen mesyuarat: <code>%s (%s)</code>.', $log->user->name, $changes->lampiran->name, $log->loggable->name, parseDocumentType($log->loggable->jenis));
                        break;
                    }
                }

                if (property_exists($changes, 'agihan')) {
                    if ($changes->event === 'created') {
                        $string = sprintf('<code>%s</code> telah menambah/menghantar agihan kepada pengguna: <code>%s</code> bagi dokumen mesyuarat: <code>%s (%s)</code>.', $log->user->name, $changes->agihan->penerima, $log->loggable->name, parseDocumentType($log->loggable->jenis));
                        break;
                    }

                    if ($changes->event === 'updated') {
                        $string = sprintf('<code>%s</code> telah menambah/menghantar semula agihan kepada pengguna: <code>%s</code> bagi dokumen mesyuarat: <code>%s (%s)</code>.', $log->user->name, $changes->agihan->penerima, $log->loggable->name, parseDocumentType($log->loggable->jenis));
                        break;
                    }

                    if ($changes->event === 'deleted') {
                        $string = sprintf('<code>%s</code> telah membatalkan agihan kepada pengguna: <code>%s</code> bagi dokumen mesyuarat: <code>%s (%s)</code>.', $log->user->name, $changes->agihan->penerima, $log->loggable->name, parseDocumentType($log->loggable->jenis));
                        break;
                    }
                }

                if (property_exists($changes, 'cabutan')) {
                    if (property_exists($changes->cabutan, 'agihan')) {
                        if ($changes->event === 'created') {
                            $string = sprintf('<code>%s</code> telah menambah/menghantar agihan dokumen kepada pengguna: <code>%s</code> bagi dokumen mesyuarat: <code>%s (%s)</code>.', $log->user->name, $changes->agihan->penerima, $log->loggable->name, parseDocumentType($log->loggable->jenis));
                            break;
                        }

                        if ($changes->event === 'updated') {
                            $string = sprintf('<code>%s</code> telah menambah/menghantar semula agihan dokumen kepada pengguna: <code>%s</code> bagi dokumen mesyuarat: <code>%s (%s)</code>.', $log->user->name, $changes->agihan->penerima, $log->loggable->name, parseDocumentType($log->loggable->jenis));
                            break;
                        }

                        if ($changes->event === 'deleted') {
                            $string = sprintf('<code>%s</code> telah membatalkan agihan dokumen kepada pengguna: <code>%s</code> bagi dokumen mesyuarat: <code>%s (%s)</code>.', $log->user->name, $changes->agihan->penerima, $log->loggable->name, parseDocumentType($log->loggable->jenis));
                            break;
                        }
                    }

                    if ($changes->event === 'created') {
                        $string = sprintf('<code>%s</code> telah menambah <a href="#" data-bs-toggle="tooltip" data-bs-placement="bottom" title="%s">cabutan</a> dokumen mesyuarat: <code>%s (%s)</code>.', $log->user->name, $changes->cabutan->content, $log->loggable->name, parseDocumentType($log->loggable->jenis));
                        break;
                    }

                    if ($changes->event === 'updated') {
                        $string = sprintf('<code>%s</code> telah mengemaskini <a href="#" data-bs-toggle="tooltip" data-bs-placement="bottom" title="%s">cabutan</a> dokumen mesyuarat: <code>%s (%s)</code>.', $log->user->name, $changes->cabutan->content, $log->loggable->name, parseDocumentType($log->loggable->jenis));
                        break;
                    }

                    if ($changes->event === 'deleted') {
                        $string = sprintf('<code>%s</code> telah menghapus <a href="#" data-bs-toggle="tooltip" data-bs-placement="bottom" title="%s">cabutan</a> dokumen mesyuarat: <code>%s (%s)</code>.', $log->user->name, $changes->cabutan->content, $log->loggable->name, parseDocumentType($log->loggable->jenis));
                        break;
                    }
                }

                $string = sprintf('<code>%s</code> telah mengemaskini dokumen mesyuarat: <code>%s (%s)</code>.', $log->user->name, $log->loggable->name, parseDocumentType($log->loggable->jenis));
                break;
            case 'deleted':
                $string = sprintf('<code>%s</code> telah menghapus dokumen mesyuarat: <code>%s (%s)</code>.', $log->user->name, $log->loggable->name, parseDocumentType($log->loggable->jenis));
                break;
            default:
                break;
        }

        return $string;
    }
}

if (!function_exists('parseUserAccountLog')) {
    function parseUserAccountLog($log)
    {
        $changes = json_decode($log->changes);

        $string = json_encode($log);

        switch ($log->event) {
            case 'created':
                $string = sprintf('<code>%s</code> telah mendaftar akaun pengguna baru: <code>%s</code>.', $log->user->name, $log->loggable->name);
                break;
            case 'updated':
                if (property_exists($changes, 'new_password')) {
                    $string = sprintf('<code>%s</code> telah mengemaskini kata laluan pengguna: <code>%s</code>.', $log->user->name, $log->loggable->name);
                    break;
                }

                if (property_exists($changes, 'logged_in_at')) {
                    $string = sprintf('<code>%s</code> telah log masuk pada <code>%s</code>.', $log->user->name, date('d/m/Y h:i a', strtotime($changes->logged_in_at)));
                    break;
                }

                if (property_exists($changes, 'logged_out_at')) {
                    $string = sprintf('<code>%s</code> telah log keluar pada <code>%s</code>.', $log->user->name, date('d/m/Y h:i a', strtotime($changes->logged_out_at)));
                    break;
                }
                if (property_exists($changes, 'roles')) {
                    if (count($changes->roles) === 0) {
                        $string = sprintf('<code>%s</code> telah set semula peranan pengguna: <code>%s</code>.', $log->user->name, $log->loggable->name);
                        break;
                    }
                    $string = sprintf('<code>%s</code> telah mengemaskini peranan: <code>%s</code> kepada pengguna: <code>%s</code>.', $log->user->name, implode(', ', $changes->roles), $log->loggable->name);
                    break;
                }

                $string = sprintf('<code>%s</code> telah mengemaskini profile pengguna: <code>%s</code>.', $log->user->name, $log->loggable->name);
                break;
            case 'deleted':
                $string = sprintf('<code>%s</code> telah menghapus akaun pengguna: <code>%s</code>.', $log->user->name, $log->loggable->name);
                break;
            default:
                break;
        }

        return $string;
    }
}

if (!function_exists('parseUserActivityLog')) {
    function parseUserActivityLog($log)
    {
        if ($log->loggable_type === User::class) {
            return parseUserAccountLog($log);
        }

        if ($log->loggable_type === Document::class) {
            return parseDocumentLog($log);
        }

        if ($log->loggable_type === Meeting::class) {
            return parseMeetingLog($log);
        }

        if ($log->loggable_type === Reply::class) {
            $changes = json_decode($log->changes);

            if ($changes->event === 'deleted') {
                return sprintf(
                    '<code>%s</code> telah menghapus lampiran <a href="#" data-bs-toggle="tooltip" data-bs-placement="bottom" title="%s">maklum balas</a>.',
                    $log->user->name,
                    $log->loggable->content,
                );
            }

            if ($changes->event === 'created') {
                return sprintf(
                    '<code>%s</code> telah menambah lampiran <a href="#" data-bs-toggle="tooltip" data-bs-placement="bottom" title="%s">maklum balas</a>.',
                    $log->user->name,
                    $log->loggable->content,
                );
            }
        }
    }
}

if (!function_exists('parseDocumentType')) {
    function parseDocumentType($slug)
    {
        $string = '[TIADA JENIS]';

        switch ($slug) {
            case 'memo-kpkt':
                $string = 'Memo KPKT';
                break;
            case 'kementerian-lain':
                $string = 'Memo Kementerian Lain';
                break;
            case 'nota':
                $string = 'Nota';
                break;
            case 'mesyuarat-jemaah-menteri':
                $string = 'Mesyuarat Jemaah Menteri';
                break;
            case 'mesyuarat-utama':
                $string = 'Mesyuarat Utama';
                break;
            default:
                break;
        }

        return $string;
    }
}

if (!function_exists('generateFeedbackUrl')) {
    function generateFeedbackUrl($routeName, Agihan $item, $extrasParams = [])
    {
        $route = "#";
        switch ($item->agihanable_type) {
            case Document::class:
                $route = route($routeName, [
                    'model' => 'dokumen',
                    'model_id' => $item->agihanable->id,
                    'agihan' => $item->id,
                    ...$extrasParams
                ]);
                break;
            case Meeting::class:
                $route = route($routeName, [
                    'model' => 'mesyuarat',
                    'model_id' => $item->agihanable->id,
                    'agihan' => $item->id,
                    ...$extrasParams
                ]);
                break;
            case Cabutan::class:
                $route = route($routeName, [
                    'model' => 'cabutan',
                    'model_id' => $item->agihanable->id,
                    'agihan' => $item->id,
                    ...$extrasParams
                ]);
                break;
            default:
                break;
        }

        return $route;
    }
}
