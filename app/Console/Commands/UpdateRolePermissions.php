<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class UpdateRolePermissions extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'role-permissions:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Kemaskini acl peranan dan akses modul.';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        printf($this->description);
        $this->call('db:seed', [
            '--class' => 'RolePermissionSeeder'
        ]);
    }
}
