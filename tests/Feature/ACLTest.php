<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ACLTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */

    protected function setUp(): void
    {
        parent::setUp();
        $this->artisan('db:seed --class=RolePermissionSeeder');
    }


    /**
     * @test
     * @dataProvider updateRoleData
     */
    public function testUpdateRole($expectedResult, $input)
    {
        $user = User::factory()->create();

        $expectedUser = app('ACLService')->updateUserRole($user, [
            'roles' => $input
        ]);

        $this->assertCount($expectedResult, $expectedUser->getRoleNames()->toArray());
    }

    public function updateRoleData()
    {
        return [
            'assign user to single role' => [
                'total_roles'   => 2,
                'roles_names'   => ['Pentadbir Sistem', 'Ketua Jabatan']
            ],
            'assign user to multiple roles' => [
                'total_roles'   => 1,
                'roles_names'   => ['Pentadbir Sistem']
            ]
        ];
    }

    /**
     * @test
     * @dataProvider updateAccessData
     */
    public function testUpdateAccess($expectedResult, $input)
    {
        $user = User::factory()->create();

        $user->assignRole($input);

        $expectedUser = app('ACLService')->resetUserRole($user);

        $this->assertCount($expectedResult, $expectedUser->getRoleNames()->toArray());
    }

    public function updateAccessData()
    {
        return [
            'reset single user role' => [
                'total_roles'   => 0,
                'roles_names'   => ['Pentadbir Sistem']
            ],
            'reset multiple user role' => [
                'total_roles'   => 0,
                'roles_names'   => ['Pentadbir Sistem', 'Ketua Jabatan']
            ]
        ];
    }
}
