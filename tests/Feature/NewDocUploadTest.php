<?php

namespace Tests\Feature;

use App\Models\Ministry;
use App\Models\User;
// use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\UploadedFile;
use Tests\TestCase;

class NewDocUploadTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testNewDocUpload()
    {
        $user = User::find(1);

        $response = $this->actingAs($user)->post(route('documents.store'), [
            'name'                      => '[name]',
            'sub_name'                  => '[sub_name]',
            'tarikh_dokumen'            => now(),
            'no_fail_kementerian'       => '[no_fail_kementerian]',
            'salinan_bilangan'          => '1',
            'jenis'                     => 'memo-kpkt',
            'kementerian'               => Ministry::get()->first()->id,
            'nama_lampiran_mesyuarat'   => '[nama_lampiran_mesyuarat]',
            'fail_lampiran_mesyuarat'   => [$this->generateFileTestFile('fail_lampiran_mesyuarat_1'), $this->generateFileTestFile('fail_lampiran_mesyuarat_2')],
            'nama_lampiran_lain'        => '[nama_lampiran_lain]',
            'fail_lampiran_lain'        => [$this->generateFileTestFile('fail_lampiran_lain_1'), $this->generateFileTestFile('fail_lampiran_lain_2')],
            'nama_lampiran_ringkasan'   => '[nama_lampiran_ringkasan]',
            'fail_lampiran_ringkasan'   => [$this->generateFileTestFile('fail_lampiran_ringkasan_1'), $this->generateFileTestFile('fail_lampiran_ringkasan_2')],
            'nama_lampiran_tambahan'    => '[nama_lampiran_tambahan]',
            'fail_lampiran_tambahan'    => [$this->generateFileTestFile('fail_lampiran_tambahan_1'), $this->generateFileTestFile('fail_lampiran_tambahan_2')],
        ]);

        $response->assertSessionHasNoErrors();
    }

    public function generateFileTestFile(string $fileName) {
        return UploadedFile::fake()->create($fileName . '.pdf', 1024);
    }
}
