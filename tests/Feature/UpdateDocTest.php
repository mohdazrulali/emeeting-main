<?php

namespace Tests\Feature;

use App\Models\Document;
use App\Models\Ministry;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\UploadedFile;
use Tests\TestCase;

class UpdateDocTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testUpdateDoc()
    {
        $user = User::find(1);

        $existingDoc = Document::create([
            'name'                  => 'Test nama dokumen',
            'sub_name'              => 'Test sub nama',
            'no_fail_kementerian'   => 'Test no fail kementerian',
            'tarikh_dokumen'        => now(),
            'salinan_bilangan'      => 1,
            'jenis'                 => 'memo-kpkt',
            'ministry_id'           => Ministry::first()->id,
            'created_by'            => $user->id
        ]);

        $response = $this->actingAs($user)->put(route('documents.update', $existingDoc->id), [
            'name'                  => 'Test update nama dokumen',
            'sub_name'              => 'Test update sub nama',
            'no_fail_kementerian'   => 'Test update no fail kementerian',
            'tarikh_dokumen'        => now()->addYear(),
            'salinan_bilangan'      => "100",
            'jenis'                 => 'memo-kpkt',
            'ministry_id'           => Ministry::orderBy('id', 'DESC')->first()->id,
        ]);

        $response->assertSessionHasNoErrors();

        $existingDoc->delete();
    }

    public function generateFileTestFile(string $fileName)
    {
        return UploadedFile::fake()->create($fileName . '.pdf', 1024);
    }
}
