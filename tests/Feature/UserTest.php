<?php

namespace Tests\Feature;

use App\Jobs\TriggerLogs;
use App\Models\Department;
use App\Models\Role;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Bus;
use Tests\TestCase;

class UserTest extends TestCase
{

    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */

    public function setUp(): void
    {
        parent::setUp();
        $this->artisan('db:seed --class=DepartmentSeeder');
        $this->artisan('db:seed --class=RolePermissionSeeder');
        $authorizedUser = User::factory()->create([
            'department_id' => Department::first()->id
        ]);
        $authorizedUser->assignRole(['Pentadbir']);
        auth()->login($authorizedUser);
    }

    public function testRegisterUser()
    {
        $department = Department::factory()->create();

        $user = app('UserService')->setCurrentUser(auth()->user())->registerUser([
            'name'          => 'Test user',
            'email'         => 'testuser@email.com',
            'password'      => 'Abcd1234',
            'designation'   => 'Pegawai',
            'ic_no'         => '000000000000',
            'phone'         => '0000000000',
            'mobile'        => '0000000000',
            'department_id' => $department->id,
            'role'          => Role::PERANAN_PENTADBIR_SISTEM
        ]);

        $this->assertModelExists($user);
        $this->assertAuditLogCount(2);
    }

    public function testGetUsers()
    {
        User::factory(10)->create([
            'department_id' => Department::first()->id
        ]);

        $user = User::factory()->create([
            'department_id' => Department::first()->id
        ]);

        $users = app('UserService')->setCurrentUser($user)->getUsers([
            'page'  => 1,
        ]);

        $this->assertCount(10, $users);
    }

    public function testUpdateUserProfile()
    {
        $user = User::factory()->create();

        $userName = $user->name;

        app('UserService')->setCurrentUser(auth()->user())->updateUserProfile($user, [
            'name'  => 'Test Update Name',
        ]);

        $this->assertNotEquals($user->name, $userName);
        $this->assertAuditLogCount(1);
    }

    public function testDeleteUser()
    {
        $user = User::factory()->create();

        app('UserService')->deleteUser($user);

        $this->assertModelMissing($user);
        $this->assertAuditLogCount(1);
    }

    public function testUpdateUserPassword()
    {
        $user = User::factory()->create();

        $newPassword = 'Abcd1234';

        $updatedUser = app('UserService')->setCurrentUser(auth()->user())->updateUserProfile($user, [
            'password'  => $newPassword,
        ]);

        auth()->attempt(['email'  => $updatedUser, 'password' => $newPassword], false);

        $this->assertAuthenticated();
        $this->assertAuditLogCount(1);
    }
}
