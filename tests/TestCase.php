<?php

namespace Tests;

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    public function assertAuditLogCount($expectedCount = 0)
    {
        $this->assertDatabaseCount('audit_logs', $expectedCount);
    }
}
