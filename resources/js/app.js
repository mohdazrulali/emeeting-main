import './bootstrap';
window.http = async (url = "/", method = "GET", data = {}) => {
    try {
        return await window.axios({
            url,
            method,
            data,
        });
    } catch (error) {
        throw error;
    }
};
