@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    Kemaskini @if (request()->jenis == 'memo-kpkt')
                    Memo KPKT
                    @elseif (request()->jenis == 'kementerian-lain')
                    Memo Kementerian Lain
                    @elseif (request()->jenis == 'nota')
                    Nota
                    @else
                    [Tiada Jenis]
                    @endif
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12 d-flex justify-content-between">
                            <ul class="nav nav-pills" role="tablist">
                                <li class="nav-item" role="presentation">
                                    <a class="nav-link active" role="tab">1. Maklumat @if (request()->jenis == 'memo-kpkt')
                                        Memo KPKT
                                        @elseif (request()->jenis == 'kementerian-lain')
                                        Memo Kementerian Lain
                                        @elseif (request()->jenis == 'nota')
                                        Nota
                                        @else
                                        [Tiada Jenis]
                                        @endif</a>
                                </li>
                            </ul>
                            <div>
                                <a type="button" class="btn btn-outline-primary" href="{{ route('documents.index', ['jenis' => request()->jenis]) }}">
                                    <i class="fa-solid fa-angle-left" style="margin-right: 8px;"></i>Kembali
                                </a>
                            </div>
                        </div>

                        <div class="col-md-12 mt-2">
                            @if (session('status'))
                            <div class="alert alert-{{ session('status.type') }}" role="alert">
                                {{ session('status.message') }}
                            </div>
                            @endif


                            @if ($errors->any())
                            <div class="alert alert-danger mb-0">
                                <ul style="margin-bottom: 0;">
                                    @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                            @endif
                        </div>

                        <div class="col-md-12 mt-2">
                            <div class="tab-content">
                                <div class="tab-pane active" role="tabpanel">
                                    <div class="card">
                                        <div class="card-header">Kegunaan Pejabat</div>
                                        <div class="card-body row">
                                            <div class="col">Tarikh & Masa: {{ now()->format('d-m-Y H:i') }}
                                            </div>
                                            <div class="col">Disediakan Oleh: {{ auth()->user()->name }}</div>
                                        </div>
                                    </div>
                                    <div class="card mt-2">
                                        <div class="card-body">
                                            <form action="{{ route('documents.store', request()->jenis) }}" method="POST" autocomplete="off">
                                                @csrf
                                                <input type="hidden" name="jenis" value="{{ request()->jenis }}">
                                                <div class="form-group row mb-2">
                                                    <div class="col-3">Nombor File Kementerian</div>
                                                    <div class="col">
                                                        <input type="text" name="no_fail_kementerian" placeholder="No File Kementerian" class="form-control @error('no_fail_kementerian') is-invalid @enderror" value="{{ old('no_fail_kementerian') }}">

                                                        @error('no_fail_kementerian')
                                                        <div class="invalid-feedback">{{ $message }}</div>
                                                        @enderror
                                                    </div>
                                                </div>

                                                <div class="form-group row mb-2">
                                                    <div class="col-3">Salinan Bilangan</div>
                                                    <div class="col">
                                                        <input type="text" name="salinan_bilangan" placeholder="Salinan Bilangan" value="{{ old('salinan_bilangan') }}" class="form-control @error('salinan_bilangan') is-invalid @enderror">
                                                        @error('salinan_bilangan')
                                                        <div class="invalid-feedback">{{ $message }}</div>
                                                        @enderror
                                                    </div>
                                                </div>

                                                <div class="form-group row mb-2">
                                                    <div class="col-3">Tajuk Dokumen</div>
                                                    <div class="col">
                                                        <input type="text" name="name" placeholder="Tajuk Dokumen" value="{{ old('name') }}" class="form-control @error('name') is-invalid @enderror">
                                                        @error('name')
                                                        <div class="invalid-feedback">{{ $message }}</div>
                                                        @enderror
                                                    </div>
                                                </div>

                                                <div class="form-group row mb-2">
                                                    <div class="col-3">Sub Tajuk Dokumen</div>
                                                    <div class="col">
                                                        <input type="text" name="sub_name" placeholder="Sub Tajuk Dokumen" value="{{ old('sub_name') }}" class="form-control @error('sub_name') is-invalid @enderror">
                                                        @error('sub_name')
                                                        <div class="invalid-feedback">{{ $message }}</div>
                                                        @enderror
                                                    </div>
                                                </div>

                                                @if(request()->jenis !== 'memo-kpkt')
                                                <div class="form-group row mb-2">
                                                    <div class="col-3">Jabatan/Kementerian</div>
                                                    <div class="col">
                                                        <select name="department_id" class="form-select">
                                                            <option value="">Sila pilih...</option>
                                                            @foreach ($departments as $department)
                                                            <option value="{{ $department->id }}" @if(old('department_id') && old('department_id') == $department->id) selected  @endif>
                                                                {{ $department->name }}
                                                                ({{ $department->shortcode }})
                                                            </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                @endif

                                                <div class="form-group row mb-2">
                                                    <div class="col-3">Tarikh Dokumen</div>
                                                    <div class="col">
                                                        <input type="date" name="tarikh_dokumen" placeholder="Tarikh Dokumen" value="{{ old('tarikh_dokumen') }}" class="form-control @error('tarikh_dokumen') is-invalid @enderror">
                                                        @error('tarikh_dokumen')
                                                        <div class="invalid-feedback">{{ $message }}</div>
                                                        @enderror
                                                    </div>
                                                </div>

                                                <div class="form-group mt-2 d-flex justify-content-end">
                                                    <button type="submit" class="btn btn-primary">Simpan <i class="fa-regular fa-floppy-disk" style="margin-left: 8px;"></i></button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection