@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    Kemaskini @if (request()->jenis == 'memo-kpkt')
                    Memo KPKT
                    @elseif (request()->jenis == 'kementerian-lain')
                    Memo Kementerian Lain
                    @elseif (request()->jenis == 'nota')
                    Nota
                    @else
                    [Tiada Jenis]
                    @endif
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12 d-flex justify-content-between">
                            <ul class="nav nav-pills" role="tablist">
                                <li class="nav-item" role="presentation">
                                    <a class="nav-link @if(request()->query('step') === '1') active disabled @endif" href="{{ route('documents.edit', ['step' => 1, 'jenis' => request()->jenis, 'document' => request()->document]) }}" role="tab">1. Maklumat @if (request()->jenis == 'memo-kpkt')
                                        Memo KPKT
                                        @elseif (request()->jenis == 'kementerian-lain')
                                        Memo Kementerian Lain
                                        @elseif (request()->jenis == 'nota')
                                        Nota
                                        @else
                                        [Tiada Jenis]
                                        @endif</a>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <a class="nav-link @if(request()->query('step') === '2') active disabled @endif" href="{{ route('documents.edit', ['step' => 2, 'jenis' => request()->jenis, 'document' => request()->document]) }}" role="tab">2. Maklumat Lampiran</a>
                                </li>
                                @if($document->jenis != 'nota')
                                <li class="nav-item" role="presentation">
                                    <a class="nav-link @if(request()->query('step') === '3') active disabled @endif" href="{{ route('documents.edit', ['step' => 3, 'jenis' => request()->jenis, 'document' => request()->document]) }}" role="tab">3. Maklumat Agihan</a>
                                </li>
                                @endif
                            </ul>
                            @switch(request()->query('step'))
                            @case('1')
                            <div>
                                <a type="button" class="btn btn-outline-primary" href="{{ route('documents.index', ['jenis' => request()->jenis]) }}">
                                    <i class="fa-solid fa-angle-left" style="margin-right: 8px;"></i>Kembali
                                </a>
                                <a type="button" class="btn btn-primary" href="{{ route('documents.edit', ['document' => request()->document, 'jenis' => request()->jenis, 'step' => 2]) }}">
                                    Seterusnya<i class="fa-solid fa-angle-right" style="margin-left: 8px;"></i>
                                </a>
                            </div>
                            @break
                            @case('2')
                            <div>
                                <a type="button" class="btn btn-outline-primary" href="{{ route('documents.edit', ['jenis' => request()->jenis, 'document' => request()->document, 'step' => 1]) }}">
                                    <i class="fa-solid fa-angle-left" style="margin-right: 8px;"></i>Kembali
                                </a>
                                <a type="button" class="btn btn-primary" href="{{ route('documents.edit', ['jenis' => request()->jenis, 'document' => request()->document, 'step' => 3]) }}">
                                    Seterusnya<i class="fa-solid fa-angle-right" style="margin-left: 8px;"></i>
                                </a>
                            </div>
                            @break
                            @case('3')
                            <div>
                                <a type="button" class="btn btn-outline-primary" href="{{ route('documents.edit', ['jenis' => request()->jenis, 'document' => request()->document, 'step' => 2]) }}">
                                    <i class="fa-solid fa-angle-left" style="margin-right: 8px;"></i>Kembali
                                </a>
                                <a type="button" class="btn btn-primary" href="{{ route('documents.index', ['jenis' => request()->jenis]) }}">
                                    Selesai<i class="fa-solid fa-check" style="margin-left: 8px;"></i>
                                </a>
                            </div>
                            @break
                            @endswitch
                        </div>

                        <div class="col-md-12 mt-2">
                            @if (session('status'))
                            <div class="alert alert-{{ session('status.type') }}" role="alert">
                                {{ session('status.message') }}
                            </div>
                            @endif


                            @if ($errors->any())
                            <div class="alert alert-danger mb-0">
                                <ul style="margin-bottom: 0;">
                                    @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                            @endif
                        </div>

                        <div class="col-md-12 mt-2">
                            <div class="tab-content">
                                <div class="tab-pane @if(request()->query('step') === '1') active @endif" role="tabpanel">
                                    <div class="card">
                                        <div class="card-header">Kegunaan Pejabat</div>
                                        <div class="card-body row">
                                            <div class="col">Tarikh & Masa: {{ now()->format('d-m-Y H:i') }}
                                            </div>
                                            <div class="col">Disediakan Oleh: {{ auth()->user()->name }}</div>
                                        </div>
                                    </div>
                                    <div class="card mt-2">
                                        <div class="card-body">
                                            <form action="{{ route('documents.update', ['jenis' => request()->jenis, request()->document]) }}" method="POST" autocomplete="off">
                                                @method('PUT')
                                                @csrf
                                                <input type="hidden" name="jenis" value="{{ request()->jenis }}">
                                                <div class="form-group row mb-2">
                                                    <div class="col-3">Nombor File Kementerian</div>
                                                    <div class="col">
                                                        <input type="text" name="no_fail_kementerian" placeholder="No File Kementerian" class="form-control @error('no_fail_kementerian') is-invalid @enderror" value="{{ old('no_fail_kementerian') ?? $document->no_fail_kementerian }}">

                                                        @error('no_fail_kementerian')
                                                        <div class="invalid-feedback">{{ $message }}</div>
                                                        @enderror
                                                    </div>
                                                </div>

                                                <div class="form-group row mb-2">
                                                    <div class="col-3">Salinan Bilangan</div>
                                                    <div class="col">
                                                        <input type="text" name="salinan_bilangan" placeholder="Salinan Bilangan" value="{{ old('salinan_bilangan') ?? $document->salinan_bilangan }}" class="form-control @error('salinan_bilangan') is-invalid @enderror">
                                                        @error('salinan_bilangan')
                                                        <div class="invalid-feedback">{{ $message }}</div>
                                                        @enderror
                                                    </div>
                                                </div>

                                                <div class="form-group row mb-2">
                                                    <div class="col-3">Tajuk Dokumen</div>
                                                    <div class="col">
                                                        <input type="text" name="name" placeholder="Tajuk Dokumen" value="{{ old('name') ?? $document->name }}" class="form-control @error('name') is-invalid @enderror">
                                                        @error('name')
                                                        <div class="invalid-feedback">{{ $message }}</div>
                                                        @enderror
                                                    </div>
                                                </div>

                                                <div class="form-group row mb-2">
                                                    <div class="col-3">Sub Tajuk Dokumen</div>
                                                    <div class="col">
                                                        <input type="text" name="sub_name" placeholder="Sub Tajuk Dokumen" value="{{ old('sub_name') ?? $document->sub_name }}" class="form-control @error('sub_name') is-invalid @enderror">
                                                        @error('sub_name')
                                                        <div class="invalid-feedback">{{ $message }}</div>
                                                        @enderror
                                                    </div>
                                                </div>
                                                @if(request()->jenis !== 'memo-kpkt')
                                                <div class="form-group row mb-2">
                                                    <div class="col-3">Kementerian/Jabatan</div>
                                                    <div class="col">
                                                        <select name="department_id" class="form-select">
                                                            @foreach ($departments as $department)
                                                            <option value="{{ $department->id }}" {{ $department->id == $document->department_id ? 'selected' : '' }}>
                                                                {{ $department->name }}
                                                                ({{ $department->shortcode }})
                                                            </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                @endif

                                                <div class="form-group row mb-2">
                                                    <div class="col-3">Tarikh Dokumen</div>
                                                    <div class="col">
                                                        <input type="date" name="tarikh_dokumen" placeholder="Tarikh Dokumen" value="{{ old('tarikh_dokumen') ?? $document->tarikh_dokumen }}" class="form-control @error('tarikh_dokumen') is-invalid @enderror">
                                                        @error('tarikh_dokumen')
                                                        <div class="invalid-feedback">{{ $message }}</div>
                                                        @enderror
                                                    </div>
                                                </div>

                                                <div class="form-group mt-2 d-flex justify-content-end">
                                                    <button type="submit" class="btn btn-primary">Simpan <i class="fa-regular fa-floppy-disk" style="margin-left: 8px;"></i></button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane @if(request()->query('step') === '2') active @endif" role="tabpanel">
                                    <div class="card mb-2">
                                        <div class="card-body">
                                            <div class="card my-2">
                                                <div class="card-header">Tambah Lampiran</div>
                                                <div class="card-body">
                                                    <form action="{{ route('documents.lampirans.store', ['jenis' => 'request()->jenis', request()->document]) }}" method="POST" enctype="multipart/form-data" autocomplete="off">
                                                        @csrf
                                                        <input type="hidden" name="jenis" value="mesyuarat">
                                                        <div class="row mb-2">
                                                            <div class="d-flex col-md-12">
                                                                <div class="col-md-8">
                                                                    <label>Nama</label>
                                                                    <input type="text" name="nama_lampiran" value="{{ old('nama_lampiran') }}" class="form-control @error('nama_lampiran') is-invalid @enderror">
                                                                    @error('nama_lampiran')
                                                                    <div class="invalid-feedback">
                                                                        {{ $message }}
                                                                    </div>
                                                                    @enderror
                                                                </div>
                                                                <div class="col ms-2">
                                                                    <label>Jenis</label>
                                                                    <select name="jenis_lampiran" class="form-control @error('jenis_lampiran') is-invalid @enderror">
                                                                        <option value="">Sila pilih...</option>
                                                                        <option value="mesyuarat" @if(old('jenis_lampiran') === 'mesyuarat') selected @endif>
                                                                            Mesyuarat
                                                                        </option>
                                                                        <option value="lain" @if(old('jenis_lampiran') === 'lain') selected @endif>
                                                                            Lampiran Lain
                                                                        </option>
                                                                        <option value="ringkasan" @if(old('jenis_lampiran') === 'ringkasan') selected @endif>
                                                                            Ringkasan Dokumen
                                                                        </option>
                                                                        <option value="tambahan" @if(old('jenis_lampiran') === 'tambahan') selected @endif>
                                                                            Dokumen Tambahan
                                                                        </option>
                                                                    </select>
                                                                    @error('jenis_lampiran')
                                                                    <div class="invalid-feedback">
                                                                        {{ $message }}
                                                                    </div>
                                                                    @enderror
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <label>Fail Lampiran</label>
                                                                <input type="file" name="fail_lampiran" accept=".pdf" class="form-control @error('fail_lampiran') is-invalid @enderror">
                                                                @error('fail_lampiran')
                                                                <div class="invalid-feedback">
                                                                    {{ $message }}
                                                                </div>
                                                                @enderror
                                                            </div>
                                                        </div>
                                                        <div class="form-group mt-2 d-flex justify-content-end">
                                                            <button class="btn btn-primary" type="submit">Tambah</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                            <table class="table text-center mb-4">
                                                <thead>
                                                    <tr>
                                                        <th>Bil</th>
                                                        <th>Nama</th>
                                                        <th>Jenis</th>
                                                        <th>Tarikh Muat Naik</th>
                                                        <th>Tindakan</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @forelse($lampirans as $lampiran)
                                                    <tr>
                                                        <td>{{ $loop->iteration + $lampirans->firstItem() - 1 }}</td>
                                                        <td>{{ $lampiran->name }}</td>
                                                        <td>{{ $lampiran->label_jenis_lampiran }}</td>
                                                        <td>{{ date('d-m-Y', strtotime($lampiran->created_at)) }}</td>
                                                        <td>
                                                            <a class="btn btn-primary" href="{{ route('view.docs', $lampiran->id) }}" target="_blank">Lihat</a>
                                                            <button type="button" onclick="deleteLampiran(`{{ route('lampirans.destroy', $lampiran->id) }}`, `{{ $lampiran->name }}`)" class="btn btn-danger">Padam</button>
                                                        </td>
                                                    </tr>
                                                    @empty
                                                    <tr>
                                                        <td colspan="5">Tiada lampiran.</td>
                                                    </tr>
                                                    @endforelse
                                                </tbody>
                                            </table>
                                            {{ $lampirans->withQueryString()->links() }}
                                        </div>
                                    </div>
                                </div>
                                @if($document->jenis !== 'nota')
                                <div class="tab-pane @if(request()->query('step') === '3') active @endif" role="tabpanel">
                                    <div class="card">
                                        <div class="card-header">
                                            Agihan Kepada
                                        </div>
                                        <div class="card-body">
                                            <form id="reset_agihan_dokumen_form" action="{{ route('documents.agihans.reset', ['jenis' => request()->jenis, $document->id]) }}" method="POST">
                                                @csrf
                                            </form>
                                            <form action="{{ route('documents.agihans.store', ['jenis' => request()->jenis, $document->id]) }}" method="POST">
                                                @csrf
                                                <input type="hidden" name="model_id" value="{{ request()->document->id }}">
                                                <input type="hidden" name="jenis" value="document">
                                                <div class="form-group">
                                                    <select class="selectpicker form-control" name="departments[]" multiple="multiple">
                                                        @foreach ($departments as $department)
                                                        <option value="{{ $department->shortcode }}" @if(in_array($department->shortcode, $document->jabatan_agihans)) selected @endif>
                                                            {{ $department->name }}
                                                            ({{ $department->shortcode }})
                                                        </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="form-group mt-2 d-flex justify-content-end">
                                                    @if(count($document->jabatan_agihans) > 0)
                                                    <button type="button" onclick="resetAgihanDokumen(event)" class="btn btn-danger"><i class="fa-solid fa-rotate-right" style="margin-right: 8px;"></i>Set Semula</button>
                                                    <button type="submit" class="btn btn-primary ms-1"><i class="fa-solid fa-paper-plane" style="margin-right: 8px;"></i>Kemaskini dan Hantar</button>
                                                    @else
                                                    <button type="submit" class="btn btn-primary ms-1"><i class="fa-solid fa-paper-plane" style="margin-right: 8px;"></i>Hantar</button>
                                                    @endif
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                    <div class="card mt-2">
                                        <div class="card-header">
                                            Penerima Agihan
                                        </div>
                                        <div class="card-body">
                                            <table class="table text-center">
                                                <thead>
                                                    <tr>
                                                        <th>No</th>
                                                        <th style="text-align: left">Alamat Emel</th>
                                                        <th style="text-align: left">Jabatan/Kementerian</th>
                                                        <th>Tarikh Hantar</th>
                                                        <th>Tindakan</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @forelse($document->agihans as $agihan)
                                                    <tr>
                                                        <td>{{ $loop->iteration }}</td>
                                                        <td style="text-align: left">{{ $agihan->penerima }}</td>
                                                        <td style="text-align: left">{{ $agihan->label_jabatan_penerima }}</td>
                                                        <td>{{ date('d/m/Y h:i a', strtotime($agihan->updated_at)) }}</td>
                                                        <td>
                                                            <form action="{{ route('documents.agihans.resend', ['jenis' => request()->jenis, $document->id]) }}" method="POST">
                                                                @csrf
                                                                <input type="hidden" name="jenis" value="document">
                                                                <input type="hidden" name="model_id" value="{{ request()->document->id }}">
                                                                <input type="hidden" name="email_penerima" value="{{ $agihan->penerima }}">
                                                                <button class="btn btn-primary" type="submit">Hantar Semula</button>
                                                            </form>
                                                        </td>
                                                    </tr>
                                                    @empty
                                                    <tr>
                                                        <td colspan="5">Tiada agihan.</td>
                                                    </tr>
                                                    @endforelse
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    const deleteLampiran = async (url, name) => {
        window.confirmDialog(`Anda pasti untuk memadam lampiran '${name}'?`, async () => {
            try {
                await window.http(url, "DELETE")
                window.location.reload()
            } catch (error) {
                return;
            }
        })
    }

    const resetAgihanDokumen = (event) => {
        event.preventDefault();
        window.confirmDialog("<p>Set semula agihan?<br>(Semua agihan kepada semua penerima akan dipadam.)</p>", () => {
            $("#reset_agihan_dokumen_form").submit();
        })
    }
</script>
@endsection