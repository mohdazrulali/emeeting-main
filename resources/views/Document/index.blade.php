@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header d-flex justify-content-between">
                    <div class="my-auto">Dokumen
                        @if (request()->jenis == 'memo-kpkt')
                        Memo KPKT
                        @elseif (request()->jenis == 'kementerian-lain')
                        Memo Kementerian Lain
                        @elseif (request()->jenis == 'nota')
                        Nota
                        @else
                        '[Tiada Jenis]'
                        @endif
                    </div>
                    <a class="btn btn-primary" href="{{ route('documents.create', ['jenis' => request()->jenis]) }}">Dokumen
                        Baru</a>
                </div>
                <div class="row" style="margin: 16px 16px 0 16px">
                    <x-carian reset-route="{{ route('documents.index', request()->jenis) }}"></x-carian>
                </div>
                <div class="card-body table-responsive">
                    <x-success-message-banner></x-success-message-banner>
                    <x-error-message-banner></x-error-message-banner>
                    <table class="table text-center">
                        <thead>
                            <tr>
                                <th>Bil</th>
                                <th>Tarikh Dokumen</th>
                                <th>No. Fail</th>
                                <th>Tajuk / Sub Tajuk Dokumen</th>
                                <th>Tarikh Kemaskini</th>
                                <th>Tindakan</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($documents as $document)
                            <tr>
                                <td>{{ $loop->iteration + $documents->firstItem() - 1 }}</td>
                                <td>{{ $document->tarikh_dokumen }}</td>
                                <td>{{ $document->no_fail_kementerian }}</td>
                                <td>{{ $document->name }} / {{ $document->sub_name }}</td>
                                <td>{{ $document->updated_at }}</td>
                                <td>
                                    <a href="{{ route('documents.edit', ['jenis' => request()->jenis, $document->id]) }}" class="btn btn-warning">Kemaskini</a>
                                    <button type="button" class="btn btn-danger" onclick="deleteDocument(event, `Hapus dokumen '{{ $document->name }}' ?`, 'delete_document_form_{{ $document->id }}')">
                                        Hapus
                                    </button>
                                    <form id="delete_document_form_{{ $document->id }}" action="{{ route('documents.destroy', ['jenis' => request()->jenis, $document]) }}" method="POST">
                                        @csrf
                                        @method('DELETE')
                                    </form>
                                </td>
                            </tr>
                            @empty
                            <tr>
                                <td colspan="6">Tiada dokumen.</td>
                            </tr>
                            @endforelse
                        </tbody>
                    </table>
                    {{ $documents->withQueryString()->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
<script>
    const deleteDocument = (event, message, formId) => {
        event.preventDefault();
        window.confirmDialog(message, () => {
            $("#" + formId).submit()
        })
    }
</script>