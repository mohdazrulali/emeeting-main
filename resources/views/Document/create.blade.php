@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header d-flex justify-content-between">
                    <div class="my-auto">Dokumen @if ($jenis == 'memo-kpkt')
                        Memo KPKT
                        @elseif ($jenis == 'kementerian-lain')
                        Memo Kementerian Lain
                        @elseif ($jenis == 'nota')
                        Nota
                        @else
                        ERROR
                        @endif
                    </div>
                    <a class="btn btn-outline-primary" href="{{ route('documents.index') }}">Back</a>
                </div>

                <div class="card-body">

                    <div class="card my-2">
                        <div class="card-header">Kegunaan Pejabat</div>
                        <div class="card-body row">
                            <div class="col">Tarikh & Masa :{{ now()->format('d-m-Y H:i') }}
                            </div>
                            <div class="col">Disediakan Oleh :{{ auth()->user()->name }}</div>
                        </div>
                    </div>

                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                    <form action="{{ route('documents.store') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="jenis" value="{{ $jenis }}">
                        <div class="card my-2">
                            <div class="card-header">Dokumen</div>
                            <div class="card-body">

                                <div class="form-group row mb-2">
                                    <div class="col-3">Nombor File Kementerian</div>
                                    <div class="col">
                                        <input type="text" name="no_fail_kementerian" placeholder="No File Kementerian" class="form-control @error('no_fail_kementerian') is-invalid @enderror" value="{{ old('no_fail_kementerian') }}">

                                        @error('no_fail_kementerian')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row mb-2">
                                    <div class="col-3">Salinan Bilangan</div>
                                    <div class="col">
                                        <input type="text" name="salinan_bilangan" placeholder="Salinan Bilangan" class="form-control @error('salinan_bilangan') is-invalid @enderror">
                                        @error('salinan_bilangan')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row mb-2">
                                    <div class="col-3">Tajuk Dokumen</div>
                                    <div class="col">
                                        <input type="text" name="name" placeholder="Tajuk Dokumen" class="form-control @error('name') is-invalid @enderror">
                                        @error('name')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row mb-2">
                                    <div class="col-3">Sub Tajuk Dokumen</div>
                                    <div class="col">
                                        <input type="text" name="sub_name" placeholder="Sub Tajuk Dokumen" class="form-control @error('sub_name') is-invalid @enderror">
                                        @error('sub_name')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row mb-2">
                                    <div class="col-3">Kementerian</div>
                                    <div class="col">

                                        <select name="kementerian" class="form-select">
                                            @foreach ($ministries as $ministry)
                                            <option value="{{ $ministry->id }}" @if ($jenis=='memo-kpkt' && $ministry->shortcode == 'KPKT') selected @endif>
                                                {{ $ministry->name }}
                                                ({{ $ministry->shortcode }})
                                            </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row mb-2">
                                    <div class="col-3">Tarikh Dokumen</div>
                                    <div class="col">
                                        <input type="date" name="tarikh_dokumen" placeholder="Tarikh Dokumen" class="form-control @error('tarikh_dokumen') is-invalid @enderror">
                                        @error('tarikh_dokumen')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="card my-4">
                            <div class="card-header">Mesyuarat</div>
                            <div class="card-body">
                                <table class="table text-center mb-4">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Nama</th>
                                            <th>Tarikh Muat Naik</th>
                                            <th>Tindakan</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>1</td>
                                            <td>File Saya</td>
                                            <td>Tarikh</td>
                                            <td>Padam</td>
                                        </tr>
                                    </tbody>
                                </table>

                                <div class="card my-2">
                                    <div class="card-header">Sekiranya terdapat lampiran, sila muat naik lampiran
                                        tersebut.</div>
                                    <div class="card-body">
                                        <div class="form-group">
                                            <label for="">Nama Lampiran</label>
                                            <input type="text" name="nama_lampiran_mesyuarat" class="form-control">
                                        </div>

                                        <div class="form-group">
                                            <label for="">Fail Lampiran</label>
                                            <input type="file" name="fail_lampiran_mesyuarat[]" accept=".pdf" class="form-control" multiple>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="card my-4">
                            <div class="card-header">Lampiran Lain</div>
                            <div class="card-body">
                                <table class="table text-center mb-4">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Nama</th>
                                            <th>Tarikh Muat Naik</th>
                                            <th>Tindakan</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>1</td>
                                            <td>File Saya</td>
                                            <td>Tarikh</td>
                                            <td>Padam</td>
                                        </tr>
                                    </tbody>
                                </table>

                                <div class="card my-2">
                                    <div class="card-header">Sekiranya terdapat lampiran, sila muat naik lampiran
                                        tersebut.</div>
                                    <div class="card-body">
                                        <div class="form-group">
                                            <label for="">Nama Lampiran</label>
                                            <input type="text" name="nama_lampiran_lain" class="form-control">
                                        </div>

                                        <div class="form-group">
                                            <label for="">Fail Lampiran</label>
                                            <input type="file" name="fail_lampiran_lain[]" accept=".pdf" class="form-control" multiple>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="card my-4">
                            <div class="card-header">Ringkasan Dokumen</div>
                            <div class="card-body">
                                <table class="table text-center mb-4">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Nama</th>
                                            <th>Tarikh Muat Naik</th>
                                            <th>Tindakan</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>1</td>
                                            <td>File Saya</td>
                                            <td>Tarikh</td>
                                            <td>Padam</td>
                                        </tr>
                                    </tbody>
                                </table>

                                <div class="card my-2">
                                    <div class="card-header">Sekiranya terdapat lampiran, sila muat naik lampiran
                                        tersebut.</div>
                                    <div class="card-body">
                                        <div class="form-group">
                                            <label for="">Nama Lampiran</label>
                                            <input type="text" name="nama_lampiran_tambahan" class="form-control">
                                        </div>

                                        <div class="form-group">
                                            <label for="">Fail Lampiran</label>
                                            <input type="file" name="fail_lampiran_tambahan[]" accept=".pdf" class="form-control" multiple>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="card my-4">
                            <div class="card-header">Maklumat Tambahan</div>
                            <div class="card-body">
                                <table class="table text-center mb-4">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Nama</th>
                                            <th>Tarikh Muat Naik</th>
                                            <th>Tindakan</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>1</td>
                                            <td>File Saya</td>
                                            <td>Tarikh</td>
                                            <td>Padam</td>
                                        </tr>
                                    </tbody>
                                </table>

                                <div class="card my-2">
                                    <div class="card-header">Sekiranya terdapat lampiran, sila muat naik lampiran
                                        tersebut.</div>
                                    <div class="card-body">
                                        <div class="form-group">
                                            <label for="">Nama Lampiran</label>
                                            <input type="text" name="nama_lampiran_ringkasan" class="form-control">
                                        </div>

                                        <div class="form-group">
                                            <label for="">Fail Lampiran</label>
                                            <input type="file" name="fail_lampiran_ringkasan[]" accept=".pdf" class="form-control" multiple>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <button class="float-end btn btn-primary" type="submit">Save</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
