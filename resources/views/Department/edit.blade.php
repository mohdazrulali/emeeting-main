@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header d-flex justify-content-between">
                        <div class="my-auto">
                            Edit Jabatan <strong>{{ $department->name }}</strong>
                        </div>
                        <a class="btn btn-outline-primary" href="{{ route('departments.index') }}">Back</a>
                    </div>

                    <div class="card-body">
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif


                        <form action="{{ route('departments.update', $department) }}" method="POST">
                            @csrf
                            @method('PUT')

                            <div class="form-group mb-3">
                                <label for="Shortcode">Bahagian</label>
                                <input type="text" name="section"
                                    value={{ $department->section }}placeholder="Contoh : Bahagian Kewangan"
                                    class="form-control @error('section') is-invalid @enderror">
                                @error('section')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="form-group mb-3">
                                <label for="Name">Nama Jabatan / Kementerian</label>
                                <input type="text" name="name" Contoh : Jabatan Kerajaan Tempatan , Kementerian
                                    Perumahan dan Kerajaan Tempatan class="form-control @error('name') is-invalid @enderror"
                                    value="{{ $department->name }}">
                                @error('name')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group mb-3">
                                <label for="Code">Code</label>
                                <input type="text" name="code" placeholder="Contoh : K01"
                                    class="form-control @error('code') is-invalid @enderror"
                                    value="{{ $department->code }}">
                                @error('code')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group mb-3">
                                <label for="Shortcode">Shortcode</label>
                                <input type="text" name="shortcode" placeholder="Contoh : BKewJKT"
                                    class="form-control @error('shortcode') is-invalid @enderror"
                                    value="{{ $department->shortcode }}">
                                @error('shortcode')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>

                            <button class="float-end btn btn-primary" type="submit">Kemaskini</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
