<footer class="main-footer  no-print">
    <div class="float-right d-none d-sm-block">
        <b>Version</b> 1.0.0
    </div>
    Copyright &copy; 2023 <span class="font-weight-bold text-logo">{{config('app.name_short')}}</span> {{config('app.dept_short')}}, {{config('app.agency_short')}} | {{config('app.name')}}</span>.

</footer>
