<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="icon" href="{{ asset('img/jata.png') }}" type="image/icon type">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="robots" content="noindex, noimageindex, nofollow, noarchive,nocache,nosnippet,noodp,noydir">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title') | {{ config('app.name', 'Sistem e-Cores') }}</title>

    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('plugins/fontawesome-free/css/all.min.css') }}">

    <!-- iCheck -->
    <link rel="stylesheet" href="{{ asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">

    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('css/adminlte.min.css') }}">

    <!-- CSS:style
    Background
    -->

    <link rel="stylesheet" href="{{ asset('css/style.css') }}">

    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>

<body class="hold-transition login-page">
<div class="display:flex; justify-content:center; text-center">
    <br><br><br><br><br><br>
    <a class="navbar-brand" href="{{ url('/') }}">
        <img src='{{ asset('img/jata.png') }}' width="180px">
    </a>
    <br>
    <h2 class="text-light font-weight-bold font-size-15">{{config('app.name')}}</h2><br />
</div>
    @yield('content')
    @yield('demo')
    <div class="container text-center">

        <span class="text-light font-weight-bold">{{config('app.agency_short')}}</span> | {{config('app.name')}}</span><br />
        <!-- Default to the left -->
        {{config('app.dept')}}<br />
        <strong>Copyright &copy; 2023 {{config('app.agency')}}</strong>

    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery -->
    <script src="{{ asset('plugins/jquery/jquery.min.js') }}"></script>
    <!-- Bootstrap 4 -->
    <script src="{{ asset('plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    @yield('page-js-script')
</body>

</html>
