<aside class="main-sidebar sidebar-dark-indigo elevation-0" >
    <!-- Brand Logo -->
    {{-- <a href="{{ Auth::guard('web')->check() ? route('web.dashboard') : route('web.dashboard') }}" --}}

    {{-- <img src="{{ asset('img/jata.png') }}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
        style="opacity: .7"> --}}
    {{-- @if (Auth::guard('web')->id() == 5)
        <span class="brand-text font-weight-light">Superuser</span>
    @elseif(Auth::guard('web')->id() == 1)
        <span class="brand-text font-weight-light">Pentadbir</span>
    @endif --}}


    <!-- Sidebar -->
    <div class="sidebar">
        <br>
        <img src="{{ asset('img/jata.png') }}" alt="AdminLTE Logo" class="brand-image img-circle elevation-4"
            style="opacity: .7">
        <a class="navbar-brand" href="{{ url('/') }}">
            {{ config('app.name', 'Laravel') }}
        </a>

        <div class="user-panel mt-4 pb-4 mb-4 d-flex">
            &ensp; <div class="d-block info fa fa-user-lock" style="font-size: 11px; color:rgb(236, 213, 5)">&ensp; :
                &emsp; {{ Auth::guard('web')->user()->roles->first()->name }}
            </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
                data-accordion="false">

                <li class="nav-item">
                    {!! Html::buttonSidebarNavLink('Dashboard', 'fas fa-tachometer-alt', [
                        'onclick' => "window.location='" . route('dashboard') . "'",
                        'class' => 'nav-link btn btn-block btn-link text-left ' . Html::active('dashboard'),
                    ]) !!}
                </li>


                {{-- @if (Auth::guard('web')->user()->roles->first()->name == 'Pentadbir') --}}
                @hasanyrole('Pentadbir')
                <li class="nav-item">
                    {!! Html::buttonSidebarNavLink('Memo-KPKT', 'fas fa-file-archive', [
                        'onclick' => "window.location='" . route('documents.index', ['jenis' => 'memo-kpkt']). "'",
                        'class' => 'nav-link btn btn-block btn-link text-left ' . Html::active('documents1.index'),
                    ]) !!}
                </li>
                <li class="nav-item">
                    {!! Html::buttonSidebarNavLink('Memo Kementerian Lain', 'fas fa-file-archive', [
                        'onclick' => "window.location='" . route('documents.index', ['jenis' => 'kementerian-lain']). "'",
                        'class' => 'nav-link btn btn-block btn-link text-left ' . Html::active('documents2.index'),
                    ]) !!}
                </li>
                <li class="nav-item">
                    {!! Html::buttonSidebarNavLink('Nota', 'fas fa-sticky-note', [
                        'onclick' => "window.location='" . route('documents.index', ['jenis' => 'nota']). "'",
                        'class' => 'nav-link btn btn-block btn-link text-left ' . Html::active('documents3.index'),
                    ]) !!}
                </li>
                @endhasanyrole
                {{-- @endif --}}
            </ul>

            @if (Auth::guard('web')->user()->roles->first()->name =='Ketua Jabatan')
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
                data-accordion="false">
                <li class="nav-header text-uppercase" style="color: rgb(255, 255, 255)">Senarai Mesyuarat
                    <hr style="height:2px;border-width:0;color:gray;background-color:rgb(83, 129, 255)">
                </li>
                <li class="nav-item">
                    {!! Html::buttonSidebarNavLink('Jemaah Menteri', 'fa fa-file-alt', [
                        'onclick' => "window.location='" . route('mesyuarat.mesyuarat-jemaah-menteri') . "'",
                        'class' => 'nav-link btn btn-block btn-link text-left ' . Html::active('mesyuarat.mesyuarat-jemaah-menteri'),
                    ]) !!}
                </li>
                <li class="nav-item">
                    {!! Html::buttonSidebarNavLink('Utama', 'fa fa-file-word', [
                        'onclick' => "window.location='" . route('mesyuarat.mesyuarat-utama') . "'",
                        'class' => 'nav-link btn btn-block btn-link text-left ' . Html::active('mesyuarat.mesyuarat-utama'),
                    ]) !!}
                </li>
                <li class="nav-item">
                    {!! Html::buttonSidebarNavLink('Nota', 'fa fa-sticky-note', [
                        'onclick' => "window.location='" . route('documents.index', ['jenis' => 'nota']). "'",
                        'class' => 'nav-link btn btn-block btn-link text-left ' . Html::active('nota.index'),
                    ]) !!}
                </li>
            </ul>
            @endif

            <br>
            @if (Auth::guard('web')->user()->roles->first()->name =='Ketua Jabatan')
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
                data-accordion="false">
                <li class="nav-header text-uppercase" style="color: rgb(255, 255, 255)">Tindakan
                    <hr style="height:2px;border-width:0;color:gray;background-color:gold">
                </li>

                <li class="nav-item">
                    {!! Html::buttonSidebarNavLink('Maklumbalas', 'fa fa-pen-fancy', [
                        'onclick' => "window.location='" . route('maklumbalas.index') . "'",
                        'class' => 'nav-link btn btn-block btn-link text-left ' . Html::active('maklumbalas.index'),
                    ]) !!}
                </li>
            </ul>
            @endif

            @if (Auth::guard('web')->user()->roles->first()->name == 'Pentadbir' || Auth::guard('web')->user()->roles->first()->name == 'Pentadbir Sistem' )
            <br>
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
                data-accordion="false">
                <li class="nav-header text-uppercase" style="color: rgb(255, 255, 255)">Pengurusan Admin
                    <hr style="height:2px;border-width:0;color:gray;background-color:rgb(83, 129, 255)">
                </li>
                <li class="nav-item">
                    {!! Html::buttonSidebarNavLink('Pengguna Sistem', 'fa fa-user-secret', [
                        'onclick' => "window.location='" . route('users.index') . "'",
                        'class' => 'nav-link btn btn-block btn-link text-left ' . Html::active('users.index'),
                    ]) !!}
                </li>
                <li class="nav-item">
                    {!! Html::buttonSidebarNavLink('Jabatan/Kementerian', 'fa fa-building', [
                        'onclick' => "window.location='" . route('departments.index') . "'",
                        'class' => 'nav-link btn btn-block btn-link text-left ' . Html::active('departments.index'),
                    ]) !!}
                </li>
                <li class="nav-item">
                    {!! Html::buttonSidebarNavLink('Kawalan Capaian', 'fas fa-broadcast-tower', [
                        'onclick' => "window.location='" . route('acls.index') . "'",
                        'class' => 'nav-link btn btn-block btn-link text-left ' . Html::active('acls.index'),
                    ]) !!}
                </li>
                <li class="nav-header text-uppercase" style="color: rgb(255, 255, 255)">Audit Log
                    <hr style="height:2px;border-width:0;color:rgba(214, 38, 38, 0.911);background-color:rgb(180, 12, 12)">
                </li>
                <li class="nav-item">
                    {!! Html::buttonSidebarNavLink('Pengguna', 'fa fa-user-secret', [
                        'onclick' => "window.location='" . route('logaudit.pengguna.index') . "'",
                        'class' => 'nav-link btn btn-block btn-link text-left ' . Html::active('logaudit.pengguna.index'),
                    ]) !!}
                </li>
                <li class="nav-item">
                    {!! Html::buttonSidebarNavLink('Dokumen', 'fa fa-user-secret', [
                        'onclick' => "window.location='" . route('logaudit.dokumen.index') . "'",
                        'class' => 'nav-link btn btn-block btn-link text-left ' . Html::active('logaudit.dokumen.index'),
                    ]) !!}
                </li>
            </ul>
            @endif




















            <!-- /.Akses Sistem -->
            @hasanyrole('Pentadbir Sistem|Pentadbir|Pentadbir Jabatan')
            {{-- @include('layouts.elements._sidebarpengurusan') --}}
            @endhasanyrole
            <!-- /.Akses Sistem -->

            <br>
            <li class="nav-header text-uppercase" style="color: rgb(255, 255, 255)">KPKT - Meja Bantuan
                <hr style="height:2px;border-width:0;color:gray;background-color:rgb(31, 199, 241)">
            </li>
            <a href="https://edirektori.kpkt.gov.my/edirektori/index.php/home/grid/199" class="nav-link" target="_blank"
                class="brand-link navbar-purple">
                <img src="{{ asset('img/pautan.png') }}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
                    style="opacity: .7"> BSA
            </a>

            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
