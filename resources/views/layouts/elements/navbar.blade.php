<nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
        </li>
    </ul>
    <div class="container">

        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <!-- Left Side Of Navbar -->
            <ul class="navbar-nav me-auto">
                @auth
                    @foreach (getNavbarMenu() as $menu)
                        @if (!isset($menu['sub_menu']))
                            @canany($menu['access'])
                                <li class="nav-item">
                                    <a class="nav-link @if (routeIsActive($menu['route'])) active @endif"
                                        href="{{ $menu['route'] }}">{{ $menu['label'] }}</a>
                                </li>
                            @endcanany
                        @else
                            @canany($menu['access'])
                                <li class="nav-item dropdown">
                                    <a class="nav-link @if (routeIsActive($menu['route'])) active @endif dropdown-toggle"
                                        href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown"
                                        aria-expanded="false">
                                        {{ $menu['label'] }}
                                    </a>
                                    <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                                        @foreach ($menu['sub_menu'] as $sub)
                                            @if ($sub && is_array($sub))
                                                @canany($sub['access'])
                                                    <li>
                                                        <a class="dropdown-item @if (routeIsActive($sub['route'])) active @endif"
                                                            href="{{ $sub['route'] }}">
                                                            {{ $sub['label'] }}
                                                        </a>
                                                    </li>
                                                @endcanany
                                            @else
                                                <li>
                                                    <hr class="dropdown-divider">
                                                </li>
                                            @endif
                                        @endforeach
                                    </ul>
                                </li>
                            @endcanany
                        @endif
                    @endforeach
                @endauth
            </ul>

            <!-- Right Side Of Navbar -->
            <ul class="navbar-nav ms-auto">
                <!-- Authentication Links -->
                @guest
                    @if (Route::has('login'))
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                        </li>
                    @endif

                    {{-- @if (Route::has('register'))
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
            </li>
            @endif --}}
                @else
                    <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                            data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            {{ Auth::user()->name }}
                        </a>

                        <div class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="{{ route('logout') }}"
                                onclick="event.preventDefault();
                                         document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                @csrf
                            </form>
                        </div>
                    </li>
                @endguest
            </ul>
        </div>
    </div>
</nav>
