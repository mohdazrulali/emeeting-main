@if (Auth::guard('web')->id() == 1)
    <li class="nav-header text-uppercase">pengurusan
        <hr style="height:2px;border-width:0;color:gray;background-color:red">
    </li>

    <!-- Meta Data  -->
    <li class="nav-item has-treeview">
        {!! Html::buttonSidebarNavLinkTreeview('Meta Data', 'fas fa-cubes', [
            'class' => 'nav-link btn btn-block
                                            btn-link text-left',
        ]) !!}
        <ul class="nav nav-treeview">
            <li class="nav-item">
                {!! Html::buttonSidebaNavItemTree('Negara', [
                    'onclick' => "window.location='" . route('mesyuarat.mesyuarat-utama') . "'",
                    'class' =>
                        'nav-link btn btn-block btn-link text-left
                                                                                            ' .
                        Html::active('mesyuarat.mesyuarat-utama'),
                ]) !!}
            </li>
            <li class="nav-item">
                {!! Html::buttonSidebaNavItemTree('Negeri', [
                    'onclick' => "window.location='" . route('mesyuarat.mesyuarat-utama') . "'",
                    'class' =>
                        'nav-link btn btn-block btn-link text-left
                                                                                            ' .
                        Html::active('mesyuarat.mesyuarat-utama'),
                ]) !!}
            </li>
        </ul>
    </li>

    <li class="nav-item has-treeview">
        {!! Html::buttonSidebarNavLinkTreeview('Data Rujukan', 'fas fa-user-shield', [
            'class' => 'nav-link btn
                                            btn-block btn-link text-left',
        ]) !!}
        <ul class="nav nav-treeview">
            @can('pengguna-list')
                <li class="nav-item">
                    {!! Html::buttonSidebaNavItemTree('Pingat', [
                        'onclick' => "window.location='" . route('dashboard') . "'",
                        'class' =>
                            'nav-link btn btn-block btn-link text-left
                                                                                                                ' .
                            Html::active('dashboard'),
                    ]) !!}
                </li>
            @endcan
            @can('pengguna-list')
                <li class="nav-item">
                    {!! Html::buttonSidebaNavItemTree('Jabatan/Bahagian', [
                        'onclick' => "window.location='" . route('maklumbalas.index') . "'",
                        'class' =>
                            'nav-link btn btn-block btn-link text-left
                                                                                                            ' .
                            Html::active('maklumbalas.index'),
                    ]) !!}
                </li>
            @endcan
        </ul>
    </li>
@endif






