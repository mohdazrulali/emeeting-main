{{-- Show success message --}}
@if (Session::has('successMessage'))
{!! '<div>'.Html::notification_alert('success',session('successMessage')).'</div>' !!}
@endif
{{-- Show fail message --}}
@if (Session::has('errorMessage'))
{!! '<div>'.Html::notification_alert('danger',session('errorMessage')).'</div>' !!}
@endif
{{-- Show fail message --}}
@if (Session::has('warningMessage'))
{!! '<div>'.Html::notification_alert('warning',session('warningMessage')).'</div>' !!}
@endif
{{-- Show fail message --}}
@if (Session::has('infoMessage'))
{!! '<div>'.Html::notification_alert('info',session('infoMessage')).'</div>' !!}
@endif
