<p>Y. Bhg. Dato'/Datuk/Datin/Tuan/Puan,</p>
<p>Dengan hormatnya saya diarah merujuk kepada perkara di atas.</p>

<p>2. Sukacitanya dimaklumkan bahawa Minit Keputusan Mesyuarat
    {{ $meeting->jenis === 'mesyuarat-jemaah-menteri' ? 'Jemaah Menteri' : 'Utama' }} telah dikeluarkan untuk makluman
    pihak
    Y. Bhg.
    Dato'/Datuk/Datin/Tuan/Puan. Pihak Y. Bhg. Dato'/Datuk/Datin/Tuan/Puan dengan ini dipohon untuk memberikan
    maklumbalas
    kepada Keputusan Minit Mesyuarat Utama berikut melalui 'Electronic Corporate Response System' (eCORES) di
    <a href="{{ url('/') }}">{{ url('/') }}</a> seperti butiran berikut:
</p>

<p>
    Jenis Dokumen : Keputusan Mesyuarat
    {{ $meeting->jenis === 'mesyuarat-jemaah-menteri' ? 'Jemaah Menteri' : 'Utama' }} <br>
    Tajuk : {{ $meeting->name }} <br>
    Tarikh Mesyuarat : {{ $meeting->tarikh_mesyuarat }} <br>
    Tujuan : {{ $meeting->tujuan }} <br>
</p>

<p>3. Memandangkan dokumen tersebut adalah bertaraf dokumen terperingkat, pihak Y. Bhg. Dato'/Datuk/Datin/Tuan/Puan
    adalah
    dinasihatkan mengambil langkah sewajarnya bagi memastikan kerahsiaannya terpelihara. Maklumat yang terdapat dalam
    dokumen ini tidak boleh didedahkan kepada mana-mana pihak kecuali pegawai-pegawai yang perlu mengetahui sahaja.</p>

<p>4. Perhatian pihak Tuan/Puan terhadap perkara ini didahului ucapan terima kasih</p>


<p>
    "BERKHIDMAT UNTUK NEGARA"</p>

<p>
    -melalui emel- <br>
    Bahagian Perancangan Strategik dan Antarabangsa, <br>
    Kementerian Perumahan dan Kerajaan Tempatan. <br>
</p>

</pre>
