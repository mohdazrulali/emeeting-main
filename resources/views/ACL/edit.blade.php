@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <x-success-message-banner></x-success-message-banner>
            <x-error-message-banner></x-error-message-banner>
            <div class="card">
                <div class="card-header">
                    Kemaskini Peranan Pengguna
                </div>
                <div class="card-body">
                    <form action="{{ route('acls.update', [$user->id]) }}" method="POST">
                        @csrf
                        @method('PUT')
                        <div class="row mb-2">
                            <label class="col-3 col-form-label">Peranan</label>
                            <div class="col">
                                <select name="roles[]" class="selectpicker form-control" multiple>
                                    @foreach($available_roles as $role)
                                    <option value="{{ $role }}" @if(in_array($role, $user->getRoleNames()->toArray())) selected @endif>{{ $role }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row mb-2">
                            <div>
                                <button class="float-end btn btn-primary ms-1" type="submit">Simpan</button>
                                <button class="float-end btn btn-danger" onclick="resetPeranan('{{ route('acls.reset', [$user->id]) }}', '{{$user->name}}')" type="button">Set Semula Peranan</button>
                            </div>
                        </div>
                        <div class="row">
                            <label class="mb-2">Akses Peranan</label>
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped">
                                    <thead>
                                        <th>Akses</th>
                                    </thead>
                                    <tbody>
                                        @forelse($user->getAllPermissions() as $permission)
                                        <tr>
                                            <td>{{ $permission->name }}</td>
                                        </tr>
                                        @empty
                                        <tr>
                                            <td class="text-center">Tiada akses peranan.</td>
                                        </tr>
                                        @endforelse
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    const resetPeranan = async (url, userName) => {
        window.confirmDialog(`Anda pasti untuk set semula peranan ${userName}?`, async () => {
            try {
                await window.http(url, "POST")
                window.location.reload()
            } catch (error) {
                return;
            }
        })
    }
</script>
@endsection
