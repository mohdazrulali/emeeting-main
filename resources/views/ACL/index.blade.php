@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Kawalan Capaian</div>

                <div class="card-body">
                    <x-carian reset-route="{{ route('acls.index') }}"></x-carian>
                    <div class="table-responsive mt-2">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Bil</th>
                                    <th>Nama Pengguna</th>
                                    <th>Peranan</th>
                                    <th class="text-center">Tindakan</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($users as $user)
                                <tr>
                                    <td>{{ $loop->iteration + $users->firstItem() - 1 }}</td>
                                    <td>{{ $user->name }}</td>
                                    <td>
                                        @forelse ($user->getRoleNames() as $role)
                                        <div class="badge bg-primary">{{ $role }}</div>
                                        @empty
                                        <div class="badge bg-secondary">Tiada Peranan</div>
                                        @endforelse
                                    </td>
                                    <td class="text-center"><a href="{{ route('acls.edit', [$user->id]) }}" class="btn btn-sm btn-primary">Kemaskini</a></td>
                                </tr>
                                @empty
                                @endforelse
                            </tbody>
                        </table>
                        {{ $users->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
