@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    Keputusan Mesyuarat
                    @if (request()->jenis == 'mesyuarat-jemaah-menteri')
                    Jemaah Menteri
                    @elseif (request()->jenis == 'mesyuarat-utama')
                    Utama
                    @else
                    [Tiada Jenis]
                    @endif
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12 d-flex justify-content-between">
                            <ul class="nav nav-pills" role="tablist">
                                <li class="nav-item" role="presentation">
                                    <a class="nav-link active" role="tab">1. Maklumat
                                        Keputusan Mesyuarat
                                        @if (request()->jenis == 'mesyuarat-jemaah-menteri')
                                        Jemaah Menteri
                                        @elseif (request()->jenis == 'mesyuarat-utama')
                                        Utama
                                        @else
                                        [Tiada Jenis]
                                        @endif</a>
                                </li>
                            </ul>
                            <div>
                                <a type="button" class="btn btn-outline-primary" href="{{ route('meetings.index', ['jenis'   => request()->jenis]) }}">
                                    <i class="fa-solid fa-angle-left" style="margin-right: 8px;"></i>Kembali
                                </a>
                            </div>
                        </div>

                        <div class="col-md-12 mt-2">
                            @if (session('status'))
                            <div class="alert alert-{{ session('status.type') }}" role="alert">
                                {{ session('status.message') }}
                            </div>
                            @endif


                            @if ($errors->any())
                            <div class="alert alert-danger mb-0">
                                <ul style="margin-bottom: 0;">
                                    @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                            @endif
                        </div>

                        <div class="col-md-12 mt-2">
                            <div class="tab-content">
                                <div class="tab-pane active" role="tabpanel">
                                    <div class="card">
                                        <div class="card-body">
                                            <form action="{{ route('meetings.store', ['jenis'   => request()->jenis, ]) }}" method="POST" enctype="multipart/form-data" autocomplete="off">
                                                @csrf
                                                <input type="hidden" name="jenis" value="{{ request()->jenis }}">

                                                <div class="form-group row mb-2">
                                                    <div class="col-3">Tujuan</div>
                                                    <div class="col">
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="tujuan" value="Makluman" @if(old('tujuan')==='Makluman' ) checked @endif @if(!old('tujuan') ) checked @endif>
                                                            <label class="form-check-label" for="Makluman">Makluman</label>
                                                        </div>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="tujuan" value="Tindakan Segera" @if(old('tujuan')==='Tindakan Segera' ) checked @endif>
                                                            <label class="form-check-label" for="Tindakan Segera">Tindakan Segera</label>
                                                        </div>

                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="tujuan" value="Tindakan Serta Merta" @if(old('tujuan')==='Tindakan Serta Merta' ) checked @endif>
                                                            <label class="form-check-label" for="Tindakan Serta Merta">Tindakan Serta Merta</label>
                                                        </div>

                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="tujuan" value="Tindakan Susulan" @if(old('tujuan')==='Tindakan Susulan' ) checked @endif>
                                                            <label class="form-check-label" for="Tindakan Susulan">Tindakan Susulan</label>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group d-flex mb-2">
                                                    <div class="col-3">Tarikh Mesyuarat</div>
                                                    <div class="col">
                                                        <input type="date" name="tarikh_mesyuarat" placeholder="Tarikh Mesyuarat" class="form-control @error('tarikh_mesyuarat') is-invalid @enderror">
                                                        @error('tarikh_mesyuarat')
                                                        <div class="invalid-feedback">{{ $message }}</div>
                                                        @enderror
                                                    </div>
                                                </div>

                                                <div class="card">
                                                    <div class="card-header">Maklumat Mesyuarat</div>
                                                    <div class="card-body">
                                                        <div class="form-group row mb-2">
                                                            <div class="col-3">Status Minit</div>
                                                            <div class="col">
                                                                <div class="form-check form-check-inline">
                                                                    <input class="form-check-input" type="radio" name="is_approved" value="1" @if(old('is_approved')==='1' ) checked @endif @if(!old('is_approved') ) checked @endif>
                                                                    <label class="form-check-label" for="Telah Disahkan">Telah Disahkan</label>
                                                                </div>

                                                                <div class="form-check form-check-inline">
                                                                    <input type="radio" name="is_approved" value="0" @if(old('is_approved')==='0' ) checked @endif>
                                                                    <label for="Belum Disahkan">Belum Disahkan</label>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="form-group row mb-2">
                                                            <div class="col-3">Tarikh Status</div>
                                                            <div class="col">
                                                                <input type="date" name="approved_date" placeholder="Tarikh Status" class="form-control @error('approved_date') is-invalid @enderror">
                                                                @error('approved_date')
                                                                <div class="invalid-feedback">{{ $message }}</div>
                                                                @enderror
                                                            </div>
                                                        </div>

                                                        <div class="form-group row mb-2">
                                                            <div class="col-3">No Rujukan Fail</div>
                                                            <div class="col">
                                                                <input type="text" name="no_rujukan_fail" placeholder="No Rujukan Fail" class="form-control @error('no_rujukan_fail') is-invalid @enderror">
                                                                @error('no_rujukan_fail')
                                                                <div class="invalid-feedback">{{ $message }}</div>
                                                                @enderror
                                                            </div>
                                                        </div>

                                                        <div class="form-group row mb-2">
                                                            <div class="col-3">Tajuk /Perkara</div>
                                                            <div class="col">
                                                                <input type="text" name="name" placeholder="Tajuk /Perkara" class="form-control @error('name') is-invalid @enderror">
                                                                @error('name')
                                                                <div class="invalid-feedback">{{ $message }}</div>
                                                                @enderror
                                                            </div>
                                                        </div>

                                                        <div class="form-group row mb-2">
                                                            <div class="col-3">Minit Mesyuarat</div>
                                                            <div class="col">
                                                                <textarea name="ringkasan" placeholder="Minit Mesyuarat" class="form-control  @error('ringkasan') is-invalid @enderror"></textarea>
                                                                @error('ringkasan')
                                                                <div class="invalid-feedback">{{ $message }}</div>
                                                                @enderror

                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                                <div class="form-group mt-2 d-flex justify-content-end">
                                                    <button type="submit" class="btn btn-primary">Simpan <i class="fa-regular fa-floppy-disk" style="margin-left: 8px;"></i></button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection