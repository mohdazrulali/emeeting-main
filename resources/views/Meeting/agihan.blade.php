@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-4">
                <div class="card">
                    <div class="card-header d-flex justify-content-between">
                        <div class="my-auto">Agihan Mesyuarat {{ $meeting->name }}
                        </div>
                        <div>
                            <a class="btn btn-outline-primary" href="{{ route('meetings.edit', ['jenis'   => request()->jenis, $meeting]) }}">Back</a>
                        </div>

                    </div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-{{ session('status.type') }}" role="alert">
                                {{ session('status.message') }}
                            </div>
                        @endif

                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <form action="{{ route('agihans.store') }}" method="POST">
                            @csrf
                            <input type="hidden" name="model_id" value="{{ $meeting->id }}">
                            <input type="hidden" name="jenis" value="meetings">
                            @foreach ($departments as $department)
                                <input type="checkbox" name="departments[]" value="{{ $department->shortcode }}"><label
                                    class="ml-2">{{ $department->name }} ({{ $department->section }}) </label><br />
                            @endforeach

                            <button class="float-end btn btn-primary" type="submit">Agih</button>
                        </form>
                    </div>

                </div>


            </div>
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header d-flex justify-content-between">
                        <div class="my-auto">Agihan Mesyuarat {{ $meeting->name }}
                        </div>
                        <div>
                            <a class="btn btn-outline-primary"
                                href="{{ route('meetings.index', ['jenis'   => request()->jenis, ]) }}">Back</a>
                        </div>

                    </div>

                    <div class="card-body">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Penerima</th>
                                    <th>Waktu Hantar</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($meeting->agihans as $agihan)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $agihan->penerima }}</td>
                                        <td>{{ $agihan->created_at }}</td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td>Tiada Agihan </td>
                                    </tr>
                                @endforelse
                                <tr>

                                </tr>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
    </div>
@endsection
