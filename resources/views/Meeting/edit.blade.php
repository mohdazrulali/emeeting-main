@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header d-flex justify-content-between">
                        <div class="my-auto">Kemaskini Keputusan Mesyuarat
                            @if ($meeting->jenis == 'mesyuarat-jemaah-menteri')
                                Jemaah Menteri
                            @elseif ($meeting->jenis == 'mesyuarat-utama')
                                Utama
                            @else
                                ERROR
                            @endif
                        </div>
                        <div>
                            <a class="btn btn-outline-success" href="{{ route('meetings.agihan', $meeting) }}">Senarai
                                Agihan</a>
                            <a class="btn btn-outline-primary"
                                href="{{ route('meetings.index', ['jenis' => $meeting->jenis]) }}">Back</a>
                        </div>

                    </div>

                    <div class="card-body">

                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <form action="{{ route('meetings.update', $meeting) }}" method="POST"
                            enctype="multipart/form-data">
                            @csrf
                            @method('PUT')
                            <input type="hidden" name="jenis" value="{{ $meeting->jenis }}">

                            <div class="form-group row mb-2">
                                <div class="col-3">Tujuan</div>
                                <div class="col">
                                    <input type="radio" {{ $meeting->tujuan == 'Makluman' ? 'checked' : '' }}
                                        name="tujuan" value="Makluman">
                                    <label for="Makluman">Makluman</label>

                                    <input type="radio" {{ $meeting->tujuan == 'Tindakan Segera' ? 'checked' : '' }}
                                        name="tujuan" value="Tindakan Segera">
                                    <label for="Tindakan Segera">Tindakan Segera</label>

                                    <input type="radio" {{ $meeting->tujuan == 'Tindakan Serta Merta' ? 'checked' : '' }}
                                        name="tujuan" value="Tindakan Serta Merta">
                                    <label for="Tindakan Serta Merta">Tindakan Serta Merta</label>

                                    <input type="radio" {{ $meeting->tujuan == 'Tindakan Susulan' ? 'checked' : '' }}
                                        name="tujuan" value="Tindakan Susulan">
                                    <label for="Tindakan Susulan">Tindakan Susulan</label>
                                </div>
                            </div>

                            <div class="form-group row mb-2">
                                <div class="col-3">Tarikh Mesyuarat</div>
                                <div class="col">
                                    <input type="date" name="tarikh_mesyuarat"
                                        value="{{ date('Y-m-d', strtotime($meeting->tarikh_mesyuarat)) }}"
                                        placeholder="Tarikh Mesyuarat"
                                        class="form-control @error('tarikh_mesyuarat') is-invalid @enderror">
                                    @error('tarikh_mesyuarat')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>

                            <div class="card">
                                <div class="card-header">Maklumat Mesyuarat</div>
                                <div class="card-body">
                                    <div class="form-group row mb-2">
                                        <div class="col-3">Status Minit</div>
                                        <div class="col">
                                            <input type="radio" {{ $meeting->is_approved === 1 ? 'checked' : '' }}
                                                name="is_approved" value="1">
                                            <label for="Telah Disahkan">Telah Disahkan</label>

                                            <input type="radio" {{ $meeting->is_approved === 0 ? 'checked' : '' }}
                                                name="is_approved" value="0">
                                            <label for="Belum Disahkan">Belum Disahkan</label>
                                        </div>
                                    </div>

                                    <div class="form-group row mb-2">
                                        <div class="col-3">Tarikh Status</div>
                                        <div class="col">
                                            <input type="date" name="approved_date"
                                                value="{{ date('Y-m-d', strtotime($meeting->approved_date)) }}"
                                                placeholder="Tarikh Status"
                                                class="form-control @error('approved_date') is-invalid @enderror">
                                            @error('approved_date')
                                                <div class="invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group row mb-2">
                                        <div class="col-3">No Rujukan Fail</div>
                                        <div class="col">
                                            <input type="text" name="no_rujukan_fail"
                                                value="{{ $meeting->no_rujukan_fail }}" placeholder="No Rujukan Fail"
                                                class="form-control @error('no_rujukan_fail') is-invalid @enderror">
                                            @error('no_rujukan_fail')
                                                <div class="invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group row mb-2">
                                        <div class="col-3">Tajuk / Perkara</div>
                                        <div class="col">
                                            <input type="text" name="name" value="{{ $meeting->name }}"
                                                placeholder="Tajuk / Perkara"
                                                class="form-control @error('name') is-invalid @enderror">
                                            @error('name')
                                                <div class="invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group row mb-2">
                                        <div class="col-3">Minit Mesyuarat</div>
                                        <div class="col">
                                            <textarea name="ringkasan" class="form-control  @error('ringkasan') is-invalid @enderror">{{ $meeting->ringkasan }}</textarea>
                                            @error('name')
                                                <div class="invalid-feedback">{{ $message }}</div>
                                            @enderror

                                        </div>
                                    </div>

                                    <div class="form-group row mb-2">
                                        <div class="col-3">Tarikh Akhir Maklum Balas </div>
                                        <div class="col">
                                            <input type="datetime-local" name="tarikh_tutup"
                                                value="{{ $meeting->tarikh_tutup }}"
                                                class="form-control  @error('tarikh_tutup') is-invalid @enderror">
                                            @error('tarikh_tutup')
                                                <div class="invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div class="card my-4">
                                <div class="card-header">Lampiran Minit</div>
                                <div class="card-body">
                                    <table class="table text-center mb-4">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Nama</th>
                                                <th>Tarikh Muat Naik</th>
                                                <th>Tindakan</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @forelse ($meeting->lampirans as $lampiran)
                                                <tr>
                                                    <td>{{ $loop->iteration }}</td>
                                                    <td>{{ $lampiran->name }}</td>
                                                    <td>{{ $lampiran->created_at }}</td>
                                                    <td><a href="{{ route('lampirans.download', $lampiran) }}">Muat
                                                            Turun</a></td>
                                                </tr>
                                            @empty
                                                <tr>
                                                    <td colspan="4">No Data</td>
                                                </tr>
                                            @endforelse

                                        </tbody>
                                    </table>

                                    <div class="card my-2">
                                        <div class="card-header">Sekiranya terdapat lampiran, sila muat naik lampiran
                                            tersebut.</div>
                                        <div class="card-body">
                                            <div class="form-group">
                                                <label for="">Nama Lampiran</label>
                                                <input type="text" name="nama_lampiran_mesyuarat"
                                                    class="form-control">
                                            </div>

                                            <div class="form-group">
                                                <label for="">Fail Lampiran</label>
                                                <input type="file" name="fail_lampiran_mesyuarat"
                                                    class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="card my-4">
                                <div class="card-header">Lampiran Minit</div>
                                <div class="card-body">
                                    <table class="table text-center mb-4">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Nama</th>
                                                <th>Tarikh Muat Naik</th>
                                                <th>Tindakan</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @forelse ($meeting->lampirans as $lampiran)
                                                <tr>
                                                    <td>{{ $loop->iteration }}</td>
                                                    <td>{{ $lampiran->name }}</td>
                                                    <td>{{ $lampiran->created_at }}</td>
                                                    <td><a href="{{ route('lampirans.download', $lampiran) }}">Muat
                                                            Turun</a></td>
                                                </tr>
                                            @empty
                                                <tr>
                                                    <td colspan="4">No Data</td>
                                                </tr>
                                            @endforelse

                                        </tbody>
                                    </table>

                                    <div class="card my-2">
                                        <div class="card-header">Sekiranya terdapat lampiran, sila muat naik lampiran
                                            tersebut.</div>
                                        <div class="card-body">
                                            <div class="form-group">
                                                <label for="">Nama Lampiran</label>
                                                <input type="text" name="nama_lampiran_mesyuarat"
                                                    class="form-control">
                                            </div>

                                            <div class="form-group">
                                                <label for="">Fail Lampiran</label>
                                                <input type="file" name="fail_lampiran_mesyuarat"
                                                    class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="card my-4">
                                <div class="card-header d-flex justify-content-between">
                                    Cabutan
                                    <button class="btn btn-primary">+</button>
                                </div>
                                <div class="card-body">
                                    <div class="card">
                                        <div class="card-body">
                                            <h6>Cabutan 1</h6>
                                            <textarea name="cabutan1" class="form-control"></textarea>
                                            <label>Agihan</label>
                                            <div>
                                                @foreach ($departments as $department)
                                                    <input type="checkbox" name="cabutan1Department[]"
                                                        value="{{ $department->shortcode }}"><label
                                                        class="ml-2">{{ $department->name }}
                                                        ({{ $department->section }})
                                                    </label><br />
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <button class="float-end btn btn-primary" type="submit">Kemaskini</button>
                        </form>
                    </div>

                </div>


            </div>
        </div>
    </div>
    </div>
@endsection
