@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-footer my-auto clearfix"> Keputusan Mesyuarat
                        @if (request()->jenis == 'mesyuarat-jemaah-menteri')
                            Jemaah Menteri
                        @elseif (request()->jenis == 'mesyuarat-utama')
                            Utama
                        @else
                            '[Tiada Jenis]'
                        @endif

                        {{-- @hasanyrole('Pentadbir') --}}
                        <a class="btn btn-primary float-right"
                        href="{{ route('meetings.create', ['jenis' => request()->jenis]) }}">Cipta
                        baharu</a>
                        {{-- @endhasanyrole --}}
                    </div>


                    {{-- <div class="card-header d-flex justify-content-between">
                        <div class="my-auto">Keputusan Mesyuarat
                            @if (request()->jenis == 'mesyuarat-jemaah-menteri')
                                Jemaah Menteri
                            @elseif (request()->jenis == 'mesyuarat-utama')
                                Utama
                            @else
                                '[Tiada Jenis]'
                            @endif
                        </div>

                        <a class="btn btn-primary float-right"
                            href="{{ route('meetings.create', ['jenis' => request()->jenis]) }}">Cipta
                            {{ ucfirst(request()->jenis) }}
                            baharu</a>
                    </div> --}}

                    <div class="row" style="margin: 16px 16px 0 16px">
                        <x-carian reset-route="{{ route('documents.index', request()->jenis) }}"></x-carian>
                    </div>

                    <div class="card-body table-responsive">
                        <x-success-message-banner></x-success-message-banner>
                        <x-error-message-banner></x-error-message-banner>
                        <table class="table text-center">
                            <thead>
                                <tr>
                                    <th>Bil</th>
                                    <th>Tarikh Minit</th>
                                    <th>Tujuan</th>
                                    <th>No Fail / Tajuk Minit</th>
                                    <th>Tarikh Akhir Maklum Balas</th>
                                    <th>Tindakan</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($meetings as $meeting)
                                    <tr>
                                        <td>{{ $loop->iteration + $meetings->firstItem() - 1 }}</td>
                                        <td>{{ $meeting->tarikh_mesyuarat }}</td>
                                        <td>{{ $meeting->tujuan }}</td>
                                        <td>{{ $meeting->no_rujukan_fail }} / {{ $meeting->name }}</td>
                                        <td>{{ $meeting->tarikh_tutup != null ? $meeting->tarikh_tutup : 'Belum Ditetapkan' }}
                                        </td>
                                        <td>
                                            {{-- <a href="{{ route('meetings.show', ['jenis' => request()->jenis, 'meeting' => $meeting]) }}"
                                                class="btn btn-warning">Lihat</a> --}}
                                            {{-- @hasanyrole('Pentadbir') --}}
                                            <a href="{{ route('meetings.edit', ['jenis' => request()->jenis, 'meeting' => $meeting]) }}"
                                                class="btn btn-warning">Kemaskini</a>
                                            <button type="button" class="btn btn-danger"
                                                onclick="deleteMeeting(event, `Hapus mesyuarat '{{ $meeting->name }}' ?`, 'delete_meeting_form_{{ $meeting->id }}')">
                                                Hapus
                                            </button>
                                            <form id="delete_meeting_form_{{ $meeting->id }}"
                                                action="{{ route('meetings.destroy', ['jenis' => request()->jenis, 'meeting' => $meeting]) }}"
                                                method="POST">
                                                @csrf
                                                @method('DELETE')
                                            </form>
                                                {{-- @endhasanyrole --}}
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="6">Tiada dokumen.</td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                        {{ $meetings->withQueryString()->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
<script>
    const deleteMeeting = (event, message, formId) => {
        event.preventDefault();
        window.confirmDialog(message, () => {
            $("#" + formId).submit()
        })
    }
</script>
