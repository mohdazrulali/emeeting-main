@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header d-flex justify-content-between">
                    <div class="my-auto">Keputusan Mesyuarat
                        @if ($jenis == 'mesyuarat-jemaah-menteri')
                        Jemaah Menteri
                        @elseif ($jenis == 'mesyuarat-utama')
                        Utama
                        @else
                        ERROR
                        @endif
                    </div>
                    <a class="btn btn-outline-primary" href="{{ route('meetings.index', ['jenis' => $jenis]) }}">Back</a>
                </div>

                <div class="card-body">

                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                    <form action="{{ route('meetings.store') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="jenis" value="{{ $jenis }}">

                        <div class="form-group row mb-2">
                            <div class="col-3">Tujuan</div>
                            <div class="col">
                                <div class="form-check form-check-inline ps-0">
                                    <input type="radio" name="tujuan" value="Makluman" checked>
                                    <label for="Makluman">Makluman</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input type="radio" name="tujuan" value="Tindakan Segera">
                                    <label for="Tindakan Segera">Tindakan Segera</label>
                                </div>

                                <div class="form-check form-check-inline">
                                    <input type="radio" name="tujuan" value="Tindakan Serta Merta">
                                    <label for="Tindakan Serta Merta">Tindakan Serta Merta</label>
                                </div>

                                <div class="form-check form-check-inline">
                                    <input type="radio" name="tujuan" value="Tindakan Susulan">
                                    <label for="Tindakan Susulan">Tindakan Susulan</label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-2">
                            <div class="col-3">Tarikh Mesyuarat</div>
                            <div class="col">
                                <input type="date" name="tarikh_mesyuarat" placeholder="Tarikh Mesyuarat" class="form-control @error('tarikh_mesyuarat') is-invalid @enderror">
                                @error('tarikh_mesyuarat')
                                <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="card">
                            <div class="card-header">Maklumat Mesyuarat</div>
                            <div class="card-body">
                                <div class="form-group row mb-2">
                                    <div class="col-3">Status Minit</div>
                                    <div class="col">
                                        <div class="form-check form-check-inline ps-0">
                                            <input type="radio" name="is_approved" checked value="1">
                                            <label for="Telah Disahkan">Telah Disahkan</label>
                                        </div>

                                        <div class="form-check form-check-inline">
                                            <input type="radio" name="is_approved" value="0">
                                            <label for="Belum Disahkan">Belum Disahkan</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row mb-2">
                                    <div class="col-3">Tarikh Status</div>
                                    <div class="col">
                                        <input type="date" name="approved_date" placeholder="Tarikh Status" class="form-control @error('approved_date') is-invalid @enderror">
                                        @error('approved_date')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row mb-2">
                                    <div class="col-3">No Rujukan Fail</div>
                                    <div class="col">
                                        <input type="text" name="no_rujukan_fail" placeholder="No Rujukan Fail" class="form-control @error('no_rujukan_fail') is-invalid @enderror">
                                        @error('no_rujukan_fail')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row mb-2">
                                    <div class="col-3">Tajuk /Perkara</div>
                                    <div class="col">
                                        <input type="text" name="name" placeholder="No Rujukan Fail" class="form-control @error('name') is-invalid @enderror">
                                        @error('name')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row mb-2">
                                    <div class="col-3">Minit Mesyuarat</div>
                                    <div class="col">
                                        <textarea name="ringkasan" class="form-control  @error('ringkasan') is-invalid @enderror"></textarea>
                                        @error('name')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror

                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="card my-4">
                            <div class="card-header">Lampiran Minit</div>
                            <div class="card-body">
                                <table class="table text-center mb-4">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Nama</th>
                                            <th>Tarikh Muat Naik</th>
                                            <th>Tindakan</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>1</td>
                                            <td>File Saya</td>
                                            <td>Tarikh</td>
                                            <td>Padam</td>
                                        </tr>
                                    </tbody>
                                </table>

                                <div class="card my-2">
                                    <div class="card-header">Sekiranya terdapat lampiran, sila muat naik lampiran
                                        tersebut.</div>
                                    <div class="card-body">
                                        <div class="form-group">
                                            <label for="">Nama Lampiran</label>
                                            <input type="text" name="nama_lampiran_mesyuarat" class="form-control">
                                        </div>

                                        <div class="form-group">
                                            <label for="">Fail Lampiran</label>
                                            <input type="file" name="fail_lampiran_mesyuarat" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        {{-- <div class="card my-4">
                                <div class="card-header">Mesyuarat</div>
                                <div class="card-body">
                                    <table class="table text-center mb-4">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Nama</th>
                                                <th>Tarikh Muat Naik</th>
                                                <th>Tindakan</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>1</td>
                                                <td>File Saya</td>
                                                <td>Tarikh</td>
                                                <td>Padam</td>
                                            </tr>
                                        </tbody>
                                    </table>

                                    <div class="card my-2">
                                        <div class="card-header">Sekiranya terdapat lampiran, sila muat naik lampiran
                                            tersebut.</div>
                                        <div class="card-body">
                                            <div class="form-group">
                                                <label for="">Nama Lampiran</label>
                                                <input type="text" name="nama_lampiran_mesyuarat" class="form-control">
                                            </div>

                                            <div class="form-group">
                                                <label for="">Fail Lampiran</label>
                                                <input type="file" name="fail_lampiran_mesyuarat"
                                                    class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="card my-4">
                                <div class="card-header">Lampiran Lain</div>
                                <div class="card-body">
                                    <table class="table text-center mb-4">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Nama</th>
                                                <th>Tarikh Muat Naik</th>
                                                <th>Tindakan</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>1</td>
                                                <td>File Saya</td>
                                                <td>Tarikh</td>
                                                <td>Padam</td>
                                            </tr>
                                        </tbody>
                                    </table>

                                    <div class="card my-2">
                                        <div class="card-header">Sekiranya terdapat lampiran, sila muat naik lampiran
                                            tersebut.</div>
                                        <div class="card-body">
                                            <div class="form-group">
                                                <label for="">Nama Lampiran</label>
                                                <input type="text" name="nama_lampiran_lain" class="form-control">
                                            </div>

                                            <div class="form-group">
                                                <label for="">Fail Lampiran</label>
                                                <input type="file" name="fail_lampiran_lain" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="card my-4">
                                <div class="card-header">Ringkasan Dokumen</div>
                                <div class="card-body">
                                    <table class="table text-center mb-4">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Nama</th>
                                                <th>Tarikh Muat Naik</th>
                                                <th>Tindakan</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>1</td>
                                                <td>File Saya</td>
                                                <td>Tarikh</td>
                                                <td>Padam</td>
                                            </tr>
                                        </tbody>
                                    </table>

                                    <div class="card my-2">
                                        <div class="card-header">Sekiranya terdapat lampiran, sila muat naik lampiran
                                            tersebut.</div>
                                        <div class="card-body">
                                            <div class="form-group">
                                                <label for="">Nama Lampiran</label>
                                                <input type="text" name="nama_lampiran_tambahan" class="form-control">
                                            </div>

                                            <div class="form-group">
                                                <label for="">Fail Lampiran</label>
                                                <input type="file" name="fail_lampiran_tambahan" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="card my-4">
                                <div class="card-header">Maklumat Tambahan</div>
                                <div class="card-body">
                                    <table class="table text-center mb-4">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Nama</th>
                                                <th>Tarikh Muat Naik</th>
                                                <th>Tindakan</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>1</td>
                                                <td>File Saya</td>
                                                <td>Tarikh</td>
                                                <td>Padam</td>
                                            </tr>
                                        </tbody>
                                    </table>

                                    <div class="card my-2">
                                        <div class="card-header">Sekiranya terdapat lampiran, sila muat naik lampiran
                                            tersebut.</div>
                                        <div class="card-body">
                                            <div class="form-group">
                                                <label for="">Nama Lampiran</label>
                                                <input type="text" name="nama_lampiran_ringkasan"
                                                    class="form-control">
                                            </div>

                                            <div class="form-group">
                                                <label for="">Fail Lampiran</label>
                                                <input type="file" name="fail_lampiran_ringkasan"
                                                    class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div> --}}

                        <button class="float-end btn btn-primary" type="submit">Save</button>
                    </form>
                </div>

            </div>


        </div>
    </div>
</div>
</div>
@endsection
