@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    Kemaskini Keputusan Mesyuarat
                    @if (request()->jenis == 'mesyuarat-jemaah-menteri')
                    Jemaah Menteri
                    @elseif (request()->jenis == 'mesyuarat-utama')
                    Utama
                    @else
                    [Tiada Jenis]
                    @endif
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12 d-flex justify-content-between">
                            <ul class="nav nav-pills" role="tablist">
                                <li class="nav-item" role="presentation">
                                    <a class="nav-link @if(request()->query('step') === '1') active disabled @endif" href="{{ route('meetings.edit', ['jenis'   => request()->jenis, 'step' => 1, 'meeting' => request()->meeting]) }}" role="tab">1. Maklumat
                                        Keputusan Mesyuarat
                                        @if (request()->jenis == 'mesyuarat-jemaah-menteri')
                                        Jemaah Menteri
                                        @elseif (request()->jenis == 'mesyuarat-utama')
                                        Utama
                                        @else
                                        [Tiada Jenis]
                                        @endif</a>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <a class="nav-link @if(request()->query('step') === '2') active disabled @endif" href="{{ route('meetings.edit', ['jenis'   => request()->jenis, 'step' => 2, 'meeting' => request()->meeting]) }}" role="tab">2. Maklumat Lampiran</a>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <a class="nav-link @if(request()->query('step') === '3') active disabled @endif" href="{{ route('meetings.edit', ['jenis'   => request()->jenis, 'step' => 3, 'meeting' => request()->meeting]) }}" role="tab">3. Maklumat Agihan</a>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <a class="nav-link @if(request()->query('step') === '4') active disabled @endif" href="{{ route('meetings.edit', ['jenis'   => request()->jenis, 'step' => 4, 'meeting' => request()->meeting]) }}" role="tab">4. Maklumat Cabutan</a>
                                </li>
                            </ul>
                            @switch(request()->query('step'))
                            @case('1')
                            <div>
                                <a type="button" class="btn btn-outline-primary" href="{{ route('meetings.index', ['jenis'   => request()->jenis]) }}">
                                    <i class="fa-solid fa-angle-left" style="margin-right: 8px;"></i>Kembali
                                </a>
                                <a type="button" class="btn btn-primary" href="{{ route('meetings.edit', ['jenis'   => request()->jenis, 'meeting' => request()->meeting, 'step' => 2]) }}">
                                    Seterusnya<i class="fa-solid fa-angle-right" style="margin-left: 8px;"></i>
                                </a>
                            </div>
                            @break
                            @case('2')
                            <div>
                                <a type="button" class="btn btn-outline-primary" href="{{ route('meetings.edit', ['jenis'   => request()->jenis, 'meeting' => request()->meeting, 'step' => 1]) }}">
                                    <i class="fa-solid fa-angle-left" style="margin-right: 8px;"></i>Kembali
                                </a>
                                <a type="button" class="btn btn-primary" href="{{ route('meetings.edit', ['jenis'   => request()->jenis, 'meeting' => request()->meeting, 'step' => 3]) }}">
                                    Seterusnya<i class="fa-solid fa-angle-right" style="margin-left: 8px;"></i>
                                </a>
                            </div>
                            @break
                            @case('3')
                            <div>
                                <a type="button" class="btn btn-outline-primary" href="{{ route('meetings.edit', ['jenis'   => request()->jenis, 'meeting' => request()->meeting, 'step' => 2]) }}">
                                    <i class="fa-solid fa-angle-left" style="margin-right: 8px;"></i>Kembali
                                </a>
                                <a type="button" class="btn btn-primary" href="{{ route('meetings.edit', ['jenis'   => request()->jenis, 'meeting' => request()->meeting, 'step' => 4]) }}">
                                    Seterusnya<i class="fa-solid fa-angle-right" style="margin-left: 8px;"></i>
                                </a>
                            </div>
                            @break
                            @case('4')
                            <div>
                                <a type="button" class="btn btn-outline-primary" href="{{ route('meetings.edit', ['jenis'   => request()->jenis, 'meeting' => request()->meeting, 'step' => 3]) }}">
                                    <i class="fa-solid fa-angle-left" style="margin-right: 8px;"></i>Kembali
                                </a>
                                <a type="button" class="btn btn-primary" href="{{ route('meetings.index', ['jenis'   => request()->jenis]) }}">
                                    Selesai<i class="fa-solid fa-check" style="margin-left: 8px;"></i>
                                </a>
                            </div>
                            @break
                            @endswitch
                        </div>

                        <div class="col-md-12 mt-2">
                            @if (session('status'))
                            <div class="alert alert-{{ session('status.type') }}" role="alert">
                                {{ session('status.message') }}
                            </div>
                            @endif


                            @if ($errors->any())
                            <div class="alert alert-danger mb-0">
                                <ul style="margin-bottom: 0;">
                                    @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                            @endif
                        </div>

                        <div class="col-md-12 mt-2">
                            <div class="tab-content">
                                <div class="tab-pane @if(request()->query('step') === '1') active @endif" role="tabpanel">
                                    <div class="card mt-2">
                                        <div class="card-body">
                                            <form action="{{ route('meetings.update', ['jenis'   => request()->jenis, $meeting->id]) }}" method="POST" enctype="multipart/form-data" autocomplete="off">
                                                @csrf
                                                @method('PUT')
                                                <input type="hidden" name="jenis" value="{{ request()->jenis }}">

                                                <div class="form-group row mb-2">
                                                    <div class="col-3">Tujuan</div>
                                                    <div class="col">
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="tujuan" value="Makluman" @if($meeting->tujuan === 'Makluman' ) checked @endif>
                                                            <label class="form-check-label" for="Makluman">Makluman</label>
                                                        </div>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="tujuan" value="Tindakan Segera" @if($meeting->tujuan === 'Tindakan Segera' ) checked @endif>
                                                            <label class="form-check-label" for="Tindakan Segera">Tindakan Segera</label>
                                                        </div>

                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="tujuan" value="Tindakan Serta Merta" @if($meeting->tujuan === 'Tindakan Serta Merta' ) checked @endif>
                                                            <label class="form-check-label" for="Tindakan Serta Merta">Tindakan Serta Merta</label>
                                                        </div>

                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="tujuan" value="Tindakan Susulan" @if($meeting->tujuan === 'Tindakan Susulan' ) checked @endif>
                                                            <label class="form-check-label" for="Tindakan Susulan">Tindakan Susulan</label>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group d-flex mb-2">
                                                    <div class="col-3">Tarikh Mesyuarat</div>
                                                    <div class="col">
                                                        <input type="date" name="tarikh_mesyuarat" placeholder="Tarikh Mesyuarat" class="form-control @error('tarikh_mesyuarat') is-invalid @enderror" value="{{ date('Y-m-d', strtotime($meeting->tarikh_mesyuarat)) }}">
                                                        @error('tarikh_mesyuarat')
                                                        <div class="invalid-feedback">{{ $message }}</div>
                                                        @enderror
                                                    </div>
                                                </div>

                                                <div class="card">
                                                    <div class="card-header">Maklumat Mesyuarat</div>
                                                    <div class="card-body">
                                                        <div class="form-group row mb-2">
                                                            <div class="col-3">Status Minit</div>
                                                            <div class="col">
                                                                <div class="form-check form-check-inline">
                                                                    <input class="form-check-input" type="radio" name="is_approved" value="1" @if($meeting->is_approved == '1' ) checked @endif>
                                                                    <label class="form-check-label" for="Telah Disahkan">Telah Disahkan</label>
                                                                </div>

                                                                <div class="form-check form-check-inline">
                                                                    <input type="radio" name="is_approved" value="0" @if($meeting->is_approved == '0' ) checked @endif>
                                                                    <label for="Belum Disahkan">Belum Disahkan</label>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="form-group row mb-2">
                                                            <div class="col-3">Tarikh Status</div>
                                                            <div class="col">
                                                                <input type="date" name="approved_date" placeholder="Tarikh Status" class="form-control @error('approved_date') is-invalid @enderror" value="{{ date('Y-m-d', strtotime($meeting->approved_date)) }}">
                                                                @error('approved_date')
                                                                <div class="invalid-feedback">{{ $message }}</div>
                                                                @enderror
                                                            </div>
                                                        </div>

                                                        <div class="form-group row mb-2">
                                                            <div class="col-3">No Rujukan Fail</div>
                                                            <div class="col">
                                                                <input type="text" name="no_rujukan_fail" placeholder="No Rujukan Fail" class="form-control @error('no_rujukan_fail') is-invalid @enderror" value="{{ $meeting->no_rujukan_fail }}">
                                                                @error('no_rujukan_fail')
                                                                <div class="invalid-feedback">{{ $message }}</div>
                                                                @enderror
                                                            </div>
                                                        </div>

                                                        <div class="form-group row mb-2">
                                                            <div class="col-3">Tajuk /Perkara</div>
                                                            <div class="col">
                                                                <input type="text" name="name" placeholder="No Rujukan Fail" class="form-control @error('name') is-invalid @enderror" value="{{ $meeting->name }}">
                                                                @error('name')
                                                                <div class="invalid-feedback">{{ $message }}</div>
                                                                @enderror
                                                            </div>
                                                        </div>

                                                        <div class="form-group row mb-2">
                                                            <div class="col-3">Minit Mesyuarat</div>
                                                            <div class="col">
                                                                <textarea name="ringkasan" class="form-control  @error('ringkasan') is-invalid @enderror">{{ $meeting->ringkasan }}</textarea>
                                                                @error('ringkasan')
                                                                <div class="invalid-feedback">{{ $message }}</div>
                                                                @enderror

                                                            </div>
                                                        </div>

                                                        <div class="form-group row mb-2">
                                                            <div class="col-3">Tarikh Akhir Maklum Balas </div>
                                                            <div class="col">
                                                                <input type="datetime-local" name="tarikh_tutup" value="{{ $meeting->tarikh_tutup }}" class="form-control  @error('tarikh_tutup') is-invalid @enderror" value="{{ date('Y-m-d', strtotime($meeting->tarikh_tutup)) }}">
                                                                @error('tarikh_tutup')
                                                                <div class="invalid-feedback">{{ $message }}</div>
                                                                @enderror
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                                <div class="form-group mt-2 d-flex justify-content-end">
                                                    <button type="submit" class="btn btn-primary">Simpan <i class="fa-regular fa-floppy-disk" style="margin-left: 8px;"></i></button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane @if(request()->query('step') === '2') active @endif" role="tabpanel">
                                    <div class="card mb-2">
                                        <div class="card-header">Lampiran Minit</div>
                                        <div class="card-body">
                                            <div class="card my-2">
                                                <div class="card-header">Tambah Lampiran</div>
                                                <div class="card-body">
                                                    <form action="{{ route('meetings.lampirans.store', ['jenis' => 'request()->jenis', request()->meeting]) }}" method="POST" enctype="multipart/form-data" autocomplete="off">
                                                        @csrf
                                                        <input type="hidden" name="jenis_lampiran" value="mesyuarat">
                                                        <div class="form-group">
                                                            <label for="">Nama Lampiran</label>
                                                            <div class="d-flex col-md-12">
                                                                <div class="col-md-8">
                                                                    <input type="text" name="nama_lampiran" value="{{ old('nama_lampiran') }}" class="form-control @error('nama_lampiran') is-invalid @enderror">
                                                                    @error('nama_lampiran')
                                                                    <div class="invalid-feedback">
                                                                        {{ $message }}
                                                                    </div>
                                                                    @enderror
                                                                </div>
                                                                <div class="col ms-2">
                                                                    <input type="file" name="fail_lampiran" accept=".pdf" class="form-control @error('fail_lampiran') is-invalid @enderror">
                                                                    @error('fail_lampiran')
                                                                    <div class="invalid-feedback">
                                                                        {{ $message }}
                                                                    </div>
                                                                    @enderror
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group mt-2 d-flex justify-content-end">
                                                            <button class="btn btn-primary" type="submit">Tambah</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                            <table class="table text-center mb-4">
                                                <thead>
                                                    <tr>
                                                        <th>Bil</th>
                                                        <th>Nama</th>
                                                        <th>Tarikh Muat Naik</th>
                                                        <th>Tindakan</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @forelse($lampirans as $lampiran)
                                                    <tr>
                                                        <td>{{ $loop->iteration + $lampirans->firstItem() - 1 }}</td>
                                                        <td>{{ $lampiran->name }}</td>
                                                        <td>{{ date('d-m-Y', strtotime($lampiran->created_at)) }}</td>
                                                        <td>
                                                            <a class="btn btn-primary" href="{{ route('view.docs', $lampiran->id) }}" target="_blank">Lihat</a>
                                                            <button type="button" onclick="deleteLampiran(`{{ route('lampirans.destroy', $lampiran->id) }}`, `{{ $lampiran->name }}`)" class="btn btn-danger">Padam</button>
                                                        </td>
                                                    </tr>
                                                    @empty
                                                    <tr>
                                                        <td colspan="4">Tiada lampiran.</td>
                                                    </tr>
                                                    @endforelse
                                                </tbody>
                                            </table>
                                            {{ $lampirans->withQueryString()->links() }}
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane @if(request()->query('step') === '3') active @endif" role="tabpanel">
                                    <div class="card">
                                        <div class="card-header">
                                            Agihan Kepada
                                        </div>
                                        <div class="card-body">
                                            <form id="reset_agihan_dokumen_form" action="{{ route('meetings.agihans.reset', ['jenis'   => request()->jenis, $meeting->id]) }}" method="POST">
                                                @csrf
                                            </form>
                                            <form action="{{ route('meetings.agihans.store', ['jenis'   => request()->jenis, $meeting->id]) }}" method="POST">
                                                @csrf
                                                <input type="hidden" name="model_id" value="{{ request()->meeting->id }}">
                                                <input type="hidden" name="jenis" value="meeting">
                                                <div class="form-group">
                                                    <select class="selectpicker form-control" name="departments[]" multiple="multiple">
                                                        @foreach ($departments as $department)
                                                        <option value="{{ $department->shortcode }}" @if(in_array($department->shortcode, $meeting->jabatan_agihans)) selected @endif>
                                                            {{ $department->name }}
                                                            ({{ $department->shortcode }})
                                                        </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="form-group mt-2 d-flex justify-content-end">
                                                    @if(count($meeting->jabatan_agihans) > 0)
                                                    <button type="button" onclick="resetAgihanDokumen(event)" class="btn btn-danger"><i class="fa-solid fa-rotate-right" style="margin-right: 8px;"></i>Set Semula</button>
                                                    <button type="submit" class="btn btn-primary ms-1"><i class="fa-solid fa-paper-plane" style="margin-right: 8px;"></i>Kemaskini dan Hantar</button>
                                                    @else
                                                    <button type="submit" class="btn btn-primary ms-1"><i class="fa-solid fa-paper-plane" style="margin-right: 8px;"></i>Hantar</button>
                                                    @endif
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                    <div class="card mt-2">
                                        <div class="card-header">
                                            Penerima Agihan
                                        </div>
                                        <div class="card-body">
                                            <table class="table text-center">
                                                <thead>
                                                    <tr>
                                                        <th>No</th>
                                                        <th style="text-align: left">Alamat Emel</th>
                                                        <th>Tarikh Hantar</th>
                                                        <th>Tindakan</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @forelse($meeting->agihans as $agihan)
                                                    <tr>
                                                        <td>{{ $loop->iteration }}</td>
                                                        <td style="text-align: left">{{ $agihan->penerima }}</td>
                                                        <td>{{ date('d/m/Y h:i a', strtotime($agihan->updated_at)) }}</td>
                                                        <td>
                                                            <form action="{{ route('meetings.agihans.resend', ['jenis'   => request()->jenis, $meeting->id]) }}" method="POST">
                                                                @csrf
                                                                <input type="hidden" name="jenis" value="meeting">
                                                                <input type="hidden" name="model_id" value="{{ request()->meeting->id }}">
                                                                <input type="hidden" name="email_penerima" value="{{ $agihan->penerima }}">
                                                                <button class="btn btn-primary" type="submit">Hantar Semula</button>
                                                            </form>
                                                        </td>
                                                    </tr>
                                                    @empty
                                                    <tr>
                                                        <td colspan="4">Tiada agihan.</td>
                                                    </tr>
                                                    @endforelse
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane @if(request()->query('step') === '4') active @endif" role="tabpanel">
                                    @if(!request()->kemaskini_cabutan)
                                    <div class="card" id="add_cabutan_card">
                                        <div class="card-header">
                                            Tambah Cabutan
                                        </div>
                                        <div class="card-body">
                                            <form action="{{ route('meetings.cabutans.store', ['jenis'   => request()->jenis, $meeting->id]) }}" method="POST">
                                                @csrf
                                                <div class="form-group mb-2">
                                                    <label>Catatan</label>
                                                    <textarea name="content" rows="5" class="form-control @error('content') is-invalid @enderror" placeholder="Catatan...">{{ old('content') }}</textarea>
                                                    @error('content')
                                                    <div class="invalid-feedback">
                                                        {{ $message }}
                                                    </div>
                                                    @enderror
                                                </div>
                                                <div class="form-group">
                                                    <label>Cabutan</label>
                                                    <select class="selectpicker form-control @error('cabutan_departments') is-invalid @enderror" name="cabutan_departments[]" multiple="multiple">
                                                        @foreach ($departments as $department)
                                                        <option value="{{ $department->shortcode }}" @if(old('cabutan_departments') && in_array($department->shortcode, old('cabutan_departments'))) selected @endif>
                                                            {{ $department->name }}
                                                            ({{ $department->shortcode }})
                                                        </option>
                                                        @endforeach
                                                    </select>
                                                    @error('cabutan_departments')
                                                    <div class="invalid-feedback">
                                                        {{ $message }}
                                                    </div>
                                                    @enderror
                                                </div>
                                                <div class="form-group mt-2 d-flex justify-content-end">
                                                    <button type="submit" class="btn btn-primary">Simpan</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                    @endif
                                    @foreach($cabutans as $cabutan)
                                    @if(request()->kemaskini_cabutan == $cabutan->id)
                                    <div class="card" id="edit_cabutan_{{ $cabutan->id }}_card">
                                        <div class="card-header">
                                            <div style="display: flex; justify-content: space-between; align-items: center;">
                                                <span>Kemaskini Cabutan</span>
                                                <a class="btn btn-light btn-sm" href="{{ route('meetings.edit', ['jenis'   => request()->jenis, $meeting->id, 'step' => 4]) }}"><i class="fa-solid fa-xmark"></i></a>
                                            </div>
                                        </div>
                                        <div class="card-body">
                                            <form action="{{ route('meetings.cabutans.update', [request()->route('jenis'), $meeting->id, $cabutan->id]) }}" method="POST">
                                                @csrf
                                                @method('PUT')
                                                <div class="form-group mb-2">
                                                    <label>Catatan</label>
                                                    <textarea name="content" rows="5" class="form-control @error('content') is-invalid @enderror" placeholder="Catatan...">{{ $cabutan->content }}</textarea>
                                                    @error('content')
                                                    <div class="invalid-feedback">
                                                        {{ $message }}
                                                    </div>
                                                    @enderror
                                                </div>
                                                <div class="form-group">
                                                    <label>Cabutan</label>
                                                    <select class="selectpicker form-control @error('cabutan_departments') is-invalid @enderror" name="cabutan_departments[]" multiple="multiple">
                                                        @foreach ($departments as $department)
                                                        <option value="{{ $department->shortcode }}" @if(old('cabutan_departments') && in_array($department->shortcode, old('cabutan_departments'))) selected @endif @if(in_array($department->shortcode, $cabutan->jabatans)) selected @endif>
                                                            {{ $department->name }}
                                                            ({{ $department->shortcode }})
                                                        </option>
                                                        @endforeach
                                                    </select>
                                                    @error('cabutan_departments')
                                                    <div class="invalid-feedback">
                                                        {{ $message }}
                                                    </div>
                                                    @enderror
                                                </div>
                                                <div class="card mt-2">
                                                    <div class="card-header">
                                                        Penerima Cabutan
                                                    </div>
                                                    <div class="card-body">
                                                        <table class="table text-center">
                                                            <thead>
                                                                <tr>
                                                                    <th>No</th>
                                                                    <th style="text-align: left">Alamat Emel</th>
                                                                    <th>Tarikh Hantar</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                @forelse($cabutan->agihans as $agihan)
                                                                <tr>
                                                                    <td>{{ $loop->iteration }}</td>
                                                                    <td style="text-align: left">{{ $agihan->penerima }}</td>
                                                                    <td>{{ date('d/m/Y h:i a', strtotime($agihan->updated_at)) }}</td>
                                                                </tr>
                                                                @empty
                                                                <tr>
                                                                    <td colspan="4">Tiada agihan.</td>
                                                                </tr>
                                                                @endforelse
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                <div class="form-group mt-2 d-flex justify-content-end">
                                                    <a type="button" class="btn btn-outline-primary" href="{{ route('meetings.edit', ['jenis'   => request()->jenis, $meeting->id, 'step' => 4]) }}">Batal</a>
                                                    <button type="submit" class="btn btn-primary ms-1"><i class="fa-solid fa-paper-plane" style="margin-right: 8px;"></i>Kemaskini dan Hantar</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                    @endif
                                    @endforeach
                                    <table class="table text-center mt-2" style="width: 100%; table-layout: fixed">
                                        <thead>
                                            <tr>
                                                <th style="width: 10%;">No</th>
                                                <th style="text-align: left">Catatan</th>
                                                <th style="width: 30%;">Tindakan</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @forelse($cabutans as $cabutan)
                                            <tr id="cabutan_{{ $cabutan->id }}">
                                                <td>{{ $loop->iteration }}</td>
                                                <td style="text-align: left; white-space: nowrap; overflow: hidden; text-overflow: ellipsis">{{ $cabutan->content }}</td>
                                                <td>
                                                    <a class="btn btn-primary @if(request()->kemaskini_cabutan == $cabutan->id) disabled @endif" href="{{ route('meetings.edit', ['jenis'   => request()->jenis, $meeting->id, 'step' => 4, 'kemaskini_cabutan' => $cabutan->id]) }}">Kemaskini</a>
                                                    <button class="btn btn-danger" type="button" onclick="deleteCabutan('{{ route('meetings.cabutans.destroy', [request()->route('jenis'), $meeting->id, $cabutan->id]) }}', `{{ $cabutan->content }}`)">Padam</button>
                                                </td>

                                            </tr>
                                            @empty
                                            <tr>
                                                <td colspan="3">Tiada cabutan.</td>
                                            </tr>
                                            @endforelse
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    const deleteLampiran = async (url, name) => {
        window.confirmDialog(`Anda pasti untuk memadam lampiran '${name}'?`, async () => {
            try {
                await window.http(url, "DELETE")
                window.location.reload()
            } catch (error) {
                return;
            }
        })
    }

    const resetAgihanDokumen = (event) => {
        event.preventDefault();
        window.confirmDialog("<p>Set semula agihan?<br>(Semua agihan kepada semua penerima akan dipadam.)</p>", () => {
            $("#reset_agihan_dokumen_form").submit();
        })
    }

    const deleteCabutan = async (url, content) => {
        window.confirmDialog(`Anda pasti untuk memadam catatan \n'${content}'?`, async () => {
            try {
                await window.http(url, "DELETE")
                window.location.reload()
            } catch (error) {
                return;
            }
        })
    }
</script>
@endsection