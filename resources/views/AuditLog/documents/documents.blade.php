@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    Log Audit <strong>{{ request()->route('document')->name }}</strong> ({{ parseDocumentType(request()->route('document')->jenis) }})
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th width="15%" class="text-center">Tarikh/Masa</th>
                                    <th width="85%">Log</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse($logs as $log)
                                <tr>
                                    <td class="text-center"> {{ date('d/m/Y h:i a', strtotime($log->created_at)) }}</td>
                                    <td>{!! parseDocumentLog($log) !!}</td>
                                </tr>
                                @empty
                                <tr>
                                    <td class="text-center" colspan="2">Tiada log.</td>
                                </tr>
                                @endforelse
                            </tbody>
                        </table>
                        {{ $logs->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection