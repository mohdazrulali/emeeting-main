@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    Log Audit Dokumen
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12 d-flex justify-content-between">
                            <ul class="nav nav-pills" role="tablist">
                                <li class="nav-item" role="presentation">
                                    <a class="nav-link @if(request()->route('type') === 'dokumen') active disabled @endif" href="{{ route('logaudit.dokumen.index', ['dokumen']) }}" role="tab">Dokumen</a>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <a class="nav-link @if(request()->route('type') === 'mesyuarat') active disabled @endif" href="{{ route('logaudit.dokumen.index', ['mesyuarat']) }}" role="tab">Mesyuarat</a>
                                </li>
                            </ul>
                        </div>

                        <div class="col-md-12 mt-2">
                            <div class="tab-content">
                                <div class="tab-pane @if(request()->route('type') === 'dokumen') active @endif" role="tabpanel">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="d-flex">
                                                <x-carian reset-route="{{ route('logaudit.dokumen.index', ['dokumen']) }}"></x-carian>
                                            </div>
                                            <div class="table-responsive">
                                                <table class="table text-center">
                                                    <thead>
                                                        <tr>
                                                            <th>Bil</th>
                                                            <th>Tarikh Dokumen</th>
                                                            <th>No. Fail</th>
                                                            <th>Tajuk / Sub Tajuk Dokumen</th>
                                                            <th>Tarikh Kemaskini</th>
                                                            <th></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @forelse ($documents as $document)
                                                        <tr>
                                                            <td>{{ $loop->iteration + $documents->firstItem() - 1 }}</td>
                                                            <td>{{ $document->tarikh_dokumen }}</td>
                                                            <td>{{ $document->no_fail_kementerian }}</td>
                                                            <td>{{ $document->name }} / {{ $document->sub_name }}</td>
                                                            <td>{{ $document->updated_at }}</td>
                                                            <td>
                                                                <a href="{{ route('logaudit.dokumen.document.show', [$document->id]) }}" class="btn btn-primary btn-sm">Lihat</a>
                                                            </td>
                                                        </tr>
                                                        @empty
                                                        <tr>
                                                            <td colspan="6">Tiada dokumen.</td>
                                                        </tr>
                                                        @endforelse
                                                    </tbody>
                                                </table>
                                                {{ $documents->withQueryString()->links() }}
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane @if(request()->route('type') === 'mesyuarat') active @endif" role="tabpanel">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="d-flex">
                                                <x-carian reset-route="{{ route('logaudit.dokumen.index', ['mesyuarat']) }}"></x-carian>
                                            </div>
                                            <div class="table-responsive">
                                                <table class="table text-center">
                                                    <thead>
                                                        <tr>
                                                            <th>Bil</th>
                                                            <th>Tarikh Minit</th>
                                                            <th>Tujuan</th>
                                                            <th>No Fail / Tajuk Minit</th>
                                                            <th>Tarikh Akhir Maklum Balas</th>
                                                            <th></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @forelse ($meetings as $meeting)
                                                        <tr>
                                                            <td>{{ $loop->iteration + $meetings->firstItem() - 1 }}</td>
                                                            <td>{{ $meeting->tarikh_mesyuarat }}</td>
                                                            <td>{{ $meeting->tujuan }}</td>
                                                            <td>{{ $meeting->no_rujukan_fail }} / {{ $meeting->name }}</td>
                                                            <td>{{ $meeting->tarikh_tutup != null ? $meeting->tarikh_tutup : 'Belum Ditetapkan' }}
                                                            </td>
                                                            <td>
                                                                <a href="{{ route('logaudit.dokumen.meeting.show', [$meeting->id]) }}" class="btn btn-primary btn-sm">Lihat</a>
                                                            </td>
                                                        </tr>
                                                        @empty
                                                        <tr>
                                                            <td colspan="6">Tiada dokumen.</td>
                                                        </tr>
                                                        @endforelse
                                                    </tbody>
                                                </table>
                                                {{ $meetings->withQueryString()->links() }}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection