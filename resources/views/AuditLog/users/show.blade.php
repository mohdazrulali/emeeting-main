@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    Log Audit <strong>{{ request()->route('user')->name }}</strong>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12 d-flex justify-content-between">
                            <ul class="nav nav-pills" role="tablist">
                                <li class="nav-item" role="presentation">
                                    <a class="nav-link @if(request()->route('type') === 'aktiviti') active disabled @endif" href="{{ route('logaudit.pengguna.show', [request()->route('user'), 'aktiviti']) }}" role="tab">Log Aktiviti</a>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <a class="nav-link @if(request()->route('type') === 'akaun') active disabled @endif" href="{{ route('logaudit.pengguna.show', [request()->route('user'), 'akaun']) }}" role="tab">Log Akaun</a>
                                </li>
                            </ul>
                        </div>

                        <div class="col-md-12 mt-2">
                            <div class="tab-content">
                                <div class="tab-pane @if(request()->route('type') === 'aktiviti') active @endif" role="tabpanel">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="table-responsive">
                                                <table class="table">
                                                    <thead>
                                                        <tr>
                                                            <th width="15%" class="text-center">Tarikh/Masa</th>
                                                            <th width="85%">Log</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @forelse($activity_logs as $log)
                                                        <tr>
                                                            <td class="text-center"> {{ date('d/m/Y h:i a', strtotime($log->created_at)) }}</td>
                                                            <td>{!! parseUserActivityLog($log) !!}</td>
                                                        </tr>
                                                        @empty
                                                        <tr>
                                                            <td class="text-center" colspan="2">Tiada log.</td>
                                                        </tr>
                                                        @endforelse
                                                    </tbody>
                                                </table>
                                                {{ $activity_logs->links() }}

                                                {{-- Template
                                                <table class="table">
                                                    <thead>
                                                        <tr>
                                                            <th width="15%" class="text-center">Tarikh/Masa</th>
                                                            <th width="85%">Log</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td class="text-center"> {{ date('d/m/Y h:i a', strtotime(now())) }}</td>
                                                            <td>
                                                                <code>{{ request()->route('user')->name }}</code> telah log masuk ke dalam sistem pada <code>{{ date('d/m/Y h:i a', strtotime(now())) }}</code>.
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-center"> {{ date('d/m/Y h:i a', strtotime(now())) }}</td>
                                                            <td>
                                                                <code>{{ request()->route('user')->name }}</code> telah mendaftar dokumen baru: <code>Dokumen A</code>.
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-center"> {{ date('d/m/Y h:i a', strtotime(now())) }}</td>
                                                            <td>
                                                                <code>{{ request()->route('user')->name }}</code> telah mengemaskini maklumat dokumen: <code>Dokumen A</code>.
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-center"> {{ date('d/m/Y h:i a', strtotime(now())) }}</td>
                                                            <td>
                                                                <code>{{ request()->route('user')->name }}</code> telah menambah <code>1</code> lampiran kepada dokumen: <code>Dokumen A</code>.
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-center"> {{ date('d/m/Y h:i a', strtotime(now())) }}</td>
                                                            <td>
                                                                <code>{{ request()->route('user')->name }}</code> telah menghapus lampiran: <code>Nama Lampiran</code> daripada dokumen: <code>Dokumen A</code>.
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-center"> {{ date('d/m/Y h:i a', strtotime(now())) }}</td>
                                                            <td>
                                                                <code>{{ request()->route('user')->name }}</code> telah menambah/menghantar agihan kepada pengguna: <code>pengguna@email.com</code> bagi dokumen: <code>Dokumen A</code>.
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-center"> {{ date('d/m/Y h:i a', strtotime(now())) }}</td>
                                                            <td>
                                                                <code>{{ request()->route('user')->name }}</code> telah menghantar semula agihan kepada pengguna: <code>pengguna@email.com</code> bagi dokumen: <code>Dokumen A</code>.
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-center"> {{ date('d/m/Y h:i a', strtotime(now())) }}</td>
                                                            <td>
                                                                <code>{{ request()->route('user')->name }}</code> telah set semula agihan bagi dokumen: <code>Dokumen A</code>.
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-center"> {{ date('d/m/Y h:i a', strtotime(now())) }}</td>
                                                            <td>
                                                                <code>{{ request()->route('user')->name }}</code> telah menambah cabutan: <code>Nama Cabutan</code> bagi dokumen: <code>Dokumen A</code>.
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-center"> {{ date('d/m/Y h:i a', strtotime(now())) }}</td>
                                                            <td>
                                                                <code>{{ request()->route('user')->name }}</code> telah menambah/menghantar arahan ulasan kepada <code>pengguna@email.com</code> bagi cabutan: <code>Nama Cabutan</code> dokumen: <code>Dokumen A</code>.
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-center"> {{ date('d/m/Y h:i a', strtotime(now())) }}</td>
                                                            <td>
                                                                <code>{{ request()->route('user')->name }}</code> telah menghantar semula arahan ulasan kepada <code>pengguna@email.com</code> bagi cabutan: <code>Nama Cabutan</code> dokumen: <code>Dokumen A</code>.
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-center"> {{ date('d/m/Y h:i a', strtotime(now())) }}</td>
                                                            <td>
                                                                <code>{{ request()->route('user')->name }}</code> telah menghapus cabutan: <code>Nama Cabutan</code> bagi dokumen: <code>Dokumen A</code>.
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-center"> {{ date('d/m/Y h:i a', strtotime(now())) }}</td>
                                                            <td>
                                                                <code>{{ request()->route('user')->name }}</code> telah memberi ulasan cabutan: <code>Nama Cabutan</code> bagi dokumen: <code>Dokumen B</code>.
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-center"> {{ date('d/m/Y h:i a', strtotime(now())) }}</td>
                                                            <td>
                                                                <code>{{ request()->route('user')->name }}</code> telah mengemaskini ulasan cabutan: <code>Nama Cabutan</code> bagi dokumen: <code>Dokumen B</code>.
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-center"> {{ date('d/m/Y h:i a', strtotime(now())) }}</td>
                                                            <td>
                                                                <code>{{ request()->route('user')->name }}</code> telah memadam ulasan cabutan: <code>Nama Cabutan</code> bagi dokumen: <code>Dokumen B</code>.
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-center"> {{ date('d/m/Y h:i a', strtotime(now())) }}</td>
                                                            <td>
                                                                <code>{{ request()->route('user')->name }}</code> telah log keluar daripada sistem pada <code>{{ date('d/m/Y h:i a', strtotime(now())) }}</code>.
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>--}}
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane @if(request()->route('type') === 'akaun') active @endif" role="tabpanel">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="table-responsive">
                                                <table class="table">
                                                    <thead>
                                                        <tr>
                                                            <th width="15%" class="text-center">Tarikh/Masa</th>
                                                            <th width="85%">Log</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @forelse($account_logs as $log)
                                                        <tr>
                                                            <td class="text-center"> {{ date('d/m/Y h:i a', strtotime($log->created_at)) }}</td>
                                                            <td>{!! parseUserAccountLog($log) !!}</td>
                                                        </tr>
                                                        @empty
                                                        <tr>
                                                            <td class="text-center" colspan="2">Tiada log.</td>
                                                        </tr>
                                                        @endforelse
                                                    </tbody>
                                                </table>
                                                {{ $account_logs->links() }}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection