@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">
                    Log Pengguna Sistem
                </div>
                <div class="card-body">
                    <div class="d-flex">
                        <x-carian reset-route="{{ route('logaudit.pengguna.index') }}"></x-carian>
                    </div>
                    <div class="table-responsive mt-2">
                        <table class="table">
                            <thead>
                                <th>Bil</th>
                                <th>Nama</th>
                                <th>No Kad Pengenalan</th>
                                <th>Email</th>
                                <th>Jabatan/Kementerian</th>
                                <th>Tarikh Daftar</th>
                                <th></th>
                            </thead>
                            <tbody>
                                @forelse($users as $user)
                                <tr>
                                    <td>{{ $loop->iteration + $users->firstItem() - 1 }}</td>
                                    <td>{{ $user->name }}</td>
                                    <td>{{ $user->ic_no }}</td>
                                    <td>{{ $user->email }}</td>
                                    <td>{{ $user->department->name }}</td>
                                    <td>{{ date('d/m/Y h:m a',strtotime($user->created_at)) }}</td>
                                    <td><a href="{{ route('logaudit.pengguna.show', [$user->id]) }}" class="btn btn-primary btn-sm">Lihat</button></a>
                                <tr>
                                    @empty
                                <tr class="text-center">
                                    <td colspan="6">Tiada pengguna.</td>
                                </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                    {{ $users->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection