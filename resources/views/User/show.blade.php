@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <x-success-message-banner></x-success-message-banner>
            <div class="card">
                <div class="card-header">
                    Profile Pengguna
                </div>
                <div class="card-body">
                    <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                        <li class="nav-item" role="presentation">
                            <button class="nav-link active" id="maklumat-peribadi-tab" data-bs-toggle="pill" data-bs-target="#maklumat-peribadi" type="button" role="tab" aria-controls="maklumat-peribadi" aria-selected="true">Maklumat Peribadi</button>
                        </li>
                        <li class="nav-item" role="presentation">
                            <button class="nav-link" id="maklumat-peranan-tab" data-bs-toggle="pill" data-bs-target="#maklumat-peranan" type="button" role="tab" aria-controls="maklumat-peranan" aria-selected="false">Maklumat Peranan</button>
                        </li>
                    </ul>
                    <div class="tab-content" id="pills-tabContent">
                        <div class="tab-pane fade show active" id="maklumat-peribadi" role="tabpanel" aria-labelledby="maklumat-peribadi-tab" tabindex="0">
                            <div class="card">
                                <div class="card-body">
                                    <div class="container-fluid">
                                        <div class="row">
                                            <div class="form-group col-sm-4">
                                                <label>Nama</label>
                                                <p><strong>{{ $user->name }}</strong></p>
                                            </div>
                                            <div class="form-group col-sm-4">
                                                <label>No Kad Pengenalan</label>
                                                <p><strong>{{ $user->ic_no }}</strong></p>
                                            </div>
                                            <div class="form-group col-sm-4">
                                                <label>Alamat Emel</label>
                                                <p><strong>{{ $user->email }}</strong></p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-sm-4">
                                                <label>Jawatan</label>
                                                <p><strong>{{ $user->designation }}</strong></p>
                                            </div>
                                            <div class="form-group col-sm-4">
                                                <label>Jabatan/Kementerian</label>
                                                <p><strong>{{ $user->department->name }}</strong></p>
                                            </div>
                                            <div class="form-group col-sm-4">
                                                <label>No Telefon Pejabat</label>
                                                <p><strong>{{ $user->phone }}</strong></p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-sm-4">
                                                <label>No Telefon Bimbit</label>
                                                <p><strong>{{ $user->mobile }}</strong></p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group d-flex justify-content-end">
                                                <form action="{{ route('users.destroy', $user->id) }}" method="POST">
                                                    @method('DELETE')
                                                    @csrf

                                                </form>
                                                <button type="button" onclick="deleteUser('{{route('users.destroy', $user->id)}}', '{{ $user->name }}')" class="btn btn-danger">Padam Pengguna</button>
                                                <a href="{{ route('users.edit', [$user->id]) }}" class="ms-1 btn btn-primary" data-bs-toggle="tooltip" data-bs-placement="right" data-bs-title="Kemaskini maklumat peribadi">Kemaskini</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="maklumat-peranan" role="tabpanel" aria-labelledby="maklumat-peranan-tab" tabindex="0">
                            <div class="card">
                                <div class="card-body">
                                    <div class="container-fluid">
                                        <div class="d-flex">
                                            <div class="form-group" style="flex: 1">
                                                <label class="mb-2">Peranan</label>
                                                <ul style="margin-bottom: 0;">
                                                    @foreach($user->roles as $role)
                                                    <li>
                                                        <p><strong>{{ $role->name }}</strong></p>
                                                    </li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                            <div>
                                                <a href="{{ route('acls.edit', [$user->id]) }}" class="btn btn-primary" data-bs-toggle="tooltip" data-bs-placement="right" data-bs-title="Kemaskini maklumat peranan dan akses">Kemaskini</a>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <label class="mb-2">Akses Peranan</label>
                                            <div class="table-responsive">
                                                <table class="table table-bordered table-striped">
                                                    <thead>
                                                        <th>Akses</th>
                                                    </thead>
                                                    <tbody>
                                                        @foreach($user->getAllPermissions() as $permission)
                                                        <tr>
                                                            <td>{{ $permission->name }}</td>
                                                        </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<script>
    const deleteUser = async (url, name) => {
        window.confirmDialog(`Anda pasti untuk memadam profile pengguna '${name}'?`, async () => {
            try {
                await window.http(url, "DELETE")
                setTimeout(() => {
                    window.location.replace("{{ route('users.index') }}")
                }, 1000);
            } catch (error) {
                return;
            }
        })
    }
</script>
@endsection
