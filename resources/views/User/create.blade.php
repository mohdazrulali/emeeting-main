@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">
                    Daftar Pengguna
                </div>
                <div class="card-body">
                    <x-error-message-banner></x-error-message-banner>
                    <form action="{{ route('users.store') }}" method="POST">
                        @csrf
                        <div class="row mb-2">
                            <label class="col-3 col-form-label">Nama</label>
                            <div class="col">
                                <input class="form-control" type="text" name="name" value="{{ old('name') }}">
                            </div>
                        </div>
                        <div class="row mb-2">
                            <label class="col-3 col-form-label">No Kad Pengenalan</label>
                            <div class="col">
                                <input class="form-control" type="number" name="ic_no" value="{{ old('ic_no') }}">
                            </div>
                        </div>
                        <div class="row mb-2">
                            <label class="col-3 col-form-label">Alamat Emel</label>
                            <div class="col">
                                <input class="form-control" type="email" name="email" value="{{ old('email') }}">
                            </div>
                        </div>
                        <div class="row mb-2">
                            <label class="col-3 col-form-label">Jawatan</label>
                            <div class="col">
                                <input class="form-control" type="text" name="designation" value="{{ old('designation') }}">
                            </div>
                        </div>
                        <div class="row mb-2">
                            <label class="col-3 col-form-label">Jabatan/Kementerian</label>
                            <div class="col">
                                <select name="department_id" class="selectpicker form-control">
                                    <option value="">Sila Pilih...</option>
                                    @foreach($departments as $department)
                                    <option value="{{ $department->id }}" @if($department->id == old('department_id')) selected @endif>{{ $department->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row mb-2">
                            <label class="col-3 col-form-label">No Telefon Pejabat</label>
                            <div class="col">
                                <input class="form-control" type="number" name="phone" value="{{ old('phone') }}">
                            </div>
                        </div>
                        <div class="row mb-2">
                            <label class="col-3 col-form-label">No Telefon Bimbit</label>
                            <div class="col">
                                <input class="form-control" type="number" name="mobile" value="{{ old('mobile') }}">
                            </div>
                        </div>
                        <div class="row mb-2">
                            <label class="col-3 col-form-label">Kata Laluan</label>
                            <div class="col">
                                <input class="form-control" type="password" name="password" value="{{ old('password') }}">
                            </div>
                        </div>
                        <div class="row mb-2">
                            <label class="col-3 col-form-label">Pengesahan Kata Laluan</label>
                            <div class="col">
                                <input class="form-control" type="password" name="password_confirmation" value="{{ old('password_confirmation') }}">
                            </div>
                        </div>
                        <div class="row mb-2">
                            <label class="col-3 col-form-label">Peranan Dalam Sistem</label>
                            <div class="col">
                                <select name="role" class="form-control">
                                    <option value="">Sila Pilih...</option>
                                    @foreach($roles as $role)
                                    <option value="{{ $role }}" @if($role == old('role')) selected @endif>{{ $role }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <button class="float-end btn btn-primary" type="submit">Simpan</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection