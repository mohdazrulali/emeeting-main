@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <x-success-message-banner></x-success-message-banner>
            <x-error-message-banner></x-error-message-banner>
            <div class="card">
                <div class="card-header">
                    Kemaskini Profil Pengguna
                </div>
                <div class="card-body">
                    <form action="{{ route('users.update', [$user->id]) }}" method="POST">
                        @csrf
                        @method('PUT')
                        <div class="row mb-2">
                            <label class="col-3 col-form-label">Nama</label>
                            <div class="col">
                                <input class="form-control" type="text" name="name" value="{{ $user->name }}">
                            </div>
                        </div>
                        <div class="row mb-2">
                            <label class="col-3 col-form-label">No Kad Pengenalan</label>
                            <div class="col">
                                <input class="form-control" type="number" name="ic_no" value="{{ $user->ic_no }}">
                            </div>
                        </div>
                        <div class="row mb-2">
                            <label class="col-3 col-form-label">Alamat Emel</label>
                            <div class="col">
                                <input class="form-control" type="email" name="email" value="{{ $user->email }}">
                            </div>
                        </div>
                        <div class="row mb-2">
                            <label class="col-3 col-form-label">Jawatan</label>
                            <div class="col">
                                <input class="form-control" type="text" name="designation" value="{{ $user->designation }}">
                            </div>
                        </div>
                        <div class="row mb-2">
                            <label class="col-3 col-form-label">Jabatan/Kementerian</label>
                            <div class="col">
                                <select name="department_id" class="selectpicker form-control">
                                    <option value="">Sila Pilih...</option>
                                    @foreach($departments as $department)
                                    <option value="{{ $department->id }}" @if($department->id == $user->department_id) selected @endif>{{ $department->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row mb-2">
                            <label class="col-3 col-form-label">No Telefon Pejabat</label>
                            <div class="col">
                                <input class="form-control" type="number" name="phone" value="{{ $user->phone }}">
                            </div>
                        </div>
                        <div class="row mb-2">
                            <label class="col-3 col-form-label">No Telefon Bimbit</label>
                            <div class="col">
                                <input class="form-control" type="number" name="mobile" value="{{ $user->mobile }}">
                            </div>
                        </div>
                        <button class="float-end btn btn-primary" type="submit">Simpan</button>
                    </form>
                </div>
            </div>
            <div class="card mt-3">
                <div class="card-header">
                    Tukar Kata Laluan Pengguna
                </div>
                <div class="card-body">
                    <form action="{{ route('users.change-password', [$user->id]) }}" method="POST">
                        @csrf
                        @method('PUT')
                        <div class="row mb-2">
                            <label class="col-3 col-form-label">Kata Laluan Baru</label>
                            <div class="col">
                                <input class="form-control" type="password" name="password" value="">
                            </div>
                        </div>
                        <div class="row mb-2">
                            <label class="col-3 col-form-label">Pengesahan Kata Laluan Baru</label>
                            <div class="col">
                                <input class="form-control" type="password" name="password_confirmation" value="">
                            </div>
                        </div>
                        <button class="float-end btn btn-primary" type="submit">Simpan</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection