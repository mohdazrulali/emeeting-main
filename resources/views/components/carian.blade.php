<form action="" style="flex: 1">
    <div class="input-group">
        <input type="text" name="{{ $searchKeyword }}" value="{{ request($searchKeyword, '') }}" class="form-control" placeholder="Carian...">
        @if(request()->carian)
        <a href="{{ $resetRoute }}" class="btn btn-outline-primary d-flex justify-content-center align-items-center"><i class="fa-solid fa-xmark"></i></a>
        @endif
        <button class="btn btn-primary" type="submit">Carian</button>
    </div>
</form>