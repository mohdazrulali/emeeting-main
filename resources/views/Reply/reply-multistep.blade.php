@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    Ulasan Dokuman {{ parseDocumentType($item->jenis) }}
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12 d-flex justify-content-between">
                            <ul class="nav nav-pills" role="tablist">
                                <li class="nav-item" role="presentation">
                                    <a class="nav-link @if(request()->query('step') === '1') active disabled @endif" href="{{ generateFeedbackUrl('reply.show', $agihan, ['step' => 1]) }}" role="tab">1. Maklumat {{ parseDocumentType($item->jenis) }}</a>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <a class="nav-link @if(request()->query('step') === '2') active disabled @endif" href="{{ generateFeedbackUrl('reply.show', $agihan, ['step' => 2]) }}" role="tab">2. @if($cabutan) Maklumat Cabutan Dan @endif Maklum Balas Anda
                                        @if(!$agihan->user_reply)
                                        <span class="badge bg-warning text-white">Belum Selesai</span>
                                        @elseif($agihan->user_reply && !$agihan->user_reply->approved_by)
                                        <span class="badge bg-secondary text-white">Belum Diluluskan</span>
                                        @else
                                        <span class="badge bg-success text-white">Selesai</span>
                                        @endif
                                    </a>
                                </li>
                            </ul>
                            <div>
                                <a type="button" class="btn btn-outline-primary" href="{{ route('reply.index') }}">
                                    <i class="fa-solid fa-angle-left" style="margin-right: 8px;"></i>Kembali
                                </a>
                            </div>
                        </div>

                        <div class="col-md-12 mt-2">
                            @if (session('status'))
                            <div class="alert alert-{{ session('status.type') }}" role="alert">
                                {{ session('status.message') }}
                            </div>
                            @endif


                            @if ($errors->any())
                            <div class="alert alert-danger mb-0">
                                <ul style="margin-bottom: 0;">
                                    @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                            @endif
                        </div>

                        <div class="col-md-12 mt-2">
                            <div class="tab-content">
                                @if(get_class($item) === 'App\Models\Meeting')
                                <div class="tab-pane @if(request()->query('step') === '1') active @endif" role="tabpanel">
                                    <div class="card mt-2">
                                        <div class="card-body">
                                            <div class="form-group row mb-2">
                                                <div class="col-3">Tujuan</div>
                                                <div class="col">
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" disabled type="radio" name="tujuan" value="Makluman" @if($item->tujuan === 'Makluman' ) checked @endif>
                                                        <label class="form-check-label" for="Makluman">Makluman</label>
                                                    </div>
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" disabled type="radio" name="tujuan" value="Tindakan Segera" @if($item->tujuan === 'Tindakan Segera' ) checked @endif>
                                                        <label class="form-check-label" for="Tindakan Segera">Tindakan Segera</label>
                                                    </div>

                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" disabled type="radio" name="tujuan" value="Tindakan Serta Merta" @if($item->tujuan === 'Tindakan Serta Merta' ) checked @endif>
                                                        <label class="form-check-label" for="Tindakan Serta Merta">Tindakan Serta Merta</label>
                                                    </div>

                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" disabled type="radio" name="tujuan" value="Tindakan Susulan" @if($item->tujuan === 'Tindakan Susulan' ) checked @endif>
                                                        <label class="form-check-label" for="Tindakan Susulan">Tindakan Susulan</label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group d-flex mb-2">
                                                <div class="col-3">Tarikh Mesyuarat</div>
                                                <div class="col">
                                                    <input type="date" disabled name="tarikh_mesyuarat" placeholder="Tarikh Mesyuarat" class="form-control @error('tarikh_mesyuarat') is-invalid @enderror" value="{{ date('Y-m-d', strtotime($item->tarikh_mesyuarat)) }}">
                                                    @error('tarikh_mesyuarat')
                                                    <div class="invalid-feedback">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>

                                            <div class="card">
                                                <div class="card-header">Maklumat Mesyuarat</div>
                                                <div class="card-body">
                                                    <div class="form-group row mb-2">
                                                        <div class="col-3">Status Minit</div>
                                                        <div class="col">
                                                            <div class="form-check form-check-inline">
                                                                <input class="form-check-input" disabled type="radio" name="is_approved" value="1" @if($item->is_approved == '1' ) checked @endif>
                                                                <label class="form-check-label" for="Telah Diluluskan">Telah Disahkan</label>
                                                            </div>

                                                            <div class="form-check form-check-inline">
                                                                <input type="radio" name="is_approved" disabled value="0" @if($item->is_approved == '0' ) checked @endif>
                                                                <label for="Belum Disahkan">Belum Disahkan</label>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row mb-2">
                                                        <div class="col-3">Tarikh Status</div>
                                                        <div class="col">
                                                            <input type="date" disabled name="approved_date" placeholder="Tarikh Status" class="form-control @error('approved_date') is-invalid @enderror" value="{{ date('Y-m-d', strtotime($item->approved_date)) }}">
                                                            @error('approved_date')
                                                            <div class="invalid-feedback">{{ $message }}</div>
                                                            @enderror
                                                        </div>
                                                    </div>

                                                    <div class="form-group row mb-2">
                                                        <div class="col-3">No Rujukan Fail</div>
                                                        <div class="col">
                                                            <input type="text" disabled name="no_rujukan_fail" placeholder="No Rujukan Fail" class="form-control @error('no_rujukan_fail') is-invalid @enderror" value="{{ $item->no_rujukan_fail }}">
                                                            @error('no_rujukan_fail')
                                                            <div class="invalid-feedback">{{ $message }}</div>
                                                            @enderror
                                                        </div>
                                                    </div>

                                                    <div class="form-group row mb-2">
                                                        <div class="col-3">Tajuk /Perkara</div>
                                                        <div class="col">
                                                            <input type="text" disabled name="name" placeholder="No Rujukan Fail" class="form-control @error('name') is-invalid @enderror" value="{{ $item->name }}">
                                                            @error('name')
                                                            <div class="invalid-feedback">{{ $message }}</div>
                                                            @enderror
                                                        </div>
                                                    </div>

                                                    <div class="form-group row mb-2">
                                                        <div class="col-3">Minit Mesyuarat</div>
                                                        <div class="col">
                                                            <textarea name="ringkasan" disabled class="form-control  @error('ringkasan') is-invalid @enderror">{{ $item->ringkasan }}</textarea>
                                                            @error('ringkasan')
                                                            <div class="invalid-feedback">{{ $message }}</div>
                                                            @enderror

                                                        </div>
                                                    </div>

                                                    <div class="form-group row mb-2">
                                                        <div class="col-3">Tarikh Akhir Maklum Balas</div>
                                                        <div class="col">
                                                            <input type="datetime-local" disabled name="tarikh_tutup" value="{{ $item->tarikh_tutup }}" class="form-control  @error('tarikh_tutup') is-invalid @enderror" value="{{ date('Y-m-d', strtotime($item->tarikh_tutup)) }}">
                                                            @error('tarikh_tutup')
                                                            <div class="invalid-feedback">{{ $message }}</div>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endif

                                @if(get_class($item) === 'App\Models\Document')
                                <div class="tab-pane @if(request()->query('step') === '1') active @endif" role="tabpanel">
                                    <div class="card">
                                        <div class="card-header">Kegunaan Pejabat</div>
                                        <div class="card-body row">
                                            <div class="col">Tarikh & Masa: {{ date('d/m/Y h:i a', strtotime($item->created_at)) }}
                                            </div>
                                            <div class="col">Disediakan Oleh: {{ $item->createdBy->name }}</div>
                                        </div>
                                    </div>
                                    <div class="card mt-2">
                                        <div class="card-body">
                                            <div class="form-group row mb-2">
                                                <div class="col-3">Nombor File Kementerian</div>
                                                <div class="col">
                                                    <input disabled type="text" name="no_fail_kementerian" placeholder="No File Kementerian" class="form-control @error('no_fail_kementerian') is-invalid @enderror" value="{{ old('no_fail_kementerian') ?? $item->no_fail_kementerian }}">

                                                    @error('no_fail_kementerian')
                                                    <div class="invalid-feedback">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>

                                            <div class="form-group row mb-2">
                                                <div class="col-3">Salinan Bilangan</div>
                                                <div class="col">
                                                    <input disabled type="text" name="salinan_bilangan" placeholder="Salinan Bilangan" value="{{ old('salinan_bilangan') ?? $item->salinan_bilangan }}" class="form-control @error('salinan_bilangan') is-invalid @enderror">
                                                    @error('salinan_bilangan')
                                                    <div class="invalid-feedback">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>

                                            <div class="form-group row mb-2">
                                                <div class="col-3">Tajuk Dokumen</div>
                                                <div class="col">
                                                    <input disabled type="text" name="name" placeholder="Tajuk Dokumen" value="{{ old('name') ?? $item->name }}" class="form-control @error('name') is-invalid @enderror">
                                                    @error('name')
                                                    <div class="invalid-feedback">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>

                                            <div class="form-group row mb-2">
                                                <div class="col-3">Sub Tajuk Dokumen</div>
                                                <div class="col">
                                                    <input disabled type="text" name="sub_name" placeholder="Sub Tajuk Dokumen" value="{{ old('sub_name') ?? $item->sub_name }}" class="form-control @error('sub_name') is-invalid @enderror">
                                                    @error('sub_name')
                                                    <div class="invalid-feedback">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                            @if($item->jenis !== 'memo-kpkt')
                                            <div class="form-group row mb-2">
                                                <div class="col-3">Kementerian/Jabatan</div>
                                                <div class="col">
                                                    <select disabled name="department_id" class="form-select">
                                                        @foreach ($departments as $department)
                                                        <option value="{{ $department->id }}" {{ $department->id == $item->department_id ? 'selected' : '' }}>
                                                            {{ $department->name }}
                                                            ({{ $department->shortcode }})
                                                        </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            @endif

                                            <div class="form-group row mb-2">
                                                <div class="col-3">Tarikh Dokumen</div>
                                                <div class="col">
                                                    <input type="date" disabled name="tarikh_dokumen" placeholder="Tarikh Dokumen" value="{{ old('tarikh_dokumen') ?? $item->tarikh_dokumen }}" class="form-control @error('tarikh_dokumen') is-invalid @enderror">
                                                    @error('tarikh_dokumen')
                                                    <div class="invalid-feedback">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endif

                                <div class="tab-pane @if(request()->query('step') === '2') active @endif" role="tabpanel">
                                    <div class="card">
                                        <div class="card-body">
                                            @if($cabutan)
                                            <div class="form-group">
                                                <label>Catatan</label>
                                                <textarea disabled rows="5" class="form-control" placeholder="Catatan...">{{ $cabutan->content }}</textarea>
                                            </div>
                                            @endif
                                            <form action="{{ route('reply.store', request()->route()->parameters) }}" class="@if($cabutan) mt-3 @endif" method="POST" enctype="multipart/form-data">
                                                @csrf
                                                <div class="form-group">
                                                    <label>Fail Dokumen Ulasan Jabatan</label>
                                                    <input type="file" name="fail_dokumen_ulasan_jabatan" accept=".pdf" class="form-control @error('fail_dokumen_ulasan_jabatan') is-invalid @enderror">
                                                    @error('fail_dokumen_ulasan_jabatan')
                                                    <div class="invalid-feedback">
                                                        {{ $message }}
                                                    </div>
                                                    @enderror
                                                </div>
                                                <div class="form-group mt-3">
                                                    <label>Fail Dokumen Sokongan</label>
                                                    <input type="file" name="fail_dokumen_sokongan" accept=".pdf" class="form-control @error('fail_dokumen_sokongan') is-invalid @enderror">
                                                    @error('fail_dokumen_sokongan')
                                                    <div class="invalid-feedback">
                                                        {{ $message }}
                                                    </div>
                                                    @enderror
                                                </div>
                                                @if($agihan->user_reply && ($agihan->user_reply->lampiranUlasanJabatan || $agihan->user_reply->lampiranSokongan))
                                                <div class="form-group table-responsive mt-3">
                                                    <table class="table">
                                                        <thead>
                                                            <th>Fail</th>
                                                            <th></th>
                                                        </thead>
                                                        <tbody>
                                                            @if($agihan->user_reply->lampiranUlasanJabatan)
                                                            <tr>
                                                                <td>{{ $agihan->user_reply->lampiranUlasanJabatan->name }}</td>
                                                                <td>
                                                                    <button type="button" onclick="deleteLampiran(`{{ route('lampirans.destroy', $agihan->user_reply->lampiranUlasanJabatan->id) }}`, `{{ $agihan->user_reply->lampiranUlasanJabatan->name }}`)" type="button" class="btn btn-danger btn-sm">Padam</button>
                                                                    <a class="btn btn-primary btn-sm" href="{{ route('view.docs', $agihan->user_reply->lampiranSokongan->id) }}" target="_blank">Lihat</a>
                                                                </td>
                                                            </tr>
                                                            @endif
                                                            @if($agihan->user_reply->lampiranSokongan)
                                                            <tr>
                                                                <td>{{ $agihan->user_reply->lampiranSokongan->name }}</td>
                                                                <td>
                                                                    <button type="button" onclick="deleteLampiran(`{{ route('lampirans.destroy', $agihan->user_reply->lampiranSokongan->id) }}`, `{{ $agihan->user_reply->lampiranSokongan->name }}`)" class="btn btn-danger btn-sm">Padam</button>
                                                                    <a class="btn btn-primary btn-sm" href="{{ route('view.docs', $agihan->user_reply->lampiranSokongan->id) }}" target="_blank">Lihat</a>
                                                                </td>
                                                            </tr>
                                                            @endif
                                                        </tbody>
                                                    </table>
                                                </div>
                                                @endif
                                                <div class="form-group mt-3">
                                                    <label>Maklum Balas Anda</label>
                                                    <textarea @if($agihan->user_reply && $agihan->user_reply->approved_by) disabled @endif rows="5" name="content" class="form-control @error('content') is-invalid @enderror" placeholder="Maklum balas anda...">{{ $agihan->user_reply->content ?? '' }}</textarea>
                                                    @error('content')
                                                    <div class="invalid-feedback">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                                @if($agihan->user_reply && !$agihan->user_reply->approved_by)
                                                <div class="form-group mt-3">
                                                    <label>Disediakan Oleh</label>
                                                    <p><strong>{{ strtoupper($agihan->user_reply->user->name) }}</strong></p>
                                                </div>
                                                @endif

                                                @if($agihan->user_reply && $agihan->user_reply->approved_by)
                                                <div class="form-group mt-3">
                                                    <label>Diluluskan Oleh</label>
                                                    <p><strong>{{ strtoupper($agihan->user_reply->approved_by->name) }}</strong></p>
                                                </div>
                                                @endif

                                                @if(!$agihan->user_reply || !$agihan->user_reply->approved_by)
                                                <div class="form-group mt-2 justify-content-end d-flex">
                                                    <button class="btn btn-primary" type="submit">@if(!$agihan->user_reply) Simpan @else Kemaskini @endif</button>
                                                </div>
                                                @endif
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    const deleteLampiran = async (url, name) => {
        window.confirmDialog(`Anda pasti untuk memadam lampiran '${name}'?`, async () => {
            try {
                await window.http(url, "DELETE")
                window.location.reload()
            } catch (error) {
                return;
            }
        })
    }
</script>
@endsection