@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Maklum Balas</div>

                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table" style="width: 100%; table-layout: fixed">
                            <thead>
                                <th width="10%">Bil </th>
                                <th>Perihal</th>
                                <th width="20%" class="text-center">Jenis</th>
                                <th class="text-center">Tarikh Terima</th>
                                <th class="text-center">Status</th>
                                <th></th>
                            </thead>
                            <tbody>
                                @forelse($replies as $action)
                                <tr>
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis">
                                        @if($action->agihanable->name)
                                        {{ $action->agihanable->name }}
                                        @elseif($action->agihanable->content)
                                        {{ $action->agihanable->content }}
                                        @else
                                        [Tiada Perihal]
                                        @endif
                                    </td>
                                    <td class="text-center">
                                        @if(get_class($action->agihanable) === 'App\Models\Cabutan')
                                        Cabutan Mesyuarat
                                        @elseif(get_class($action->agihanable) === 'App\Models\Meeting')
                                        Agihan Mesyuarat
                                        @elseif(get_class($action->agihanable) === 'App\Models\Document')
                                        Agihan Dokumen
                                        @endif
                                    </td>
                                    <td class="text-center">{{ date('d/m/Y', strtotime($action->created_at)) }}</td>
                                    <td class="text-center">
                                        @if(!$action->user_reply)
                                        <span class="badge bg-warning text-white">Belum Selesai</span>
                                        @elseif($action->user_reply && !$action->user_reply->approved_by)
                                        <span class="badge bg-secondary text-white">Belum Diluluskan</span>
                                        @else
                                        <span class="badge bg-success text-white">Selesai</span>
                                        @endif
                                    </td>
                                    <td class="text-center">
                                        <a class="btn btn-primary btn-sm" href="{{ generateFeedbackUrl('reply.show', $action) }}" type="button">Kemaskini</a>
                                    </td>
                                </tr>
                                </tr>
                                @empty
                                <tr class="text-center">
                                    <td colspan="5">Tiada tindakan.</td>
                                </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
