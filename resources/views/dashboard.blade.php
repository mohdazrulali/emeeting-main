@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header d-flex justify-content-between align-items-center">
                    <span>Dashboard</span>
                    <div class="dropdown">
                        <a class="btn btn-secondary btn-sm dropdown-toggle" role="button" id="dropdownMenuLink" data-bs-toggle="dropdown" aria-expanded="false">
                            {{ request('tapisan_tahun', 'Pilih tahun...') }}
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                            <li><a class="dropdown-item @if(request()->tapisan_tahun === '2018') disabled @endif" href="{{ route('dashboard', ['tapisan_bulan' => request('tapisan_bulan', ''), 'tapisan_tahun' => '2018']) }}">2018</a></li>
                            <li><a class="dropdown-item @if(request()->tapisan_tahun === '2019') disabled @endif" href="{{ route('dashboard', ['tapisan_bulan' => request('tapisan_bulan', ''), 'tapisan_tahun' => '2019']) }}">2019</a></li>
                            <li><a class="dropdown-item @if(request()->tapisan_tahun === '2020') disabled @endif" href="{{ route('dashboard', ['tapisan_bulan' => request('tapisan_bulan', ''), 'tapisan_tahun' => '2020']) }}">2020</a></li>
                            <li><a class="dropdown-item @if(request()->tapisan_tahun === '2021') disabled @endif" href="{{ route('dashboard', ['tapisan_bulan' => request('tapisan_bulan', ''), 'tapisan_tahun' => '2021']) }}">2021</a></li>
                            <li><a class="dropdown-item @if(request()->tapisan_tahun === '2022') disabled @endif" href="{{ route('dashboard', ['tapisan_bulan' => request('tapisan_bulan', ''), 'tapisan_tahun' => '2022']) }}">2022</a></li>
                            <li><a class="dropdown-item @if(request()->tapisan_tahun === '2023') disabled @endif" href="{{ route('dashboard', ['tapisan_bulan' => request('tapisan_bulan', ''), 'tapisan_tahun' => '2023']) }}">2023</a></li>
                        </ul>
                    </div>
                </div>

                <div class="card-body p-0">
                    <div class="container-fluid p-2">
                        <div class="row g-2 p-0">
                            <div class="col-sm-12">
                                <div class="card">
                                    <div class="card-header d-flex justify-content-center align-items-center">
                                        <span>Jumlah Dokumen Mengikut Bulan</span>
                                        <span class="badge rounded-pill ms-2" style="min-width: 30px; background-color: rgb(75, 192, 192)">{{ $document_count }}</span>
                                    </div>
                                    <div class="card-body">
                                        <div class="container-fluid p-0">
                                            <canvas id="dokumen_chart"></canvas>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="card">
                                <div class="card-header d-flex justify-content-center align-items-center">
                                        <span>Jumlah Mesyuarat Mengikut Bulan</span>
                                        <span class="badge bg-primary rounded-pill ms-2" style="min-width: 30px">{{ $meeting_count }}</span>
                                    </div>
                                    <div class="card-body">
                                        <div class="container-fluid p-0">
                                            <canvas id="mesyuarat_chart"></canvas>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="module">
    const documentData = {{ Js::from($document_data) }}
    const meetingData = {{ Js::from($meeting_data) }}
    const labels = {{ Js::from($labels) }}

    new Chart(
        document.getElementById('dokumen_chart'), {
            type: 'line',
            data: {
                labels,
                datasets: [{
                    data: documentData,
                    label: "Dokumen",
                    fill: false,
                    tension: 0.1,
                    borderColor: "rgb(75, 192, 192)",
                    backgroundColor: "rgba(75, 192, 192, 0.5)"
                }]
            },
        }
    );

    new Chart(
        document.getElementById('mesyuarat_chart'), {
            type: 'line',
            data: {
                labels,
                datasets: [{
                    data: meetingData,
                    label: "Mesyuarat",
                    fill: false,
                    tension: 0.1
                }]
            },
        }
    );
</script>
@endsection
