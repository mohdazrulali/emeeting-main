<?php

namespace Database\Factories;

use App\Models\Ministry;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Document>
 */
class DocumentFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'name' => fake()->name(),
            'sub_name' => fake()->name(),
            'no_fail_kementerian' => 'KPKT-' . fake()->buildingNumber(),
            'tarikh_setuju_ksu' => fake()->date(),
            'tarikh_lulus_ybm' => fake()->date(),
            'tarikh_tutup_ulasan' => fake()->dateTime(),
            'tarikh_keputusan' => fake()->date(),
            'tarikh_dokumen' => fake()->date(),
            'salinan_bilangan' => fake()->lastName(),
            'jenis' => fake()->randomElement(['memo-kpkt', 'kementerian-lain', 'nota']),
            'created_by' => User::factory(),
            'ministry_id' => Ministry::factory(),
        ];
    }
}
