<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Meeting>
 */
class MeetingFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'name' => fake()->name(),
            'tarikh_mesyuarat' => fake()->date(),
            'is_approved' => fake()->randomElement([0, 1]),
            'jenis' => fake()->randomElement(['mesyuarat-jemaah-menteri', 'mesyuarat-utama']),
            'tujuan' => fake()->randomElement(['Makluman', 'Tindakan Segera', 'Tindakan Serta Merta', 'Tindakan Susulan']),
            'approved_date' => fake()->date(),
            'ringkasan' => fake()->text(),
            'no_rujukan_fail' => fake()->buildingNumber(),
            'tarikh_tutup' => fake()->dateTime(),
        ];
    }
}
