<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Department>
 */
class DepartmentFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            // 'name' => 'Jabatan ' . fake()->city(),
            // 'code' => 'J' . fake()->citySuffix(),
            // 'shortcode' => fake()->countryCode(),
            // 'section' => fake()->name()
        ];
    }
}
