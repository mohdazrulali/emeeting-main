<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class KetuaJabatanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::factory()->asHOD()->create([
            'name'      => 'hod_sample_1',
            'email'     => 'hod_sample_1@email.com',
            'password'  => Hash::make('password')
        ]);
    }
}
