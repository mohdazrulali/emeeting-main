<?php

namespace Database\Seeders;

use App\Models\Role as ModelsRole;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RolePermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // add permissions here
        $adminRolePermissions = [
            'view-dashboard',
            'view-log-audit',
            'view-departments',
            'view-acls',
            'update-acls',
            'view-users',
            'add-pengguna',
            'view-activiti-log-pengguna',
            'edit-profil-pengguna',
            'delete-profile-pengguna',
            'view-documents',
            'view-documents-memo-kpkt',
            'view-documents-kementerian-lain',
            'view-documents-nota',
            'view-documents-trail',
            'view-maklum-balas',
            'view-meetings-mesyuarat-jemaah-menteri',
            'view-meetings-mesyuarat-utama',
        ];

        $systemAdminRolePermissions = [
            'view-dashboard',
            'view-log-audit',
            'view-departments',
            'view-acls',
            'update-acls',
            'view-users',
            'add-pengguna',
            'view-activiti-log-pengguna',
            'edit-profil-pengguna',
            'delete-profile-pengguna',
            // 'view-documents',
            // 'view-documents-memo-kpkt',
            // 'view-documents-kementerian-lain',
            // 'view-documents-nota',
            'view-documents-trail',
        ];

        $hodRolePermissions = [
            'view-dashboard',
            'view-maklum-balas',
            'view-meetings-mesyuarat-jemaah-menteri',
            'view-meetings-mesyuarat-utama',
            'view-documents-nota',
        ];

        foreach (array_merge($adminRolePermissions, $systemAdminRolePermissions, $hodRolePermissions) as $permission) {
            Permission::updateOrCreate(['name' => $permission]);
        }

        //BSA
        $adminRole = Role::updateOrCreate([
            'name' => ModelsRole::PERANAN_PENTADBIR,
        ]);

        //BTM
        $systemAdminRole = Role::updateOrCreate([
            'name' => ModelsRole::PERANAN_PENTADBIR_SISTEM,
        ]);

        $hodRole = Role::updateOrCreate([
            'name' => ModelsRole::PERANAN_KETUA_JABATAN,
        ]);

        $superuserRole = Role::updateOrCreate([
            'name' => ModelsRole::PERANAN_SUPERUSER,
        ]);

        // assign permissions to roles
        $adminRole->syncPermissions($adminRolePermissions);
        $systemAdminRole->syncPermissions($systemAdminRolePermissions);
        $hodRole->syncPermissions($hodRolePermissions);
        $superuserRole->syncPermissions(array_merge($adminRolePermissions, $systemAdminRolePermissions, $hodRolePermissions));
    }
}
