<?php

namespace Database\Seeders;

use App\Models\Ministry;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class MinistrySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Ministry::factory(20)->create();

        Ministry::insert([
            [
                'name' => 'Kementerian Perumahan dan Kerajaan Tempatan',
                'shortcode' => 'KPKT',
            ],
            [
                'name' => 'Kementerian Komunikasi dan Multimedia',
                'shortcode' => 'K-KOM'
            ]
        ]);
    }
}
