<?php

namespace Database\Seeders;

use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        // sample superuser
        User::factory()->create([
            'name' => 'Superuser',
            'email' => 'superuser@emeeting.com',
            'password'  => 'password'
        ])->assignRole(Role::PERANAN_SUPERUSER);

        // sample btm
        User::factory()->create([
            'name' => 'Pentadbir Sistem',
            'email' => 'sysadmin@emeeting.com',
            'password'  => 'password'
        ])->assignRole(Role::PERANAN_PENTADBIR_SISTEM);

        // sample urusetia BSA
        User::factory()->create([
            'name' => 'Urusetia',
            'email' => 'urusetia@emeeting.com',
            'password'  => 'password'
        ])->assignRole(Role::PERANAN_PENTADBIR);

        User::factory()->create([
            'name' => 'PERANAN_PENTADBIR',
            'email' => 'urusetia1@emeeting.com',
            'password'  => 'password'
        ])->assignRole(Role::PERANAN_PENTADBIR);

        // sample ketua jabatan
        User::factory()->create([
            'name' => 'Ketua Jabatan',
            'email' => 'ketua@emeeting.com',
            'password'  => 'password'
        ])->assignRole(Role::PERANAN_KETUA_JABATAN);

        // sample superuser
        User::factory()->create([
            'name' => 'Azrul',
            'email' => 'azrul@emeeting.com',
            'password'  => 'password'
        ])->assignRole(Role::PERANAN_PENTADBIR_SISTEM);

        // User::factory(10)->create([
        //     'password'  => 'Abcd1234'
        // ]);

    }
}
