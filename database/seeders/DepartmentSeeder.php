<?php

namespace Database\Seeders;

use App\Models\Department;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DepartmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Department::updateOrCreate([
            'name'      => 'KEMENTERIAN PEMBANGUNAN KERAJAAN TEMPATAN',
            'code'      => 'KPKT',
            'shortcode' => 'KPKT',
            'section'   => 'KEMENTERIAN PEMBANGUNAN KERAJAAN TEMPATAN',
        ]);
        Department::updateOrCreate([
            'name'      => 'PEJABAT KETUA SETIAUSAHA',
            'code'      => 'PKSU',
            'shortcode' => 'PKSU',
            'section'   => 'PEJABAT KETUA SETIAUSAHA',
        ]);
        Department::updateOrCreate([
            'name'      => 'TIMBALAN KETUA SETIAUSAHA (PENGURUSAN)',
            'code'      => 'TKSU(P)',
            'shortcode' => 'TKSU(P)',
            'section'   => 'TIMBALAN KETUA SETIAUSAHA (PENGURUSAN)',
        ]);
        Department::updateOrCreate([
            'name'      => 'PEJABAT TIMBALAN KETUA SETIAUSAHA (DASAR DAN PEMBANGUNAN)',
            'code'      => 'PTKSU(D)',
            'shortcode' => 'PTKSU(D)',
            'section'   => 'PEJABAT TIMBALAN KETUA SETIAUSAHA (DASAR DAN PEMBANGUNAN)',
        ]);
        Department::updateOrCreate([
            'name'      => 'PEJABAT TIMBALAN KETUA SETIAUSAHA (KESEJAHTERAAN BANDAR)',
            'code'      => 'PTKSU(K)',
            'shortcode' => 'PTKSU(K)',
            'section'   => 'PEJABAT TIMBALAN KETUA SETIAUSAHA (KESEJAHTERAAN BANDAR)',
        ]);
        Department::updateOrCreate([
            'name'      => 'BAHAGIAN UNDANG-UNDANG',
            'code'      => 'BUU',
            'shortcode' => 'BUU',
            'section'   => 'BAHAGIAN UNDANG-UNDANG',
        ]);
        Department::updateOrCreate([
            'name'      => 'UNIT KOMUNIKASI KORPORAT',
            'code'      => 'UKK',
            'shortcode' => 'UKK',
            'section'   => 'UNIT KOMUNIKASI KORPORAT',
        ]);
        Department::updateOrCreate([
            'name'      => 'UNIT AUDIT DALAM',
            'code'      => 'UAD',
            'shortcode' => 'UAD',
            'section'   => 'UNIT AUDIT DALAM',
        ]);
        Department::updateOrCreate([
            'name'      => 'UNIT INTEGRITI',
            'code'      => 'UI',
            'shortcode' => 'UI',
            'section'   => 'UNIT INTEGRITI',
        ]);
        Department::updateOrCreate([
            'name'      => 'BAHAGIAN KEWANGAN DAN PEROLEHAN',
            'code'      => 'BKEW',
            'shortcode' => 'BKEW',
            'section'   => 'BAHAGIAN KEWANGAN DAN PEROLEHAN',
        ]);
        Department::updateOrCreate([
            'name'      => 'BAHAGIAN SUMBER MANUSIA',
            'code'      => 'BSM',
            'shortcode' => 'BSM',
            'section'   => 'BAHAGIAN SUMBER MANUSIA',
        ]);
        Department::updateOrCreate([
            'name'      => 'BAHAGIAN TEKNOLOGI MAKLUMAT',
            'code'      => 'BTM',
            'shortcode' => 'BTM',
            'section'   => 'BAHAGIAN TEKNOLOGI MAKLUMAT',
        ]);
        Department::updateOrCreate([
            'name'      => 'BAHAGIAN KHIDMAT PENGURUSAN',
            'code'      => 'BKP',
            'shortcode' => 'BKP',
            'section'   => 'BAHAGIAN KHIDMAT PENGURUSAN',
        ]);
        Department::updateOrCreate([
            'name'      => 'BAHAGIAN AKAUN',
            'code'      => 'BA',
            'shortcode' => 'BA',
            'section'   => 'BAHAGIAN AKAUN',
        ]);
        Department::updateOrCreate([
            'name'      => 'BAHAGIAN PEMBANGUNAN DAN PELAKSANAAN PROJEK',
            'code'      => 'BPEMB',
            'shortcode' => 'BPEMB',
            'section'   => 'BAHAGIAN PEMBANGUNAN DAN PELAKSANAAN PROJEK',
        ]);
        Department::updateOrCreate([
            'name'      => 'BAHAGIAN PERANCANGAN STRATEGIK DAN HUBUNGAN ANTARABANGSA',
            'code'      => 'BSA',
            'shortcode' => 'BSA',
            'section'   => 'BAHAGIAN PERANCANGAN STRATEGIK DAN HUBUNGAN ANTARABANGSA',
        ]);
        Department::updateOrCreate([
            'name'      => 'BAHAGIAN KESEJAHTERAAN KOMUNITI',
            'code'      => 'BSK',
            'shortcode' => 'BSK',
            'section'   => 'BAHAGIAN KESEJAHTERAAN KOMUNITI',
        ]);
        Department::updateOrCreate([
            'name'      => 'BAHAGIAN PERLAKSANAAN DAN PERKHIDMATAN KEJURUTERAAN',
            'code'      => 'BPJ',
            'shortcode' => 'BPJ',
            'section'   => 'BAHAGIAN PERLAKSANAAN DAN PERKHIDMATAN KEJURUTERAAN',
        ]);
        Department::updateOrCreate([
            'name'      => 'BAHAGIAN KAWALAN KREDIT KOMUNITI',
            'code'      => 'BKK',
            'shortcode' => 'BKK',
            'section'   => 'BAHAGIAN KAWALAN KREDIT KOMUNITI',
        ]);
        Department::updateOrCreate([
            'name'      => 'PLAN MALAYSIA',
            'code'      => 'PLANM',
            'shortcode' => 'PLANM',
            'section'   => 'PLAN MALAYSIA',
        ]);
        Department::updateOrCreate([
            'name'      => 'JABATAN BOMBA DAN PENYELAMAT MALAYSIA',
            'code'      => 'JBPM',
            'shortcode' => 'JBPM',
            'section'   => 'JABATAN BOMBA DAN PENYELAMAT MALAYSIA',
        ]);
        Department::updateOrCreate([
            'name'      => 'JABATAN PERUMAHAN NEGARA',
            'code'      => 'JPN',
            'shortcode' => 'JPN',
            'section'   => 'JABATAN PERUMAHAN NEGARA',
        ]);
        Department::updateOrCreate([
            'name'      => 'JABATAN KERAJAAN TEMPATAN',
            'code'      => 'JKT',
            'shortcode' => 'JKT',
            'section'   => 'JABATAN KERAJAAN TEMPATAN',
        ]);
        Department::updateOrCreate([
            'name'      => 'JABATAN LANDSKAP NEGARA',
            'code'      => 'JLN',
            'shortcode' => 'JLN',
            'section'   => 'JABATAN LANDSKAP NEGARA',
        ]);
        Department::updateOrCreate([
            'name'      => 'JABATAN PENGURUSAN SISA PEPEJAL NEGARA',
            'code'      => 'JPSPN',
            'shortcode' => 'JPSPN',
            'section'   => 'JABATAN PENGURUSAN SISA PEPEJAL NEGARA',
        ]);
        Department::updateOrCreate([
            'name'      => 'TRIBUNAL PERUMAHAN DAN PENGURUSAN STRATA',
            'code'      => 'TPPS',
            'shortcode' => 'TPPS',
            'section'   => 'TRIBUNAL PERUMAHAN DAN PENGURUSAN STRATA',
        ]);
        Department::updateOrCreate([
            'name'      => 'INSTITUT LATIHAN PERUMAHAN DAN KERAJAAN TEMPATAN',
            'code'      => 'I-KPKT',
            'shortcode' => 'I-KPKT',
            'section'   => 'INSTITUT LATIHAN PERUMAHAN DAN KERAJAAN TEMPATAN',
        ]);
        Department::updateOrCreate([
            'name'      => 'PERBADANAN PENGURUSAN SISA PEPEJAL DAN PEMBERSIHAN AWAM (SWCORP)',
            'code'      => 'SWCORP',
            'shortcode' => 'SWCORP',
            'section'   => 'PERBADANAN PENGURUSAN SISA PEPEJAL DAN PEMBERSIHAN AWAM (SWCORP)',
        ]);

    }
}
