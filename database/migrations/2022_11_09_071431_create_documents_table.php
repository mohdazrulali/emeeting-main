<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('documents', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('sub_name');
            $table->string('no_fail_kementerian');
            $table->date('tarikh_setuju_ksu')->nullable();
            $table->date('tarikh_lulus_ybm')->nullable();
            $table->dateTime('tarikh_tutup_ulasan')->nullable();
            $table->date('tarikh_keputusan')->nullable();
            $table->date('tarikh_dokumen');
            $table->string('salinan_bilangan');
            $table->string('jenis');
            $table->unsignedBigInteger('created_by');
            $table->foreign('created_by')->references('id')->on('users');
            $table->unsignedBigInteger('updated_by')->nullable();
            $table->foreign('updated_by')->references('id')->on('users');



            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('documents');
    }
};
