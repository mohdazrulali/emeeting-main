<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('meetings', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->date('tarikh_mesyuarat');
            $table->boolean('is_approved')->default(0);
            $table->string('jenis');
            $table->enum('tujuan', ['Makluman', 'Tindakan Segera', 'Tindakan Serta Merta', 'Tindakan Susulan']);
            $table->date('approved_date');
            $table->string('ringkasan');
            $table->string('no_rujukan_fail');
            $table->dateTime('tarikh_tutup')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('meetings');
    }
};
