<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->softDeletes();
        });

        Schema::table('documents', function (Blueprint $table) {
            $table->softDeletes();
        });

        Schema::table('meetings', function (Blueprint $table) {
            $table->softDeletes();
        });

        Schema::table('lampirans', function (Blueprint $table) {
            $table->softDeletes();
        });

        Schema::table('agihans', function (Blueprint $table) {
            $table->softDeletes();
        });

        Schema::table('replies', function (Blueprint $table) {
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });
        
        Schema::table('documents', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });

        Schema::table('meetings', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });

        Schema::table('lampirans', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });

        Schema::table('agihans', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });

        Schema::table('replies', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });
    }
};
